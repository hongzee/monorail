# lerna-typescript-cra-uilib-starter

Starter for Monorepo: Lerna, TypeScript, CRA and Storybook

# Commands

Bootstrap & Install Deps

`yarn`

Start App Locally

`yarn start`

Start Storybook

`yarn story`

Build App

`yarn build`

Loading a new libs into main app (run this in monorail-app)

`yarn lerna add react`
