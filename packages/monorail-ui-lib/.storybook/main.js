module.exports = {
  stories: ['../src/**/*.story.[tj]s(x)?'],
  addons: ['@storybook/addon-knobs/register']
};
