import React, { useState } from "react";
import { withKnobs, radios, number } from "@storybook/addon-knobs";
// import Container from "@material-ui/core/Container";
import Popover from "@material-ui/core/Popover";
import PopperCard from "../mortyCard";
import Button from "@material-ui/core/Button";
// import RickAndMorty from "../rickandmorty/index";

// import { BarProps } from "@nivo/bar";

export default {
  title: "Popper",
  decorators: [withKnobs]
};

// export const morty = () => {
//   return <RickAndMorty content={}/>;
// };

export const buttonPopper = () => {
  const [anchorEl, setAnchorEl] = useState<HTMLButtonElement | null>(null);

  return (
    <React.Fragment>
      <Button onClick={event => setAnchorEl(event.currentTarget)}>
        Click me
      </Button>
      <Popover
        id={"popover"}
        open={Boolean(anchorEl)}
        anchorEl={anchorEl}
        onClose={() => setAnchorEl(null)}
        anchorOrigin={{
          vertical: "top",
          horizontal: "right"
        }}
        transformOrigin={{
          vertical: "bottom",
          horizontal: "left"
        }}
      >
        <PopperCard
          content={{
            name: "SuperContent",
            status: "Alive",
            gender: "Male",
            origin: { name: "Test Origin" },
            location: { name: "Test location" },
            episode: []
          }}
        />
      </Popover>
    </React.Fragment>
  );
};
