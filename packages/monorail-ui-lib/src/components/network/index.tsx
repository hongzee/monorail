import React, { useRef, useEffect, useMemo } from "react";

import _isEqual from "lodash/isEqual";
import _cloneDeep from "lodash/cloneDeep";

import ForceGraph2D, {
  ForceGraphProps,
  NodeObject,
  LinkObject,
  GraphData,
} from "react-force-graph-2d";
import ForceGraph3D from "react-force-graph-3d";

export type MyNodeObject = NodeObject & {
  image?: string;
  name?: string;
  status?: string;
  species?: string;
  created?: string;
  gender?: string;
  type?: string;
  color?: string;
  class?: boolean;
};

export interface MyGraphData extends GraphData {
  nodes: MyNodeObject[];
  links: LinkObject[];
}

interface MyForceGraphProps extends ForceGraphProps {
  distance?: number;
  strength?: number;
  graphData?: MyGraphData;
}

export const Network2D = React.memo((props: MyForceGraphProps) => {
  const fgRef = useRef(null);
  const { distance = 30, strength = -30 } = props;

  const data = useMemo(() => {
    return {
      nodes: _cloneDeep(props.graphData.nodes),
      links: _cloneDeep(props.graphData.links),
    };
  }, [props.graphData.nodes, props.graphData.links, props.nodeAutoColorBy]);

  useEffect(() => {
    if (fgRef) {
      fgRef.current.d3Force("link").distance(distance);
    }
  }, [distance]);

  useEffect(() => {
    if (fgRef) {
      fgRef.current.d3Force("charge").strength(strength);
    }
  }, [strength]);

  return (
    <ForceGraph2D
      {...props}
      ref={fgRef}
      graphData={data}
      // graphData={_cloneDeep(props.graphData)}
    />
  );
}, areEqual);

export const Network3D = (props: ForceGraphProps) => {
  return <ForceGraph3D {...props} />;
};

function areEqual(prevProps, nextProps) {
  /*
  return true if passing nextProps to render would return
  the same result as passing prevProps to render,
  otherwise return false
  */
  return (
    _isEqual(prevProps.graphData, nextProps.graphData) &&
    prevProps.distance === nextProps.distance &&
    prevProps.strength === nextProps.strength &&
    prevProps.nodeAutoColorBy === nextProps.nodeAutoColorBy &&
    prevProps.nodeCanvasObject === nextProps.nodeCanvasObject
  );
}

export default Network2D;
