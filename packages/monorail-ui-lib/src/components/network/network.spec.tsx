import React from "react";
import ReactDom from "react-dom";

const Test = () => {
  return <div>Test</div>;
};

test("render the network diagram", () => {
  const div = document.createElement("div");
  document.body.appendChild(div);
  ReactDom.render(<Test />, div);
});
