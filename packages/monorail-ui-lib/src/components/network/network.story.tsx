import React, { useCallback, useState } from "react";

import {
  withKnobs,
  number,
  object,
  optionsKnob as options,
} from "@storybook/addon-knobs";

import { Network2D, Network3D, MyNodeObject } from "./index";
import { NodeObject } from "react-force-graph-2d";

import rick from "./rick.json";
import miserables from "./miserables.json";

export default { title: "Network", decorators: [withKnobs] };

const rangeDistance = {
  range: true,
  min: 0,
  max: 400,
  step: 10,
};

const rangeStrength = {
  range: true,
  min: -100,
  max: 100,
  step: 5,
};

export const Simple2d = () => {
  const distance = number("distance", 50, rangeDistance);
  const strength = number("strength", -30, rangeStrength);
  return (
    <Network2D graphData={miserables} distance={distance} strength={strength} />
  );
};

export const Simple3d = () => <Network3D graphData={miserables} />;

export const Rick2d = () => {
  const graphData = object("Graph Data", rick, "graphdata");
  const distance = number("distance", 100, rangeDistance);
  const strength = number("strength", -50, rangeStrength);

  const colorByObj = Object.keys(graphData.nodes?.[0])
    .filter((k: string) => k !== "image" && k !== "created" && k !== "id")
    .reduce((acc, k: string) => ({ ...acc, [k]: k }), {});

  const colorBy = options(
    "Color By",
    colorByObj,
    "status",
    { display: "select" },
    "colorBy"
  );

  const [highlightNodes, setHighlightNodes] = useState(new Set());
  // const [highlightLinks, setHighlightLinks] = useState(new Set());
  const [hoverNode, setHoverNode] = useState<NodeObject | null>(null);

  const updateHighlight = () => {
    setHighlightNodes(highlightNodes);
    // setHighlightLinks(highlightLinks);
  };

  const handleNodeHover = (node: MyNodeObject | null) => {
    highlightNodes.clear();
    // highlightLinks.clear();

    if (node) {
      highlightNodes.add(node);
      // node.neighbors.forEach(neighbor => highlightNodes.add(neighbor));
      // node.links.forEach(link => highlightLinks.add(link));
    }

    setHoverNode(node || null);
    updateHighlight();
  };

  const nodeCanvasObject = useCallback(
    (node: MyNodeObject, ctx) => {
      const { x = 0, y = 0, image = "", color = "#ea5400" } = node;
      const size = 50;

      if (hoverNode) {
        ctx.beginPath();
        ctx.arc(node.x, node.y, (size / 2) * 1.4, 0, 2 * Math.PI, false);
        ctx.fillStyle = node === hoverNode ? "red" : "orange";
        ctx.fill();
        ctx.closePath();
      }

      // Draws outline around nodes
      ctx.beginPath();
      ctx.fillStyle = color;
      ctx.arc(x, y, size / 2 + 3, 0, 2 * Math.PI, false);
      ctx.fill();
      ctx.closePath();

      // Clips the images to the size of the defined arc
      ctx.beginPath();
      ctx.save();
      ctx.arc(x, y, size / 2, 0, 2 * Math.PI, false);
      ctx.clip();
      const img = new Image();
      img.src = image;
      ctx.drawImage(img, x - size / 2, y - size / 2, size, size);
      ctx.restore();
      ctx.closePath();
    },
    [hoverNode]
  );

  return (
    <Network2D
      graphData={graphData}
      nodeVal={50}
      linkWidth={3}
      distance={distance}
      nodeAutoColorBy={colorBy}
      strength={strength}
      linkColor={() => "indigo"}
      nodeCanvasObjectMode={(node) =>
        highlightNodes.has(node) ? "after" : "replace"
      }
      nodeCanvasObject={nodeCanvasObject}
      // dagMode="bu"
      onNodeHover={handleNodeHover}
    />
  );
};
