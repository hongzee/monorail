import React, { useState } from "react";
import clsx from "clsx";
import {
  createStyles,
  makeStyles,
  useTheme,
  Theme,
} from "@material-ui/core/styles";
import Avatar from "@material-ui/core/Avatar";
import Drawer from "@material-ui/core/Drawer";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import List from "@material-ui/core/List";
import CssBaseline from "@material-ui/core/CssBaseline";
import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";
import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import Tooltip from "@material-ui/core/Tooltip";
import AccountCircle from "@material-ui/icons/AccountCircle";

import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import _isNil from "lodash/isNil";

// Google login

const drawerWidth = 240;

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: "flex",
    },
    appBar: {
      zIndex: theme.zIndex.drawer + 1,
      transition: theme.transitions.create(["width", "margin"], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
      backgroundColor: theme.palette.primary.dark,
    },
    appBarShift: {
      marginLeft: drawerWidth,
      width: `calc(100% - ${drawerWidth}px)`,
      transition: theme.transitions.create(["width", "margin"], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen,
      }),
    },
    title: {
      flexGrow: 1,
    },
    menuButton: {
      marginRight: 36,
    },
    hide: {
      display: "none",
    },
    drawer: {
      width: drawerWidth,
      flexShrink: 0,
      whiteSpace: "nowrap",
    },
    drawerOpen: {
      width: drawerWidth,
      transition: theme.transitions.create("width", {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen,
      }),
    },
    drawerClose: {
      transition: theme.transitions.create("width", {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
      overflowX: "hidden",
      width: theme.spacing(7) + 1,
      [theme.breakpoints.up("sm")]: {
        width: theme.spacing(9) + 1,
      },
    },
    toolbar: {
      display: "flex",
      alignItems: "center",
      justifyContent: "flex-end",
      padding: theme.spacing(0, 1),
      ...theme.mixins.toolbar,
    },
    content: {
      flexGrow: 1,
      padding: theme.spacing(3),
    },
    listIcon: {
      marginLeft: 7,
    },
    selectedIcon: {
      color: theme.palette.action.selected,
    },
  })
);

export interface MiniDrawerProps {
  text?: string;
  pages?: Array<Page>;
  activePage?: number;
  onPageChange?: (pageIndex: number) => void;
  profile?: any;
  auth?: boolean;
  signIn?: () => void;
  signOut?: () => void;
}

export interface Page {
  text: string;
  icon: JSX.Element;
  path: string;
  component: JSX.Element;
}

export const MiniDrawer = ({
  auth,
  signIn,
  signOut,
  profile,
  ...props
}: MiniDrawerProps) => {
  // use States
  const [activePage, setActivePage] = useState(props.activePage);
  const [open, setOpen] = useState(false);

  // other "use" hooks
  const classes = useStyles();
  const theme = useTheme();

  const { text = "", pages = [], onPageChange } = props;
  const activePageNumber = _isNil(activePage) ? false : true;

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  const handlePageChange = (pageIndex) => {
    setActivePage(pageIndex);
    onPageChange(pageIndex);
  };

  return (
    <>
      <CssBaseline />
      <AppBar
        position="fixed"
        className={clsx(classes.appBar, {
          [classes.appBarShift]: open,
        })}
      >
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            onClick={handleDrawerOpen}
            edge="start"
            className={clsx(classes.menuButton, {
              [classes.hide]: open,
            })}
          >
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" className={classes.title} noWrap>
            {text}
          </Typography>
          <div>
            {auth ? (
              <IconButton
                aria-label={profile.givenName}
                aria-controls="menu-appbar"
                aria-haspopup="true"
                color="inherit"
                onClick={() => signOut()}
              >
                {profile.imageUrl ? (
                  <Avatar alt={profile.givenName} src={profile.imageUrl} />
                ) : (
                  <AccountCircle />
                )}
              </IconButton>
            ) : (
              <Button color="inherit" onClick={() => signIn()}>
                Login
              </Button>
            )}
          </div>
        </Toolbar>
      </AppBar>
      <Drawer
        variant="permanent"
        className={clsx(classes.drawer, {
          [classes.drawerOpen]: open,
          [classes.drawerClose]: !open,
        })}
        classes={{
          paper: clsx({
            [classes.drawerOpen]: open,
            [classes.drawerClose]: !open,
          }),
        }}
      >
        <div className={classes.toolbar}>
          <IconButton onClick={handleDrawerClose}>
            {theme.direction === "rtl" ? (
              <ChevronRightIcon />
            ) : (
              <ChevronLeftIcon />
            )}
          </IconButton>
        </div>
        <Divider />
        {pages.map((page: Page, index) => (
          <List key={page.text}>
            <ListItem button>
              <Tooltip title={page.text}>
                <ListItemIcon
                  className={clsx(classes.listIcon, {
                    [classes.selectedIcon]:
                      activePageNumber && activePage === index,
                  })}
                  onClick={() => handlePageChange(index)}
                >
                  {page.icon}
                </ListItemIcon>
              </Tooltip>
              <ListItemText primary={page.text} />
            </ListItem>
          </List>
        ))}
      </Drawer>
    </>
  );
};

export default MiniDrawer;
