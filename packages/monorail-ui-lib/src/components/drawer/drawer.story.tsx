import React, { useState } from "react";
import MiniDrawer from ".";
import FaceIcon from "@material-ui/icons/TagFaces";
// import DialogTitle from '@material-ui/core/DialogTitle';
// import Dialog from '@material-ui/core/Dialog';
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";

import { HashRouter as Router, Route } from "react-router-dom";

import Keycloak from "keycloak-js";
import { KeycloakProvider, useKeycloak } from "@react-keycloak/web";
const keycloak = new (Keycloak as any)({
  url: "https://keycloak.infratrode.com/auth",
  realm: "Monorail",
  clientId: "account",
});

import { useGoogleLogin, useGoogleLogout } from "react-google-login";

const clientId: string =
  "78267046844-1ur9kt69f70dk8f98v9s8d204h8qo3pc.apps.googleusercontent.com";

const PAGES = [
  { text: "Face", icon: <FaceIcon />, path: "/face", component: <div /> },
];

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    app: {
      width: "100%",
      minHeight: "100vh",
      display: "flex",
    },
    toolbar: {
      display: "flex",
      alignItems: "center",
      justifyContent: "flex-end",
      padding: theme.spacing(0, 1),
      // necessary for content to be below app bar
      ...theme.mixins.toolbar,
    },
    content: {
      flexGrow: 1,
      padding: `${theme.spacing(3)}px`,
    },
  })
);

const AppRoutes = () => {
  const [keycloak, initialized] = useKeycloak();
  const classes = useStyles();

  if (!initialized) {
    return <div>Loading...</div>;
  }

  return (
    <div className={classes.app}>
      <MiniDrawer
        text="LOOKING GLASS"
        pages={PAGES}
        onPageChange={() => {}}
        activePage={-1}
        auth={keycloak.authenticated}
        signIn={keycloak.login}
        signOut={keycloak.logout}
      />
      <main className={classes.content}>
        <div className={classes.toolbar} />

        <Router>
          <Route key="home" path="/">
            <div>
              {keycloak?.tokenParsed?.["given_name"]} is
              {keycloak.authenticated ? "  " : " NOT"} authenticated
            </div>
          </Route>
        </Router>
      </main>
    </div>
  );
};

export const KeycloakLogin = () => {
  return (
    <KeycloakProvider
      keycloak={keycloak}
      initConfig={{ onLoad: "check-sso" }}
      onEvent={(evt) => console.log("onKeycloakEvt", evt)}
      onTokens={(tk) => console.log("onTokens", tk)}
    >
      <AppRoutes />
    </KeycloakProvider>
  );
};

export const GoogleLoginTest = () => {
  const [profile, setProfile] = useState({});
  const [token, setToken] = useState({});
  const [auth, setAuth] = useState(false);

  const onSuccessLogin = (res) => {
    console.log(res);
    setAuth(true);
    setProfile(res.profileObj);
    setToken(res.tokenObj);
  };

  const onFailureLogin = (error) => {
    console.log("error: ", error);
  };

  const onLogoutSuccess = () => {
    console.log("Successfully logged off...");
    setAuth(false);
    setProfile({});
    setToken({});
  };

  const onFailureLogout = () => {
    console.log("error logging out");
  };

  const { signIn } = useGoogleLogin({
    clientId,
    onSuccess: onSuccessLogin,
    onFailure: onFailureLogin,
    cookiePolicy: "single_host_origin",
    isSignedIn: true,
  });

  const { signOut } = useGoogleLogout({
    clientId,
    onLogoutSuccess,
    onFailure: onFailureLogout,
    cookiePolicy: "single_host_origin",
  });

  return (
    <MiniDrawer
      text="LOOKING GLASS"
      pages={PAGES}
      onPageChange={() => {}}
      activePage={-1}
      profile={profile}
      auth={auth}
      signIn={signIn}
      signOut={signOut}
    />
  );
};

export default { title: "Login Examples" };
