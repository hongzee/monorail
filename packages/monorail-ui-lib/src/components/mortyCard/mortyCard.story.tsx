import React from "react";
import { withKnobs, radios, number } from "@storybook/addon-knobs";
import _orderBy from "lodash/orderBy";
import _keys from "lodash/keys";
import _has from "lodash/has";
import _isEmpty from "lodash/isEmpty";
import PopperCard from "./index";
import rmData from "./rickAndMorty.json";
import * as d3 from "d3";
import { dsv } from "d3";

export default {
  title: "Morty Card",
  decorators: [withKnobs]
};

export const mortycard = () => {
  return <PopperCard content={rmData[0]} />;
};

export const mortySummary = () => {
  // console.log("d3:", d3);
  // const groupData = d3.group(rmData, d => {
  let searchVars = { gender: "", status: "", species: "" };
  let groups = ["gender", "species", "status"];
  let nesting = _keys(searchVars).map(key => {
    if (searchVars[key] !== "") {
      return key;
    }
    return "";
  });
  let searchVals = _keys(searchVars).map(key => searchVars[key]);
  // console.log("nesting:", nesting);

  const groupRollups = groups.map(group => {
    let groupData = d3.nest();
    let path = [];
    nesting
      .filter(nest => nest !== group && !_isEmpty(nest))
      .forEach((nest: string) => {
        console.log("group, nest:", group, nest);
        groupData.key((d: any) => d[nest]);
        path.push(searchVars[nest]);
      });
    groupData.key((d: any) => d[group]);
    let groupRolled: any = groupData
      .rollup((v: any) => v.length)
      .entries(rmData);
    console.log("groupRolled:", groupRolled);
    if (!_isEmpty(searchVars[group])) {
      path.push(searchVars[group]);
      // groupRolled = groupRolled[0];
    }
    return {
      data: groupRolled,
      path: path
    };
  });

  console.log("groupRollups:", groupRollups);
  let genderCounts = groupRollups[0];
  let speciesCounts = groupRollups[1];
  let statusCounts = groupRollups[2];

  statusCounts.path.forEach(p => {
    statusCounts.data = _has(
      statusCounts.data.filter(d => d.key === p)[0],
      "values"
    )
      ? statusCounts.data.filter(d => d.key === p)[0].values
      : [statusCounts.data.filter(d => d.key === p)[0]];
  });
  speciesCounts.path.forEach(p => {
    speciesCounts.data = _has(
      speciesCounts.data.filter(d => d.key === p)[0],
      "values"
    )
      ? speciesCounts.data.filter(d => d.key === p)[0].values
      : [speciesCounts.data.filter(d => d.key === p)[0]];
  });

  genderCounts.path.forEach(p => {
    genderCounts.data = _has(
      genderCounts.data.filter(d => d.key === p)[0],
      "values"
    )
      ? genderCounts.data.filter(d => d.key === p)[0].values
      : [genderCounts.data.filter(d => d.key === p)[0]];
  });
  console.log("searchVals:", searchVals);
  console.log("genderCounts:", genderCounts);
  console.log("speciesCounts:", speciesCounts);
  console.log("statusCounts:", statusCounts);
  // console.log("searchVals:", searchVals);

  // console.log("genderData:", data);

  // const d3Data = d3.nest();

  // let genderData = d3Data.key((d: any) => d.gender);
  // let genderData = d3Data.key((d: any) => d.gender);
  //  let status2 = gender2.key((d: any) => d.status);
  //let data = gender2.entries(rmData);

  const genderRollups = d3
    .nest()
    // .key((d: any) => d.status)
    .key((d: any) => d.species)
    .key((d: any) => d.gender)
    .rollup((v: any) => v.length)
    .entries(rmData);
  console.log("genderRollups:", genderRollups);
  // const speciesRollups = d3
  //   .nest()
  //   .key((d: any) => d.gender)
  //   .key((d: any) => d.species)
  //   // .key((d: any) => d.status)
  //   .rollup((v: any) => v.length)
  //   .entries(rmData);
  // const statusRollups = d3
  //   .nest()
  //   .key((d: any) => d.gender)
  //   // .key((d: any) => d.species)
  //   .key((d: any) => d.status)
  //   // .key((d: any) => d.status)
  //   .rollup((v: any) => v.length)
  //   .entries(rmData);
  // return d.gender;
  // const rollupData = d3.rollup(rmData, d => d.gender);
  // console.log("groupData:", genderRollups, speciesRollups, statusRollups);
  // console.log("rollupData:", rollupData);
  return <div>test</div>;
};
