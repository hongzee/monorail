import React from "react";
import { makeStyles, Theme, createStyles } from "@material-ui/core/styles";
import clsx from "clsx";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardMedia from "@material-ui/core/CardMedia";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import Collapse from "@material-ui/core/Collapse";
import Avatar from "@material-ui/core/Avatar";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import { red } from "@material-ui/core/colors";
import FavoriteIcon from "@material-ui/icons/Favorite";
import ShareIcon from "@material-ui/icons/Share";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import MoreVertIcon from "@material-ui/icons/MoreVert";
// import Skeleton from "@material-ui/lab/Skeleton";
// import { PopoverProps } from "@material-ui/core/Popover";
import Grid from "@material-ui/core/Grid";
import Box from "@material-ui/core/Box";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      maxWidth: 345
    },
    media: {
      height: 190,
      paddingTop: "56.25%" // 16:9
    },
    expand: {
      transform: "rotate(0deg)",
      marginLeft: "auto",
      transition: theme.transitions.create("transform", {
        duration: theme.transitions.duration.shortest
      })
    },
    expandOpen: {
      transform: "rotate(180deg)"
    },
    avatar: {
      backgroundColor: red[500]
    },
    card: {
      maxWidth: 345,
      margin: theme.spacing(2)
    }
  })
);

export interface PopperCardProps {
  // loading?: boolean;
  content: any;
}

export const PopperCard = (props: PopperCardProps) => {
  const classes = useStyles();
  const [expanded, setExpanded] = React.useState(false);
  const { content } = props;
  const handleExpandClick = () => {
    setExpanded(!expanded);
  };

  return (
    <div style={{ overflow: "auto" }}>
      <Card className={classes.root}>
        <CardHeader
          avatar={
            <Avatar aria-label="recipe" className={classes.avatar}>
              <img src={content.image} height={50} width={50} />
            </Avatar>
          }
          action={
            <IconButton aria-label="settings">
              <MoreVertIcon />
            </IconButton>
          }
          title={content.name}
          subheader={
            content.type
              ? `${content.species} (${content.type})`
              : content.species
          }
        />
        {/* <CardMedia
        className={classes.media}
        image="https://imagesvc.meredithcorp.io/v3/mm/image?url=https%3A%2F%2Fcdn-image.myrecipes.com%2Fsites%2Fdefault%2Ffiles%2Fstyles%2F4_3_horizontal_-_1200x900%2Fpublic%2F1551740522%2F0867_180612_DuPree_MyRecipe_Traditional%20Spanish%20Paella_6996%20copy.jpg%3Fitok%3Dcr6GuJWO"
        title="Paella dish"
      /> */}
        <CardContent>
          <Grid container spacing={3}>
            <Grid item xs={4}>
              <Box
                fontWeight="fontWeightBold"
                fontSize={12}
                color="darkgoldenrod"
              >
                Gender
              </Box>
            </Grid>
            <Grid item xs={8}>
              <Box fontSize={12}>{content.gender}</Box>
            </Grid>
            <Grid item xs={4}>
              <Box
                fontWeight="fontWeightBold"
                fontSize={12}
                color="darkgoldenrod"
              >
                Status
              </Box>
            </Grid>
            <Grid item xs={8}>
              <Box fontSize={12}>{content.status}</Box>
            </Grid>
            <Grid item xs={4}>
              <Box
                fontWeight="fontWeightBold"
                fontSize={12}
                color="darkgoldenrod"
              >
                Origin
              </Box>
            </Grid>
            <Grid item xs={8}>
              <Box fontSize={12}>{content.origin.name}</Box>
            </Grid>
            <Grid item xs={4}>
              <Box
                fontWeight="fontWeightBold"
                fontSize={12}
                color="darkgoldenrod"
              >
                Location
              </Box>
            </Grid>
            <Grid item xs={8}>
              <Box fontSize={12}>{content.location.name}</Box>
            </Grid>
          </Grid>
        </CardContent>
        <CardActions disableSpacing>
          <IconButton aria-label="add to favorites">
            <FavoriteIcon />
          </IconButton>
          <IconButton aria-label="share">
            <ShareIcon />
          </IconButton>
          <IconButton
            className={clsx(classes.expand, {
              [classes.expandOpen]: expanded
            })}
            onClick={handleExpandClick}
            aria-expanded={expanded}
            aria-label="show more"
          >
            <ExpandMoreIcon />
          </IconButton>
        </CardActions>
        <Collapse in={expanded} timeout="auto" unmountOnExit>
          <CardContent>
            <Typography paragraph>Episodes</Typography>
            <Grid container spacing={3}>
              {content.episode.map(ep => (
                <React.Fragment>
                  <Grid item xs={4}>
                    <Box
                      fontWeight="fontWeightBold"
                      fontSize={12}
                      color="darkgoldenrod"
                    >
                      {ep.episode}
                    </Box>
                  </Grid>
                  <Grid item xs={8}>
                    <Box fontSize={12}> {ep.name}</Box>
                  </Grid>
                </React.Fragment>
              ))}
            </Grid>
          </CardContent>
        </Collapse>
      </Card>
    </div>
  );
};

export default PopperCard;
