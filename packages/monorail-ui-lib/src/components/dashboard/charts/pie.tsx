import React, { ProviderExoticComponent } from "react";
import { ResponsivePie, PieSvgProps } from "@nivo/pie";
// make sure parent container have a defined height when using
// responsive component, otherwise height will be 0 and
// no chart will be rendered.

const PieComponent = (props: PieSvgProps) => {
  const { data = [], fill = [], colors } = props;
  return (
    <ResponsivePie
      data={data}
      sortByValue={true}
      margin={{ top: 40, right: 80, bottom: 80, left: 80 }}
      innerRadius={0}
      // padAngle={0.7}
      // cornerRadius={8}
      colors={colors}
      // borderWidth={1}
      // borderColor={{ from: "color", modifiers: [["darker", 0.2]] }}
      // radialLabelsSkipAngle={10}
      // radialLabelsTextXOffset={6}
      // radialLabelsTextColor="#333333"
      // radialLabelsLinkOffset={0}
      // radialLabelsLinkDiagonalLength={16}
      // radialLabelsLinkHorizontalLength={24}
      // radialLabelsLinkStrokeWidth={1}
      // radialLabelsLinkColor={{ from: "color" }}
      // slicesLabelsSkipAngle={10}
      // slicesLabelsTextColor="#333333"
      animate={true}
      motionStiffness={90}
      motionDamping={15}
      defs={[]}
      fill={fill}
      legends={[]}
    />
  );
};

export default PieComponent;
