import React from "react";
import { ResponsiveBar, BarSvgProps } from "@nivo/bar";

// import { LegendMouseHandler } from "@nivo/legends";
// make sure parent container have a defined height when using
// responsive component, otherwise height will be 0 and
// no chart will be rendered.

const ResponsiveBarComponent = (props: BarSvgProps) => {
  const {
    data = [],
    keys = [],
    groupMode = "grouped",
    indexBy = "country",
    layout = "vertical",
    colors,
    maxValue,
    minValue,
    fill,
    onClick,
    axisLeft,
    axisBottom
  } = props;

  // const legendHandler: LegendMouseHandler = (data, event) => {
  //   console.log(`legend click: ${data}`);
  // };
  // console.log("data:", data);
  return (
    <ResponsiveBar
      data={data}
      // keys={["hot dog", "burger", "sandwich", "kebab", "fries", "donut"]}
      keys={keys}
      indexBy={indexBy}
      layout={layout}
      colors={colors}
      groupMode={groupMode}
      maxValue={maxValue}
      minValue={minValue}
      onClick={onClick}
      axisBottom={axisBottom}
      axisLeft={axisLeft}
      padding={0.3}
      margin={{ top: 25, right: 130, bottom: 5, left: 125 }}
      theme={{
        tooltip: {
          container: {
            fontSize: 12,
            color: "darkgoldenrod",
            background: "dimgray",
            borderRadius: "2px",
            boxShadow: "0 1px 2px rgba(0, 0, 0, 0.25)",
            padding: "5px 9px"
          }
        },
        axis: {
          legend: {
            text: {
              fontSize: 12,
              fill: "darkgoldenrod"
            }
          }
        }
      }}
      defs={[
        // using plain object
        {
          id: "gradientC",
          type: "linearGradient",
          colors: [
            { offset: 0, color: "#bdc3c7" },
            { offset: 100, color: "#2c3e50" }
          ]
        }
      ]}
      // 2. defining rules to apply those gradients
      fill={
        [
          // match using object query
          // { match: { id: "mode" }, id: "gradientA" },
          // // match using function
          // { match: d => d.id === "vue", id: "gradientB" },
          // match all, will only affect 'elm', because once a rule match,
          // others are skipped, so now it acts as a fallback
          // { match: "*", id: "gradientC" }
        ]
      }
      borderColor={{ from: "color", modifiers: [["darker", 1.6]] }}
      axisTop={null}
      axisRight={null}
      enableGridX={false}
      enableGridY={false}
      labelSkipWidth={33}
      labelSkipHeight={12}
      labelTextColor={{ from: "color", modifiers: [["brighter", 1.6]] }}
      tooltip={tooltip => {
        const { id, value, color, indexValue } = tooltip;
        // console.log("tooltip:", tooltip);
        return (
          // <strong style={{ color }}>
          <strong
          // style={{
          //   WebkitTextStrokeWidth: "0.0009em",
          //   WebkitTextStrokeColor: "black"
          // }}
          >
            {indexValue} - {value} {id}
          </strong>
        );
      }}
      // labelTextColor="inherit:darker(1.4)"

      // legends={[
      //   {
      //     dataFrom: "keys",
      //     anchor: "bottom-right",
      //     direction: "column",
      //     justify: false,
      //     translateX: 120,
      //     translateY: 0,
      //     itemsSpacing: 2,
      //     itemWidth: 100,
      //     itemHeight: 20,
      //     itemDirection: "left-to-right",
      //     itemOpacity: 0.85,
      //     symbolSize: 20,
      //     effects: [
      //       {
      //         on: "hover",
      //         style: {
      //           itemOpacity: 1
      //         }
      //       }
      //     ],
      //     onClick: data => console.log(`legend click: ${JSON.stringify(data)}`)
      //   }
      // ]}
      animate={true}
      motionStiffness={90}
      motionDamping={15}
    />
  );
};

export default ResponsiveBarComponent;
