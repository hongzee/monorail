import React, { useState, useEffect } from "react";

import {
  ApolloClient,
  HttpLink,
  InMemoryCache,
  ApolloProvider,
  useQuery,
  gql,
} from "@apollo/client";
import Container from "@material-ui/core/Container";
import HomeIcon from "@material-ui/icons/Home";
import Close from "@material-ui/icons/Close";
// import { ResponsiveSunburst } from "@nivo/sunburst";
// import { ResponsiveBubble } from "@nivo/circle-packing";
import { ResponsivePie } from "@nivo/pie";

import _keys from "lodash/keys";
import _orderBy from "lodash/orderBy";
import _flatten from "lodash/flatten";
import _isNil from "lodash/isNil";
import _find from "lodash/find";
import _countBy from "lodash/countBy";
import _mergeWith from "lodash/mergeWith";
// import { Map } from "@monorail/ui-lib";
// import { HeatmapLayer } from "@deck.gl/aggregation-layers";

import {
  MODES,
  AXIS_BOTTOM,
  AXIS_LEFT,
  FEED_COLORS,
  // PIE_DATA,
  // PIE_DATA2
} from "./constants/constants";

import BreadCrumbs from "./breadcrumbs";
import ResponiveBarComponent from "./charts/bar";
// import ResponsiveRadarComponent from "./charts/radar";
// import { Feedback } from "@material-ui/icons";
// import PieComponent from "./pie";

const client = new ApolloClient({
  cache: new InMemoryCache(),
  link: new HttpLink({
    uri: "https://api.digitransit.fi/routing/v1/routers/finland/index/graphql",
  }),
});

const GET_FEEDS = gql`
  query {
    feeds {
      feedId
      agencies {
        id
        gtfsId
        lang
        name
        url
        timezone
        lang
        phone
        fareUrl
        routes {
          id
          gtfsId
          shortName
          longName
          type
          mode
          desc
          color
          # bikesAllowed
          # trips {
          #   id
          #}
        }
      }
    }
    # clusters {
    #   id
    #   lat
    #   lon
    # }
  }
`;

export interface DashboardProps {
  text?: string;
}

const DashboardComponent = () => {
  const { loading, error, data } = useQuery(GET_FEEDS);
  const [feedFilter, setFeedFilter] = useState(null);
  const [agencyFilter, setAgencyFilter] = useState(null);
  const [modeFilter, setModeFilter] = useState(null);
  const [breadCrumbs, setBreadCrumbs] = useState([]);
  const [companyModeFilter, setCompanyModeFilter] = useState(null);
  const [agencyModeFilter, setAgencyModeFilter] = useState(null);

  //Defaults
  const barHeight = 30;
  const singleBarHeight = 65;
  const chartWidth = "1100px";
  // const barColor = "hsl(217, 90%, 61%)";

  const handleReset = () => {
    setFeedFilter(null);
    setAgencyFilter(null);
    setModeFilter(null);
    setCompanyModeFilter(null);
    setAgencyModeFilter(null);
    // generateBreadCrumbs();
  };

  useEffect(() => {
    if (data) {
      const breadCrumbs = [
        {
          label: "Home",
          icon: <HomeIcon fontSize="small" />,
          type: "icon",
          callback: handleReset,
        },
      ];
      if (feedFilter) {
        breadCrumbs.push({
          label: feedFilter,
          icon: <Close />,
          type: "deleteIcon",
          callback: () => setFeedFilter(null),
        });
      }
      if (agencyFilter) {
        breadCrumbs.push({
          label: agencyData.filter(
            (agency: any) => agency.id === agencyFilter
          )[0].agency,
          icon: <Close />,
          type: "deleteIcon",
          callback: () => setAgencyFilter(null),
        });
      }
      if (modeFilter) {
        breadCrumbs.push({
          label: MODES.filter((mode) => mode.id === modeFilter)[0].label,
          icon: <Close />,
          type: "deleteIcon",
          callback: () => setModeFilter(null),
        });
      }
      if (!_isNil(companyModeFilter)) {
        breadCrumbs.push({
          label: companyModeFilter ? "Multi Companies" : "Single Companies",
          icon: <Close />,
          type: "deleteIcon",
          callback: () => setCompanyModeFilter(null),
        });
      }
      if (!_isNil(agencyModeFilter)) {
        breadCrumbs.push({
          label: agencyModeFilter ? "Multi Agencies" : "Single Agencies",
          icon: <Close />,
          type: "deleteIcon",
          callback: () => setAgencyModeFilter(null),
        });
      }
      setBreadCrumbs(breadCrumbs);
    }
  }, [
    data,
    feedFilter,
    agencyFilter,
    modeFilter,
    companyModeFilter,
    agencyModeFilter,
  ]);

  const barColorScheme = ({ id, data }) => {
    return data[`${id}Color`];
  };

  if (loading) return <p>Loading...</p>;
  if (error) return <p>Error :(</p>;

  // const handleClick = (event: React.MouseEvent<Element, MouseEvent>) => {
  //   event.preventDefault();
  // };

  const handleModeSelection = (data) => {
    //Need to set the mode and the agency?
    setModeFilter(data.data.modeId);
  };

  const handleSetFeedFilter = (data) => {
    setFeedFilter(data.indexValue);
  };

  const handleSetAgencyFilter = (data) => {
    setAgencyFilter(data.data.id);
  };

  const handlePieModeClick = (data) => {
    setCompanyModeFilter(data.id === "multimode" ? true : false);
    // console.log("data:", data);
  };
  const handlePieAgencyModeClick = (data) => {
    setAgencyModeFilter(data.id === "multimode" ? true : false);
  };

  //NOTE: preparing the data for the charts - should normally be done mostly by graphql or solr
  //BEGIN DATA PREP

  //Filter feeds by the selected feed
  let filteredFeeds = _isNil(feedFilter)
    ? data.feeds
    : data.feeds.filter((feed) => feed.feedId.toUpperCase() === feedFilter);

  //Filter feeds by the selected agency
  filteredFeeds = _isNil(agencyFilter)
    ? filteredFeeds
    : filteredFeeds.filter(
        (feed) => _find(feed.agencies, ["id", agencyFilter]) !== undefined
      );

  let feedBarData = filteredFeeds.map((feed) => {
    const filteredAgencies = _isNil(agencyFilter)
      ? feed.agencies
      : feed.agencies.filter((agency) => agency.id === agencyFilter);

    const agencyData = filteredAgencies
      .map((agency, index) => ({
        id: agency.id,
        agency: `${agency.name.substring(0, 10).toUpperCase()}-${index}`,
        routes: _isNil(modeFilter)
          ? agency.routes.length
          : agency.routes.filter((route) => {
              return route.mode === _find(MODES, ["id", modeFilter]).mode;
            }).length,
        routesColor: FEED_COLORS[feed.feedId.toUpperCase()],
        modeCounts: _countBy(agency.routes, "mode"),
        multiMode: _keys(_countBy(agency.routes, "mode")).length > 1,
      }))
      .filter((agency) => agency.routes > 0);

    let feedModeData: any = _flatten(
      agencyData.map((agency: any) =>
        _keys(agency.modeCounts).map((key) => ({
          mode: key,
          count: agency.modeCounts[key],
        }))
      )
    ).reduce((acc: any, val: any) => {
      let o = acc
        .filter((obj: any) => {
          return obj.mode === val.mode;
        })
        .pop() || { mode: val.mode, count: 0 };
      o.count += val.count;
      acc.push(o);
      return acc;
    }, []);
    feedModeData = feedModeData.filter((itm, i, a) => {
      return i == a.indexOf(itm);
    });

    return {
      feed: feed.feedId.toUpperCase(),
      agencyData,
      agencies: agencyData.length,
      modeData: feedModeData,
      modes: feedModeData.length,
      feedColor: FEED_COLORS[feed.feedId.toUpperCase()],
    };
  });

  //Filter data by the selected mode
  feedBarData = _isNil(modeFilter)
    ? feedBarData
    : feedBarData.filter(
        (feed) =>
          feed.modeData.filter((mode) => {
            return (
              mode.mode ===
              MODES.filter((mode) => mode.id === modeFilter)[0].mode
            );
          }).length > 0
      );
  if (!_isNil(companyModeFilter)) {
    feedBarData = feedBarData.filter((feed) => {
      if (companyModeFilter) {
        return feed.modeData.length > 1;
      } else {
        return feed.modeData.length === 1;
      }
    });
  }

  const modeData = _orderBy(
    MODES.filter((mode) => {
      if (!_isNil(modeFilter)) {
        return mode.id === modeFilter;
      }
      return true;
    }).map((mode) => {
      let modeTotal = 0;
      let modeObj = {
        mode: mode.id.toUpperCase(),
        modeId: mode.id,
        total: 0,
      };
      feedBarData.forEach((feed) => {
        const feedMode = feed.modeData.filter(
          (modeData) => modeData.mode === mode.mode
        );
        if (feedMode.length > 0) {
          modeObj[`${feed.feed}`] = feedMode[0].count;
          modeTotal = modeTotal + feedMode[0].count;
        } else {
          modeObj[`${feed.feed}`] = 0;
        }
      });
      modeObj.total = modeTotal;
      return modeObj;
    }),
    ["total", "asc"]
  ).filter((mode) => mode.total > 0);
  // );

  let agencyData: Array<any> = _orderBy(
    _flatten(feedBarData.map((feed) => feed.agencyData)),
    ["routes"],
    ["desc"]
  );
  //END DATA PREP
  if (!_isNil(agencyModeFilter)) {
    agencyData = agencyData.filter(
      (agency: any) => agency.multiMode === agencyModeFilter
    );
  }

  const multiModeAgencyData = _countBy(
    agencyData.map((agency: any) => agency.multiMode)
  );
  const multiModeAgencyPieData = [
    {
      id: "multimode",
      label: "Multi Mode Agencies",
      value: multiModeAgencyData.true,
      color: "hsla(98, 56%, 27%, 1)",
    },
    {
      id: "notmultimode",
      label: "Single Mode Agencies",
      value: multiModeAgencyData.false,
      color: "hsla(0, 63%, 38%, 1)",
    },
  ];

  const feedMultiModeData = _countBy(
    feedBarData.map((feed) => feed.modeData.length > 1)
  );
  const feedPieModeData = [
    {
      id: "multimode",
      label: "Multi Mode Companies",
      value: feedMultiModeData.true,
      color: "hsla(98, 56%, 27%, 1)",
    },
    {
      id: "notmultimode",
      label: "Single Mode Companies",
      value: feedMultiModeData.false,
      color: "hsla(0, 63%, 38%, 1)",
    },
  ];

  const finlandView = {
    longitude: 24.9413548,
    latitude: 60.1717794,
    zoom: 10,
    pitch: 0,
    bearing: 0,
  };

  // const heatmapData = data.clusters.map(cluster => ({
  //   COORDINATES: [cluster.lat, cluster.lon],
  //   WEIGHT: 1
  // }));
  // const layer = new HeatmapLayer({
  //   id: "heatmapLayer",
  //   getPosition: d => d.COORDINATES,
  //   getWeight: d => d.WEIGHT
  // });

  return (
    <div
      style={{
        backgroundColor: "#303030",
        paddingTop: "15px",
        height: "1100px",
      }}
    >
      <div style={{ paddingLeft: "65px" }}>
        <BreadCrumbs crumbs={breadCrumbs} />
      </div>
      {/* <div
        style={{
          height: "400px"
        }}
      >
        <div style={{ position: "absolute", left: "100px", top: "80px" }}>
          <Map
            initialView={finlandView}
            baseMap={`https://api.maptiler.com/maps/darkmatter/style.json?key=R01BPnJMraEGqoIU2zM6`}
            height={400}
            width={800}
          />
        </div>
      </div> */}
      <div
        style={{
          height: "250px",
          width: "1100px",
          display: "flex",
        }}
      >
        <div style={{ height: "250px", flex: "0 0 50%" }}>
          <ResponsivePie
            colors={(data) => {
              return data.color as "string" | "number";
            }}
            margin={{ top: 80, right: 120, bottom: 80, left: 120 }}
            data={feedPieModeData}
            animate={true}
            innerRadius={0}
            padAngle={0.5}
            cornerRadius={5}
            radialLabel="label"
            radialLabelsLinkColor="darkgoldenrod"
            radialLabelsLinkStrokeWidth={2}
            radialLabelsTextColor="darkgray"
            onClick={handlePieModeClick}
            theme={{
              tooltip: {
                container: {
                  fontSize: 12,
                  color: "darkgoldenrod",
                  background: "dimgray",
                  borderRadius: "2px",
                  boxShadow: "0 1px 2px rgba(0, 0, 0, 0.25)",
                  padding: "5px 9px",
                },
              },
              axis: {
                legend: {
                  text: {
                    fontSize: 12,
                    fill: "darkgoldenrod",
                  },
                },
              },
            }}
          />
        </div>
        <div style={{ height: "250px", flex: 1 }}>
          <ResponsivePie
            colors={(data) => {
              return data.color as "string" | "number";
            }}
            margin={{ top: 80, right: 120, bottom: 80, left: 120 }}
            data={multiModeAgencyPieData}
            animate={true}
            innerRadius={0}
            padAngle={0.5}
            cornerRadius={5}
            radialLabel="label"
            radialLabelsLinkColor="darkgoldenrod"
            radialLabelsLinkStrokeWidth={2}
            radialLabelsTextColor="darkgray"
            onClick={handlePieAgencyModeClick}
            theme={{
              tooltip: {
                container: {
                  fontSize: 12,
                  color: "darkgoldenrod",
                  background: "dimgray",
                  borderRadius: "2px",
                  boxShadow: "0 1px 2px rgba(0, 0, 0, 0.25)",
                  padding: "5px 9px",
                },
              },
              axis: {
                legend: {
                  text: {
                    fontSize: 12,
                    fill: "darkgoldenrod",
                  },
                },
              },
            }}
          />
        </div>
      </div>
      <div
        style={{
          height:
            feedBarData.length > 1
              ? `${feedBarData.length * barHeight + 20}px`
              : `${singleBarHeight}px`,
          width: chartWidth,
        }}
      >
        <ResponiveBarComponent
          data={_orderBy(feedBarData, ["agencies"], ["asc"])}
          keys={["agencies"]}
          groupMode={"stacked"}
          indexBy={"feed"}
          layout={"horizontal"}
          colors={({ id, data }) => barColorScheme({ id: "feed", data })}
          axisLeft={{ ...AXIS_LEFT, legend: "Company Agencies" }}
          axisBottom={{ ...AXIS_BOTTOM }}
          onClick={handleSetFeedFilter}
        />
      </div>
      <div
        style={{
          height:
            agencyData.slice(0, 10).length > 1
              ? `${agencyData.slice(0, 10).length * barHeight + 20}px`
              : `${singleBarHeight}px`,
          width: chartWidth,
        }}
      >
        <ResponiveBarComponent
          data={_orderBy(agencyData.slice(0, 10), ["routes"], ["asc"])}
          keys={["routes"]}
          groupMode={"stacked"}
          indexBy={"agency"}
          layout={"horizontal"}
          colors={barColorScheme}
          axisLeft={{ ...AXIS_LEFT, legend: "Agency Routes" }}
          axisBottom={{ ...AXIS_BOTTOM }}
          onClick={handleSetAgencyFilter}
        />
      </div>
      <div
        style={{
          height:
            modeData.length > 1
              ? `${modeData.length * barHeight + 20}px`
              : `${singleBarHeight}px`,
          width: chartWidth,
        }}
      >
        <ResponiveBarComponent
          data={modeData}
          keys={_keys(FEED_COLORS)}
          groupMode={"stacked"}
          indexBy={"mode"}
          layout={"horizontal"}
          colors={({ id, data }) => {
            return FEED_COLORS[id];
          }}
          axisLeft={{ ...AXIS_LEFT, legend: "Transport Modes" }}
          axisBottom={{ ...AXIS_BOTTOM, legend: "Routes" }}
          onClick={(data) => {
            handleModeSelection(data);
          }}
        />
      </div>
    </div>
  );
};

export const Dashboard = () => {
  return (
    <ApolloProvider client={client}>
      <Container>
        <DashboardComponent />
      </Container>
    </ApolloProvider>
  );
};

export default Dashboard;
