import React from "react";

export const FEED_COLORS = {
  LAUTTA: "hsla(92, 33%, 40%, 1)",
  OULU: "hsla(52, 75%, 24%, 1)",
  LINKKI: "hsla(177, 28%, 17%, 1)",
  TAMPERE: "hsla(202, 77%, 42%, 1)",
  HSL: "hsla(157, 14%, 25%, 1)",
  MATKA: "hsla(202, 77%, 19%, 1)"
};

export const MODES = [
  {
    id: "airplane",
    label: "Airplane",
    mode: "AIRPLANE",
    color: "hsl(15.11502368928947, 70%, 50%)"
  },
  {
    id: "bus",
    label: "Bus",
    mode: "BUS",
    color: "hsl(95.11502368928947, 70%, 50%)"
  },
  {
    id: "cable_car",
    label: "Cable Car",
    mode: "CABLE_CAR",
    color: "hsl(355.11502368928947, 70%, 50%)"
  },
  {
    id: "car",
    label: "Car",
    mode: "CAR",
    color: "hsl(5.11502368928947, 70%, 50%)"
  },
  {
    id: "ferry",
    label: "Ferry",
    mode: "FERRY",
    color: "hsl(25.11502368928947, 70%, 50%)"
  },
  {
    id: "funicular",
    label: "Funicular",
    mode: "FUNICULAR",
    color: "hsl(65.11502368928947, 70%, 50%)"
  },
  {
    id: "gondola",
    label: "Gondola",
    mode: "GONDOLA",
    color: "hsl(77.11502368928947, 70%, 50%)"
  },
  {
    id: "rail",
    label: "Rail",
    mode: "RAIL",
    color: "hsl(225.11502368928947, 70%, 50%)"
  },
  {
    id: "subway",
    label: "Subway",
    mode: "SUBWAY",
    color: "hsl(180.11502368928947, 70%, 50%)"
  },
  {
    id: "tram",
    label: "Tram",
    mode: "TRAM",
    color: "hsl(295.11502368928947, 70%, 50%)"
  },
  {
    id: "walk",
    label: "Walk",
    mode: "WALK",
    color: "hsl(111.11502368928947, 70%, 50%)"
  }
];

const CustomTick = tick => {
  // const theme = useTheme();
  // console.log("tick:", tick);
  return (
    <g transform={`translate(${tick.x},${tick.y + 22})`}>
      <text
        textAnchor="middle"
        dominantBaseline="middle"
        style={{
          fill: "darkgray",
          fontSize: 10
        }}
      >
        {/* {tick.value} */}
      </text>
    </g>
  );
};
const CustomLeftTick = tick => {
  // const theme = useTheme();

  return (
    <g transform={`translate(${tick.x - 40},${tick.y})`}>
      <text
        textAnchor="middle"
        dominantBaseline="middle"
        style={{
          fill: "darkgray",
          fontSize: 10
        }}
      >
        {tick.value}
      </text>
    </g>
  );
};

export const AXIS_LEFT = {
  tickSize: 5,
  tickPadding: 5,
  tickRotation: 0,
  //   legend: "Feeds",
  legendPosition: "middle" as "end" | "start" | "middle",
  legendOffset: -100,
  renderTick: CustomLeftTick
};

export const AXIS_BOTTOM = {
  tickSize: 5,
  tickPadding: 5,
  tickRotation: 0,
  //   legend: "Modes",
  legendPosition: "middle" as "end" | "start" | "middle",
  legendOffset: 32,
  renderTick: CustomTick
};

export const PIE_DATA = [
  {
    id: "bikes",
    label: "Bikes Allowed",
    value: 65,
    color: "hsla(98, 56%, 27%, 1)"
  },
  {
    id: "uknown",
    label: "Unknown",
    value: 25,
    color: "hsla(59, 63%, 38%, 1)"
  },
  {
    id: "nobikes",
    label: "Bikes not Allowed",
    value: 10,
    color: "hsla(0, 63%, 38%, 1)"
  }
];
export const PIE_DATA2 = [
  {
    id: "wheelchair",
    label: "Wheelchair Accessible",
    value: 85,
    color: "hsla(98, 56%, 27%, 1)"
  },
  {
    id: "uknown",
    label: "Unknown",
    value: 13,
    color: "hsla(59, 63%, 38%, 1)"
  },
  {
    id: "nowheelchair",
    label: "Not Wheelchair Accessible",
    value: 2,
    color: "hsla(0, 63%, 38%, 1)"
  }
];
