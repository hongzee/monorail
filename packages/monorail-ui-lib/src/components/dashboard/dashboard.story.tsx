import React from "react";
import { withKnobs, radios, number } from "@storybook/addon-knobs";

import Dashboard from "./index";
import PieComponent from "./charts/pie";
import ResponiveBarComponent from "./charts/bar";
import { ResponsiveSunburst } from "@nivo/sunburst";

// import { BarProps } from "@nivo/bar";

export default {
  title: "Dashboard",
  decorators: [withKnobs]
};

const pieData = [
  { id: "HSL", label: "HSL", value: 1 },
  { id: "LINKKI", label: "LINKKI", value: 2 },
  { id: "TAMPERE", label: "TAMPERE", value: 1 },
  { id: "LAUTTA", label: "LAUTTA", value: 22 },
  { id: "OULU", label: "OULU", value: 12 },
  { id: "MATKA", label: "MATKA", value: 312 }
];
const feedFill = [
  { match: { id: "HSL" }, id: "lines" },
  { match: { id: "LINKKI" }, id: "dots" },
  { match: { id: "tampere" }, id: "lines" },
  { match: { id: "lautta" }, id: "dots" },
  { match: { id: "OULU" }, id: "lines" },
  { match: { id: "MATKA" }, id: "dots" }
];

const barData = [
  {
    feed: "HSL",
    agencies: 1,
    routes: 497,
    bus: 458,
    airplane: 0,
    cable_car: 0,
    car: 0,
    ferry: 2,
    funicular: 0,
    gondola: 0,
    rail: 12,
    subway: 4,
    tram: 21,
    walk: 0,
    agenciesColor: "hsl(195.11502368928947, 70%, 50%)",
    routesColor: "hsl(70.44065654250818, 70%, 50%)",
    busColor: "hsl(95.11502368928947, 70%, 50%)",
    airplaneColor: "hsl(15.11502368928947, 70%, 50%)",
    ferryColor: "hsl(25.11502368928947, 70%, 50%)",
    railColor: "hsl(225.11502368928947, 70%, 50%)",
    tramColor: "hsl(295.11502368928947, 70%, 50%)"
  },
  {
    feed: "LINKKI",
    agencies: 2,
    routes: 109,
    bus: 109,
    airplane: 0,
    cable_car: 0,
    car: 0,
    ferry: 0,
    funicular: 0,
    gondola: 0,
    rail: 0,
    subway: 0,
    tram: 0,
    walk: 0,
    agenciesColor: "hsl(195.11502368928947, 70%, 50%)",
    routesColor: "hsl(70.44065654250818, 70%, 50%)",
    busColor: "hsl(95.11502368928947, 70%, 50%)",
    airplaneColor: "hsl(15.11502368928947, 70%, 50%)",
    ferryColor: "hsl(25.11502368928947, 70%, 50%)",
    railColor: "hsl(225.11502368928947, 70%, 50%)",
    tramColor: "hsl(295.11502368928947, 70%, 50%)"
  },
  {
    feed: "tampere",
    agencies: 1,
    routes: 121,
    bus: 121,
    airplane: 0,
    cable_car: 0,
    car: 0,
    ferry: 0,
    funicular: 0,
    gondola: 0,
    rail: 0,
    subway: 0,
    tram: 0,
    walk: 0,
    agenciesColor: "hsl(195.11502368928947, 70%, 50%)",
    routesColor: "hsl(70.44065654250818, 70%, 50%)",
    busColor: "hsl(95.11502368928947, 70%, 50%)",
    airplaneColor: "hsl(15.11502368928947, 70%, 50%)",
    ferryColor: "hsl(25.11502368928947, 70%, 50%)",
    railColor: "hsl(225.11502368928947, 70%, 50%)",
    tramColor: "hsl(295.11502368928947, 70%, 50%)"
  },
  {
    feed: "lautta",
    agencies: 22,
    routes: 88,
    bus: 0,
    airplane: 0,
    cable_car: 0,
    car: 0,
    ferry: 88,
    funicular: 0,
    gondola: 0,
    rail: 0,
    subway: 0,
    tram: 0,
    walk: 0,
    agenciesColor: "hsl(195.11502368928947, 70%, 50%)",
    routesColor: "hsl(70.44065654250818, 70%, 50%)",
    busColor: "hsl(95.11502368928947, 70%, 50%)",
    airplaneColor: "hsl(15.11502368928947, 70%, 50%)",
    ferryColor: "hsl(25.11502368928947, 70%, 50%)",
    railColor: "hsl(225.11502368928947, 70%, 50%)",
    tramColor: "hsl(295.11502368928947, 70%, 50%)"
  },
  {
    feed: "OULU",
    agencies: 12,
    routes: 92,
    bus: 92,
    airplane: 0,
    cable_car: 0,
    car: 0,
    ferry: 0,
    funicular: 0,
    gondola: 0,
    rail: 0,
    subway: 0,
    tram: 0,
    walk: 0,
    agenciesColor: "hsl(195.11502368928947, 70%, 50%)",
    routesColor: "hsl(70.44065654250818, 70%, 50%)",
    busColor: "hsl(95.11502368928947, 70%, 50%)",
    airplaneColor: "hsl(15.11502368928947, 70%, 50%)",
    ferryColor: "hsl(25.11502368928947, 70%, 50%)",
    railColor: "hsl(225.11502368928947, 70%, 50%)",
    tramColor: "hsl(295.11502368928947, 70%, 50%)"
  },
  {
    feed: "MATKA",
    agencies: 312,
    routes: 3706,
    bus: 2694,
    airplane: 405,
    rail: 607,
    agenciesColor: "hsl(195.11502368928947, 70%, 50%)",
    routesColor: "hsl(70.44065654250818, 70%, 50%)",
    busColor: "hsl(95.11502368928947, 70%, 50%)",
    airplaneColor: "hsl(15.11502368928947, 70%, 50%)",
    ferryColor: "hsl(25.11502368928947, 70%, 50%)",
    railColor: "hsl(225.11502368928947, 70%, 50%)",
    tramColor: "hsl(295.11502368928947, 70%, 50%)"
  }
];
const groupMode = "grouped";
const tempKeys = [
  "agencies",
  "routes",
  "bus",
  "airplane",
  "car",
  "ferry",
  "funicular",
  "gondola",
  "rail",
  "subway",
  "tram",
  "walk"
];

export const pieChart = () => (
  <div style={{ height: "300px", width: "700px" }}>
    <PieComponent data={pieData} fill={feedFill} />
  </div>
);

export const barChart = () => {
  const groups = ["GROUP-ID1"];
  const options = {
    Vertical: "vertical",
    Horizontal: "horizontal"
  };
  const groupOptions = {
    Grouped: "grouped",
    Stacked: "stacked"
  };

  const layoutStyle = radios("Select layout", options, "vertical", groups[0]);
  const groupMode = radios(
    "Select groupMode",
    groupOptions,
    "grouped",
    groups[0]
  );

  const rangeOptions = {
    range: true,
    min: 0,
    max: 3750,
    step: 10
  };

  const maxValue = number("Max Value", 3750, rangeOptions, groups[0]);
  const minValue = number("Min Value", 0, rangeOptions, groups[0]);

  return (
    <div style={{ height: "400px", width: "600px" }}>
      <ResponiveBarComponent
        data={barData}
        keys={tempKeys}
        groupMode={groupMode as "grouped" | "stacked"}
        indexBy={"feed"}
        layout={layoutStyle as "vertical" | "horizontal"}
        colors={({ id, data }) => {
          return data[`${id}Color`];
        }}
        maxValue={maxValue}
        minValue={minValue}
        onClick={data => console.log(`clicked on:${JSON.stringify(data)}`)}
        fill={feedFill}
      />
    </div>
  );
};

// export const sunburstChart = () => {
//   return (
//     <div style={{ height: "400px", width: "600px" }}>
//       <ResponsiveSunburst
//         data={sunburstData}
//         margin={{ top: 40, right: 20, bottom: 20, left: 20 }}
//         identity="name"
//         value="loc"
//         cornerRadius={2}
//         borderWidth={1}
//         borderColor="white"
//         colors={{ scheme: "nivo" }}
//         childColor={{ from: "color" }}
//         animate={true}
//         motionStiffness={90}
//         motionDamping={15}
//         isInteractive={true}
//       />
//     </div>
//   );
// };

export const simpleDashboard = () => <Dashboard />;

const sunburstData = {
  name: "nivo",
  color: "hsl(141, 70%, 50%)",
  children: [
    {
      name: "viz",
      color: "hsl(285, 70%, 50%)",
      children: [
        {
          name: "stack",
          color: "hsl(215, 70%, 50%)",
          children: [
            {
              name: "chart",
              color: "hsl(268, 70%, 50%)",
              loc: 59922
            },
            {
              name: "xAxis",
              color: "hsl(47, 70%, 50%)",
              loc: 51943
            },
            {
              name: "yAxis",
              color: "hsl(146, 70%, 50%)",
              loc: 9520
            },
            {
              name: "layers",
              color: "hsl(20, 70%, 50%)",
              loc: 36744
            }
          ]
        },
        {
          name: "pie",
          color: "hsl(251, 70%, 50%)",
          children: [
            {
              name: "chart",
              color: "hsl(302, 70%, 50%)",
              children: [
                {
                  name: "pie",
                  color: "hsl(51, 70%, 50%)",
                  children: [
                    {
                      name: "outline",
                      color: "hsl(357, 70%, 50%)",
                      loc: 34559
                    },
                    {
                      name: "slices",
                      color: "hsl(327, 70%, 50%)",
                      loc: 39101
                    },
                    {
                      name: "bbox",
                      color: "hsl(112, 70%, 50%)",
                      loc: 47529
                    }
                  ]
                },
                {
                  name: "donut",
                  color: "hsl(12, 70%, 50%)",
                  loc: 28300
                },
                {
                  name: "gauge",
                  color: "hsl(88, 70%, 50%)",
                  loc: 78904
                }
              ]
            },
            {
              name: "legends",
              color: "hsl(287, 70%, 50%)",
              loc: 140931
            }
          ]
        }
      ]
    },
    {
      name: "colors",
      color: "hsl(80, 70%, 50%)",
      children: [
        {
          name: "rgb",
          color: "hsl(204, 70%, 50%)",
          loc: 85015
        },
        {
          name: "hsl",
          color: "hsl(62, 70%, 50%)",
          loc: 135009
        }
      ]
    },
    {
      name: "utils",
      color: "hsl(219, 70%, 50%)",
      children: [
        {
          name: "randomize",
          color: "hsl(9, 70%, 50%)",
          loc: 25599
        },
        {
          name: "resetClock",
          color: "hsl(8, 70%, 50%)",
          loc: 107708
        },
        {
          name: "noop",
          color: "hsl(154, 70%, 50%)",
          loc: 73686
        },
        {
          name: "tick",
          color: "hsl(46, 70%, 50%)",
          loc: 187542
        },
        {
          name: "forceGC",
          color: "hsl(312, 70%, 50%)",
          loc: 66937
        },
        {
          name: "stackTrace",
          color: "hsl(182, 70%, 50%)",
          loc: 36146
        },
        {
          name: "dbg",
          color: "hsl(99, 70%, 50%)",
          loc: 50597
        }
      ]
    },
    {
      name: "generators",
      color: "hsl(99, 70%, 50%)",
      children: [
        {
          name: "address",
          color: "hsl(335, 70%, 50%)",
          loc: 154760
        },
        {
          name: "city",
          color: "hsl(41, 70%, 50%)",
          loc: 198117
        },
        {
          name: "animal",
          color: "hsl(278, 70%, 50%)",
          loc: 76478
        },
        {
          name: "movie",
          color: "hsl(284, 70%, 50%)",
          loc: 103332
        },
        {
          name: "user",
          color: "hsl(141, 70%, 50%)",
          loc: 145006
        }
      ]
    },
    {
      name: "set",
      color: "hsl(214, 70%, 50%)",
      children: [
        {
          name: "clone",
          color: "hsl(272, 70%, 50%)",
          loc: 125487
        },
        {
          name: "intersect",
          color: "hsl(122, 70%, 50%)",
          loc: 50813
        },
        {
          name: "merge",
          color: "hsl(283, 70%, 50%)",
          loc: 27708
        },
        {
          name: "reverse",
          color: "hsl(316, 70%, 50%)",
          loc: 192280
        },
        {
          name: "toArray",
          color: "hsl(330, 70%, 50%)",
          loc: 159729
        },
        {
          name: "toObject",
          color: "hsl(118, 70%, 50%)",
          loc: 176695
        },
        {
          name: "fromCSV",
          color: "hsl(194, 70%, 50%)",
          loc: 138224
        },
        {
          name: "slice",
          color: "hsl(262, 70%, 50%)",
          loc: 160521
        },
        {
          name: "append",
          color: "hsl(349, 70%, 50%)",
          loc: 9797
        },
        {
          name: "prepend",
          color: "hsl(45, 70%, 50%)",
          loc: 179732
        },
        {
          name: "shuffle",
          color: "hsl(176, 70%, 50%)",
          loc: 188451
        },
        {
          name: "pick",
          color: "hsl(78, 70%, 50%)",
          loc: 31147
        },
        {
          name: "plouc",
          color: "hsl(145, 70%, 50%)",
          loc: 170923
        }
      ]
    },
    {
      name: "text",
      color: "hsl(234, 70%, 50%)",
      children: [
        {
          name: "trim",
          color: "hsl(337, 70%, 50%)",
          loc: 94514
        },
        {
          name: "slugify",
          color: "hsl(351, 70%, 50%)",
          loc: 32673
        },
        {
          name: "snakeCase",
          color: "hsl(58, 70%, 50%)",
          loc: 44890
        },
        {
          name: "camelCase",
          color: "hsl(208, 70%, 50%)",
          loc: 197001
        },
        {
          name: "repeat",
          color: "hsl(232, 70%, 50%)",
          loc: 27750
        },
        {
          name: "padLeft",
          color: "hsl(3, 70%, 50%)",
          loc: 114628
        },
        {
          name: "padRight",
          color: "hsl(136, 70%, 50%)",
          loc: 170115
        },
        {
          name: "sanitize",
          color: "hsl(273, 70%, 50%)",
          loc: 79999
        },
        {
          name: "ploucify",
          color: "hsl(264, 70%, 50%)",
          loc: 17883
        }
      ]
    },
    {
      name: "misc",
      color: "hsl(1, 70%, 50%)",
      children: [
        {
          name: "whatever",
          color: "hsl(271, 70%, 50%)",
          children: [
            {
              name: "hey",
              color: "hsl(52, 70%, 50%)",
              loc: 172367
            },
            {
              name: "WTF",
              color: "hsl(242, 70%, 50%)",
              loc: 36757
            },
            {
              name: "lol",
              color: "hsl(163, 70%, 50%)",
              loc: 179664
            },
            {
              name: "IMHO",
              color: "hsl(215, 70%, 50%)",
              loc: 19265
            }
          ]
        },
        {
          name: "other",
          color: "hsl(34, 70%, 50%)",
          loc: 100067
        },
        {
          name: "crap",
          color: "hsl(309, 70%, 50%)",
          children: [
            {
              name: "crapA",
              color: "hsl(309, 70%, 50%)",
              loc: 175244
            },
            {
              name: "crapB",
              color: "hsl(177, 70%, 50%)",
              children: [
                {
                  name: "crapB1",
                  color: "hsl(212, 70%, 50%)",
                  loc: 131804
                },
                {
                  name: "crapB2",
                  color: "hsl(338, 70%, 50%)",
                  loc: 106262
                },
                {
                  name: "crapB3",
                  color: "hsl(242, 70%, 50%)",
                  loc: 115014
                },
                {
                  name: "crapB4",
                  color: "hsl(44, 70%, 50%)",
                  loc: 57977
                }
              ]
            },
            {
              name: "crapC",
              color: "hsl(59, 70%, 50%)",
              children: [
                {
                  name: "crapC1",
                  color: "hsl(176, 70%, 50%)",
                  loc: 22217
                },
                {
                  name: "crapC2",
                  color: "hsl(126, 70%, 50%)",
                  loc: 197338
                },
                {
                  name: "crapC3",
                  color: "hsl(266, 70%, 50%)",
                  loc: 73438
                },
                {
                  name: "crapC4",
                  color: "hsl(17, 70%, 50%)",
                  loc: 39372
                },
                {
                  name: "crapC5",
                  color: "hsl(294, 70%, 50%)",
                  loc: 98726
                },
                {
                  name: "crapC6",
                  color: "hsl(183, 70%, 50%)",
                  loc: 188527
                },
                {
                  name: "crapC7",
                  color: "hsl(319, 70%, 50%)",
                  loc: 6298
                },
                {
                  name: "crapC8",
                  color: "hsl(295, 70%, 50%)",
                  loc: 127628
                },
                {
                  name: "crapC9",
                  color: "hsl(234, 70%, 50%)",
                  loc: 106767
                }
              ]
            }
          ]
        }
      ]
    }
  ]
};
