import React from "react";
import { withKnobs, radios, number } from "@storybook/addon-knobs";
import BetterTooltip from "./index";
import Button from "@material-ui/core/Button";
import PopperCard from "./popperCard";
// import { BarProps } from "@nivo/bar";

export default {
  title: "Better Tooltip",
  decorators: [withKnobs]
};

export const hoverText = () => (
  <BetterTooltip mode={"hover"} title="super duper tooltip">
    <Button>Hover over me</Button>
  </BetterTooltip>
);

export const hoverComponent = () => (
  <BetterTooltip mode={"hover"} title={<PopperCard />} arrow interactive>
    <Button>Hover over me</Button>
  </BetterTooltip>
);

export const clickableTextContent = () => (
  <BetterTooltip mode={"click"} title="super duper tooltip">
    <Button>Click Me!</Button>
  </BetterTooltip>
);
export const clickableComponent = () => (
  <BetterTooltip mode={"click"} title={<PopperCard />} arrow interactive>
    <Button>Click Me!</Button>
  </BetterTooltip>
);
