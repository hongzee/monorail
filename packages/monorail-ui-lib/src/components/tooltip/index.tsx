import React, { useState } from "react";
import Tooltip from "@material-ui/core/Tooltip";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import { TooltipProps } from "@material-ui/core/Tooltip";

export interface BetterToolTipProps extends TooltipProps {
  mode: "hover" | "click";
  // content?: JSX.Element | string;
  // children: JSX.Element;
  // arrow?: boolean;
}

export const BetterTooltip = (props: BetterToolTipProps) => {
  const [open, setOpen] = useState(false);
  const { mode, children, title, arrow = false } = props;

  const handleTooltipClose = event => {
    // console.log("event:", event);
    setOpen(false);
  };

  const handleTooltipOpen = () => {
    setOpen(true);
  };

  const clickableTooltipProps =
    mode === "click"
      ? {
          open: open
        }
      : {};

  const commonTooltipProps = {
    disableFocusListener: true,
    disableTouchListener: true,
    disableHoverListener: mode === "hover" ? false : true,
    title: title,
    arrow: arrow
  };

  return (
    <div onClick={handleTooltipOpen}>
      <ClickAwayListener onClickAway={handleTooltipClose}>
        <Tooltip
          PopperProps={{
            disablePortal: true
          }}
          onClose={handleTooltipClose}
          {...clickableTooltipProps}
          {...commonTooltipProps}
        >
          {children}
        </Tooltip>
      </ClickAwayListener>
    </div>
  );
};

export default BetterTooltip;
