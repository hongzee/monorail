import React from "react";
import Breadcrumbs from "@material-ui/core/Breadcrumbs";
import { Theme, emphasize, withStyles } from "@material-ui/core/styles";
import Chip from "@material-ui/core/Chip";

const StyledBreadcrumb = withStyles((theme: Theme) => ({
  root: {
    backgroundColor: theme.palette.grey[800],
    height: theme.spacing(3),
    color: "darkgoldenrod",
    fontSize: 10,
    fontWeight: theme.typography.fontWeightRegular,
    "&:hover, &:focus": {
      backgroundColor: theme.palette.grey[300]
    },
    "&:active": {
      boxShadow: theme.shadows[1],
      backgroundColor: emphasize(theme.palette.grey[300], 0.12)
    }
  }
}))(Chip) as typeof Chip;

export interface BreadCrumb {
  label: string;
  icon: JSX.Element;
  type: "icon" | "deleteIcon";
  callback: any;
}

export interface BreadCrumbProps {
  crumbs: Array<BreadCrumb>;
}

const BreadCrumbs = (props: BreadCrumbProps) => {
  const { crumbs = [] } = props;

  return (
    <Breadcrumbs aria-label="breadcrumb">
      {crumbs.map((crumb, index) => {
        let crumbProps = {};
        if (crumb.type === "icon") {
          crumbProps["icon"] = crumb.icon;
        } else {
          crumbProps["deleteIcon"] = crumb.icon;
          crumbProps["onDelete"] = crumb.callback;
        }

        return (
          <StyledBreadcrumb
            key={index}
            label={crumb.label}
            onClick={crumb.callback}
            {...crumbProps}
          />
        );
      })}
    </Breadcrumbs>
  );
};

export default BreadCrumbs;
