import React from "react";
import { withKnobs, radios, number } from "@storybook/addon-knobs";
import _orderBy from "lodash/orderBy";
import Histogram from "./index";

export default {
  title: "Histogram",
  decorators: [withKnobs]
};

const chartTheme = {
  tooltip: {
    container: {
      fontSize: 12,
      color: "white",
      background: "dimgray",
      borderRadius: "2px",
      boxShadow: "0 1px 2px rgba(0, 0, 0, 0.25)",
      padding: "5px 9px"
    }
  },
  axis: {
    legend: {
      text: {
        fontSize: 11,
        fill: "darkgoldenrod"
      }
    }
  }
};

const statusData = _orderBy(
  [
    {
      Status: "Alive",
      Count: 281,
      color: "orange"
    },
    { Status: "Dead", Count: 147, color: "orange" },
    { Status: "Unknown", Count: 65, color: "orange" }
  ],
  ["Count", "asc"]
);
const genderData = _orderBy(
  [
    {
      Gender: "Male",
      Count: 371,
      color: "orange"
    },
    { Gender: "Female", Count: 74, color: "orange" },
    { Gender: "Unknown", Count: 42, color: "orange" },
    { Gender: "Genderless", Count: 6, color: "orange" }
  ],
  ["Count", "asc"]
);

const speciesData = _orderBy(
  [
    {
      Species: "Human",
      Count: 297,
      color: "orange"
    },
    { Species: "Alien", Count: 132, color: "orange" },
    { Species: "Humanoid", Count: 53, color: "orange" },
    { Species: "Animal", Count: 17, color: "orange" },
    { Species: "Robot", Count: 11, color: "orange" },
    { Species: "Cronenberg", Count: 8, color: "orange" },
    { Species: "Mytholog", Count: 7, color: "orange" },
    { Species: "Poopybutthole", Count: 6, color: "orange" },
    { Species: "Disease", Count: 6, color: "orange" },
    { Species: "Unknown", Count: 5, color: "orange" },
    { Species: "Vampire", Count: 3, color: "orange" },
    { Species: "Parasite", Count: 1, color: "orange" }
  ],
  ["Count", "asc"]
);

const data = [
  {
    label: "Gender",
    data: genderData,
    keys: ["Count"],
    indexBy: "Gender",
    groupMode: "stacked" as "grouped" | "stacked",
    barHeight: 25,
    colors: ({ id, data }) => data.color,
    onClick: data => console.log("Gender", data),
    onClear: data => console.log("Clear Gender")
  },
  {
    label: "Status",
    data: statusData,
    keys: ["Count"],
    indexBy: "Status",
    groupMode: "stacked" as "grouped" | "stacked",
    barHeight: 25,
    colors: ({ id, data }) => data.color,
    onClick: data => console.log("Status", data),
    onClear: data => console.log("Clear Status")
  },
  {
    label: "Species",
    data: speciesData,
    keys: ["Count"],
    indexBy: "Species",
    groupMode: "stacked" as "grouped" | "stacked",
    barHeight: 25,
    colors: ({ id, data }) => data.color,
    onClick: data => console.log("Species", data),
    onClear: data => console.log("Clear Species")
  }
];

export const histogram = () => {
  return (
    <div
      style={{ height: "500px", width: "700px", backgroundColor: "#303030" }}
    >
      <Histogram data={data} theme={chartTheme} width={500} onHome={() => {}} />
    </div>
  );
};
