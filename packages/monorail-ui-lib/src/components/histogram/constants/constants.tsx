import React from "react";

const CustomTick = tick => {
  // const theme = useTheme();
  // console.log("tick:", tick);
  return (
    <g transform={`translate(${tick.x},${tick.y + 22})`}>
      <text
        textAnchor="middle"
        dominantBaseline="middle"
        style={{
          fill: "darkgray",
          fontSize: 10
        }}
      >
        {/* {tick.value} */}
      </text>
    </g>
  );
};
const CustomLeftTick = tick => {
  // const theme = useTheme();

  return (
    <g transform={`translate(${tick.x - 40},${tick.y})`}>
      <text
        textAnchor="middle"
        dominantBaseline="middle"
        style={{
          fill: "darkgray",
          fontSize: 10
        }}
      >
        {tick.value}
      </text>
    </g>
  );
};

export const AXIS_LEFT = {
  tickSize: 5,
  tickPadding: 5,
  tickRotation: 0,
  // legend: "Gender",
  legendPosition: "middle" as "end" | "start" | "middle",
  legendOffset: -100,
  renderTick: CustomLeftTick
};

export const AXIS_BOTTOM = {
  tickSize: 5,
  tickPadding: 5,
  tickRotation: 0,
  //   legend: "Modes",
  legendPosition: "middle" as "end" | "start" | "middle",
  legendOffset: 32,
  renderTick: CustomTick
};
