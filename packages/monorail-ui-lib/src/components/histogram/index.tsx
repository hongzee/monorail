import React, { useState, useEffect } from "react";

import Container from "@material-ui/core/Container";
import HomeIcon from "@material-ui/icons/Home";
import Close from "@material-ui/icons/Close";
import Grid from "@material-ui/core/Grid";

import { ResponsiveBar } from "@nivo/bar";
import { OrdinalColorsInstruction } from "@nivo/colors";
import { Theme } from "@nivo/core";
import {
  makeStyles,
  createStyles,
  Theme as MaterialTheme,
  // fade
} from "@material-ui/core/styles";

import _keys from "lodash/keys";
import _orderBy from "lodash/orderBy";
import _flatten from "lodash/flatten";
import _isNil from "lodash/isNil";
import _find from "lodash/find";
import _countBy from "lodash/countBy";
import _mergeWith from "lodash/mergeWith";
import _pick from "lodash/pick";
import NIVO_DEFAULT_THEME from "./constants/defaultTheme";

import { AXIS_BOTTOM, AXIS_LEFT } from "./constants/constants";

import BreadCrumbs from "./breadcrumbs";

const useStyles = makeStyles((theme: MaterialTheme) =>
  createStyles({
    histogramContainer: {
      padding: "15px",
    },
    breadCrumbContainer: {
      paddingLeft: "65px",
      paddingBottom: "20px",
    },
  })
);

export interface HistogramData {
  label: string;
  data: Array<any>;
  keys: Array<string>;
  indexBy: string;
  sort?: string;
  groupMode?: "grouped" | "stacked";
  barHeight?: number;
  colors?: OrdinalColorsInstruction;
  onClick?: (datum: any) => void;
  onClear?: (datum: any) => void;
}
export interface BreadCrumbProps {
  data: any;
  group: string;
}

export interface HistogramProps {
  data: Array<HistogramData>;
  theme?: Theme;
  width?: number;
  onHome: () => void;
  filterData?: boolean;
}

export const Histogram = (props: HistogramProps) => {
  const [breadCrumbs, setBreadCrumbs] = useState([]);
  const [barData, setBarData] = useState([]);
  const { data, theme, width, onHome, filterData = true } = props;
  const classes = useStyles();

  //Defaults
  const DEFAULT_BAR_HEIGHT = 20;
  const SINGLE_BAR_HEIGHT = 35;
  const DEFAULT_WIDTH = "600px";
  const DEFAULT_THEME = NIVO_DEFAULT_THEME as Theme;
  const DEFAULT_COLORS = { scheme: "nivo" } as OrdinalColorsInstruction;

  useEffect(() => {
    if (data) {
      //Filter data if there are breadcrumbs
      let filteredData = data;
      if (filterData && breadCrumbs.length > 1) {
        //We have non home breadcrumbs
        filteredData = data.map((d) => {
          const matchingCrumb = breadCrumbs.filter(
            (crumb) => crumb.indexBy === d.indexBy
          );
          if (matchingCrumb.length === 1) {
            //filter
            return {
              ...d,
              data: d.data.filter(
                (item) => item[d.indexBy] === matchingCrumb[0].label
              ),
            };
          }
          return d;
        });
      }
      if (breadCrumbs.length === 0) {
        setBreadCrumbs([homeCrumb]);
      }
      setBarData(filteredData);
    }
  }, [data, breadCrumbs]);

  const barColorScheme = ({ id, data }) => {
    return data[`${id}Color`];
  };

  const handleReset = () => {
    setBreadCrumbs([homeCrumb]);
    onHome();
  };

  const handleClearCrumb = (data, onClear) => {
    onClear(data);

    setBreadCrumbs((prev) =>
      prev.filter((crumb) => crumb.label !== data.indexValue)
    );
  };

  const handleOnClick = (data, indexBy, onClick, onClear) => {
    const newCrumb = {
      label: data.indexValue,
      indexBy: indexBy,
      icon: <Close />,
      type: "deleteIcon",
      callback: () => handleClearCrumb(data, onClear),
    };
    //Currently only 1 crumb per category
    const existingCategoryCrumb =
      breadCrumbs.filter((crumb) => crumb.indexBy === newCrumb.indexBy).length >
      0;
    if (!existingCategoryCrumb) {
      setBreadCrumbs((prev) => prev.concat(newCrumb));
      onClick(data);
    }
  };

  const homeCrumb = {
    label: "Home",
    icon: <HomeIcon fontSize="small" />,
    type: "icon",
    callback: handleReset,
  };
  // };

  return (
    <Container className={classes.histogramContainer}>
      <Container className={classes.breadCrumbContainer}>
        <BreadCrumbs crumbs={breadCrumbs} />
      </Container>
      <Grid
        container
        direction="column"
        justify="flex-start"
        alignItems="flex-start"
      >
        {barData.map((d, index) => {
          const chartBarHeight = d.barHeight ? d.barHeight : DEFAULT_BAR_HEIGHT;
          return (
            <Grid item xs={12} key={index}>
              <div
                style={{
                  height:
                    d.data.length > 1
                      ? `${d.data.length * chartBarHeight}px`
                      : `${SINGLE_BAR_HEIGHT}px`,
                  width: width ? width : DEFAULT_WIDTH,
                }}
              >
                <ResponsiveBar
                  data={d.data}
                  keys={d.keys}
                  groupMode={d.groupMode}
                  indexBy={d.indexBy}
                  layout={"horizontal"}
                  colors={d.colors ? d.colors : DEFAULT_COLORS}
                  theme={theme ? theme : DEFAULT_THEME}
                  axisLeft={{ ...AXIS_LEFT, legend: d.label }}
                  axisBottom={{ ...AXIS_BOTTOM }}
                  onClick={(data) =>
                    handleOnClick(data, d.indexBy, d.onClick, d.onClear)
                  }
                  margin={{ top: 0, right: 10, bottom: 0, left: 125 }}
                  padding={0.3}
                  borderColor={{ from: "color", modifiers: [["darker", 1.6]] }}
                  axisTop={null}
                  axisRight={null}
                  labelSkipWidth={12}
                  labelSkipHeight={12}
                  labelTextColor={{
                    from: "color",
                    modifiers: [["darker", 1.6]],
                  }}
                  animate={true}
                  motionStiffness={90}
                  motionDamping={15}
                  enableGridX={false}
                  enableGridY={false}
                />
              </div>
            </Grid>
          );
        })}
      </Grid>
    </Container>
  );
};

export default Histogram;
