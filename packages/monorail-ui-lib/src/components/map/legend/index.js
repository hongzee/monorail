import React, { useRef, useEffect } from 'react';
import * as d3 from 'd3';
import keyMirror from 'keymirror';
import _range from 'lodash/range';

import { scaleLinear, scaleQuantize, scaleQuantile, scaleOrdinal, scaleSqrt, scaleLog, scalePoint } from 'd3-scale';

import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import SvgIcon from '@material-ui/core/SvgIcon';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import FolderIcon from '@material-ui/icons/Folder';

import { legendColor } from 'd3-svg-legend';

const useStyles = makeStyles((theme) => ({
  iconsPaper: {
    textAlign: 'center',
    fontFamily: 'sans-serif',
   
  },
  icon: { display: 'block', marginLeft: 'auto', marginRight: 'auto' },
  
}));

const ROW_H = 10;
const GAP = 4;

export const SCALE_TYPES = keyMirror({
  ordinal: null,
  quantile: null,
  quantize: null,
  linear: null,
  sqrt: null,
  log: null,

  // ordinal domain to linear range
  point: null,
});

//TODO expose colour scheme groups as CONST keys

const SCALE_FUNC = {
  [SCALE_TYPES.linear]: scaleLinear,
  [SCALE_TYPES.quantize]: scaleQuantize,
  [SCALE_TYPES.quantile]: scaleQuantile,
  [SCALE_TYPES.ordinal]: scaleOrdinal,
  [SCALE_TYPES.sqrt]: scaleSqrt,
  [SCALE_TYPES.log]: scaleLog,
  [SCALE_TYPES.point]: scalePoint,
};

export const MapLegend = (props) => {
  const { width, scaleType = SCALE_TYPES.ordinal, domain, range, displayLabel = true } = props;
  const data = [].concat(domain);
  const scaleFunction = SCALE_FUNC[scaleType];
  const colourScale = scaleFunction(range).domain(domain);
  const height = data.length * (ROW_H + GAP);
  const classes = useStyles();

  return (
    <Paper className={classes.iconsPaper} >
      <svg width={width - 24} height={height}>
        {data.map((item, idx) => (
          <LegendIconRow key={idx} label={item} displayLabel={displayLabel} color={colourScale(item)} idx={idx} />
        ))}
      </svg>
      <Typography style={{ textAlign: 'center' }} display='block' variant='caption'>
        foo
      </Typography>
    </Paper>
  );
};


export const ScaleSequential = (props) => {
  const { domain = [0,100], colorRange = d3.interpolateRainbow, shape = '', path = '', title = '' } = props;
  const scale = d3.scaleSequential(d3.interpolateRainbow).domain(domain);
  const classes = useStyles();

const range = _range(domain[0], domain[1], (domain[1]-domain[0])/100);

  return (
    <Paper className={classes.iconsPaper}>
      {/* <List dense={true} disablePadding={true}> */}
        {range.map((d) => {
          const color = scale(d);
          return (
            // <ListItem key={d} disableGutters={true}>
            //   <ListItemIcon>
            //     <Icon className={classes.icon} style={{ color: `${color}` }} shape={rectangleSVG} />
            //   </ListItemIcon>
            //   <ListItemText
            //     primary={
            //       <Typography type='body2' style={{ fontSize: '12px', fontWeight: 'bold' }}>
            //         {d}
            //       </Typography>
            //     }
            //   />
            // </ListItem>
            <div style={{height:'2px', width: '14px', backgroundColor:`${color}`}}/>
          );
        })}
      {/* </List> */}
    </Paper>
  );
};

export const ScaleOrdinal = (props) => {
  const { domain = [''], colorRange = d3.schemeCategory10, path = '', title = '' } = props;
  const classes = useStyles();
  const scale = d3.scaleOrdinal().domain(domain).range(colorRange);
  return (
    <Paper className={classes.iconsPaper}>
      <List dense={true}>
        {domain.map((d) => {
          const color = scale(d);
          return (
            <ListItem key={d}>
              <ListItemIcon>
                <Icon className={classes.icon} style={{ color: `${color}` }} shape={path} />
              </ListItemIcon>
              <ListItemText
                primary={
                  <Typography type='body2' style={{ fontSize: '12px', fontWeight: 'bold' }}>
                    {d}
                  </Typography>
                }
              />
            </ListItem>
          );
        })}
      </List>
    </Paper>
  );
};

export const LegendIconRow = (props) => {
  return (
    <span>
      <Icon {...props} />
      <LegendLabel {...props} />
    </span>
  );
};

export const Icon = (props) => {
  const { shape } = props;
  return (
    <SvgIcon {...props}>
      <path d={`${shape}`} />
    </SvgIcon>
  );
};

export const LegendLabel = ({ label = '' }) => {
  return <Typography component={'span'}>{label}</Typography>;
};
