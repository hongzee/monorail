import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import { array, optionsKnob as options, withKnobs } from '@storybook/addon-knobs';
import colorbrewer from 'colorbrewer';
import keyMirror from 'keymirror';
import _reduce from 'lodash/reduce';
import React from 'react';
import { ScaleOrdinal, ScaleSequential } from './index';
import { Paper, Typography, Divider } from '@material-ui/core/';

export default {
  title: 'Legend',
  decorators: [withKnobs],
};

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    padding: `0 ${theme.spacing(3)}px ${theme.spacing(3)}px ${theme.spacing(3)}px`,
  },
}));

const KNOB_GROUP = 'legend controls';

const PATHS = {
  triangle: 'M1,21H23L12,2',
  square: 'M3,3V21H21V3',
  rectangle: 'M4,6V19H20V6H4Z',
  circle: 'M12,2A10,10 0 0,0 2,12A10,10 0 0,0 12,22A10,10 0 0,0 22,12A10,10 0 0,0 12,2Z',
  person:
    'M3 5v14c0 1.1.89 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2H5c-1.11 0-2 .9-2 2zm12 4c0 1.66-1.34 3-3 3s-3-1.34-3-3 1.34-3 3-3 3 1.34 3 3zm-9 8c0-2 4-3.1 6-3.1s6 1.1 6 3.1v1H6v-1z',
  car:
    'M18.92 5.01C18.72 4.42 18.16 4 17.5 4h-11c-.66 0-1.21.42-1.42 1.01L3 11v8c0 .55.45 1 1 1h1c.55 0 1-.45 1-1v-1h12v1c0 .55.45 1 1 1h1c.55 0 1-.45 1-1v-8l-2.08-5.99zM6.5 15c-.83 0-1.5-.67-1.5-1.5S5.67 12 6.5 12s1.5.67 1.5 1.5S7.33 15 6.5 15zm11 0c-.83 0-1.5-.67-1.5-1.5s.67-1.5 1.5-1.5 1.5.67 1.5 1.5-.67 1.5-1.5 1.5zM5 10l1.5-4.5h11L19 10H5z',
  nav: 'M12 2L4.5 20.29l.71.71L12 18l6.79 3 .71-.71z',
  phone:
    'M20.1 7.7l-1 1c1.8 1.8 1.8 4.6 0 6.5l1 1c2.5-2.3 2.5-6.1 0-8.5zM18 9.8l-1 1c.5.7.5 1.6 0 2.3l1 1c1.2-1.2 1.2-3 0-4.3zM14 1H4c-1.1 0-2 .9-2 2v18c0 1.1.9 2 2 2h10c1.1 0 2-.9 2-2V3c0-1.1-.9-2-2-2zm0 19H4V4h10v16z',
};

const getValueOfField = (fieldName, obj) => {
  return obj[fieldName];
};

// const createKeysObj = (o)

const getDataDomain = (data, valueAccessor) => {
  return data.map(valueAccessor);
};

const colourSchemeGroupKeys = keyMirror(colorbrewer.schemeGroups);

console.log('colorbrewer.schemeGroups:', colorbrewer.schemeGroups);

export const Scale_Ordinal = () => {
  //to be move tothe component
  const classes = useStyles();

  const defaultDomainData = ['Alice', 'Bob', 'Charlie', 'Eve'];
  const domainData = array('data', defaultDomainData, ',', KNOB_GROUP);

  const schemeGroup = options(
    'scheme group',
    colourSchemeGroupKeys,
    colourSchemeGroupKeys.qualitative,
    { display: 'inline-radio' },
    KNOB_GROUP
  );

  const colorScalesKeys = colorbrewer.schemeGroups[schemeGroup];

  const colourScalesKeyObj = _reduce(
    colorScalesKeys,
    function (result, value, key) {
      result[value] = value;
      return result;
    },
    {}
  );

  const defaultScale = Object.values(colourScalesKeyObj)[2];

  const colorScaleName = options(
    'colour scheme',
    colourScalesKeyObj,
    defaultScale,
    { display: 'inline-radio' },
    KNOB_GROUP
  );

  const colorRange = colorbrewer[colorScaleName][domainData.length];

  const symbols = {
    triangle: { label: 'Triangle', shape: 'path', path: PATHS.triangle },
    square: { label: 'Square', shape: 'path', path: PATHS.square },
    circle: { label: 'Circle', shape: 'path', path: PATHS.circle },
    rectangle: { label: 'Rectangle', shape: 'path', path: PATHS.rectangle },
    person: { label: 'Person', shape: 'path', path: PATHS.person },
    car: { label: 'Car', shape: 'path', path: PATHS.car },
    nav: { label: 'Nav', shape: 'path', path: PATHS.nav },
    phone: { label: 'Phone', shape: 'path', path: PATHS.phone },
  };

  return (
    <div
      style={{
        //backgroundColor: '#303030',
        fill: 'black',
        fontFamily: 'sans-serif',
        fontSize: '12px',
        fontWeight: 'bold',
      }}
    >
      <Grid container className={classes.root} spacing={2}>
        <Grid item xs={12}>
          <Grid container justify='flex-start' spacing={2}>
            {Object.keys(symbols).map((k) => (
              <Grid key={`grid_${k}`} item>
                <ScaleOrdinal key={k} domain={domainData} path={symbols[k].path} colorRange={colorRange} />
              </Grid>
            ))}
          </Grid>
        </Grid>
      </Grid>
    </div>
  );
};



export const ColourSchemes = () => {
  return (
    <div>
      {Object.keys(colourSchemeGroupKeys).map((k, i) => {
        return (
          <Grid container spacing={3}>
            <Grid item xs={12}>
              <Typography variant='h4'>{k}</Typography>
            </Grid>
            {colorbrewer.schemeGroups[k].map((scheme) => {
              return (
                <Grid item xs={1}>
                  <Typography variant='body1'>{scheme}</Typography>
                  {colorbrewer[scheme][8].map((c) => {
                    return (
                      <Paper
                        key={`${scheme}_${c}`}
                        square={true}
                        style={{ width: '25px', backgroundColor: `${c}`, height: '35px' }}
                      />
                    );
                  })}
                </Grid>
              );
            })}
          </Grid>
          
        );
      })}
    </div>
  );
};
