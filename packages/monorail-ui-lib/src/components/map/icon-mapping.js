export const ICON_MAPPING = {
	'3d_rotation': {
		x: 0,
		y: 0,
		width: 36,
		height: 36,
		mask: true
	},
	ac_unit: {
		x: 36,
		y: 0,
		width: 36,
		height: 36,
		mask: true
	},
	access_alarm: {
		x: 72,
		y: 0,
		width: 36,
		height: 36,
		mask: true
	},
	access_alarms: {
		x: 108,
		y: 0,
		width: 36,
		height: 36,
		mask: true
	},
	access_time: {
		x: 144,
		y: 0,
		width: 36,
		height: 36,
		mask: true
	},
	accessibility: {
		x: 180,
		y: 0,
		width: 36,
		height: 36,
		mask: true
	},
	accessible: {
		x: 216,
		y: 0,
		width: 36,
		height: 36,
		mask: true
	},
	account_balance: {
		x: 252,
		y: 0,
		width: 36,
		height: 36,
		mask: true
	},
	account_balance_wallet: {
		x: 288,
		y: 0,
		width: 36,
		height: 36,
		mask: true
	},
	account_box: {
		x: 324,
		y: 0,
		width: 36,
		height: 36,
		mask: true
	},
	account_circle: {
		x: 360,
		y: 0,
		width: 36,
		height: 36,
		mask: true
	},
	adb: {
		x: 396,
		y: 0,
		width: 36,
		height: 36,
		mask: true
	},
	add: {
		x: 432,
		y: 0,
		width: 36,
		height: 36,
		mask: true
	},
	add_a_photo: {
		x: 468,
		y: 0,
		width: 36,
		height: 36,
		mask: true
	},
	add_alarm: {
		x: 504,
		y: 0,
		width: 36,
		height: 36,
		mask: true
	},
	add_alert: {
		x: 540,
		y: 0,
		width: 36,
		height: 36,
		mask: true
	},
	add_box: {
		x: 576,
		y: 0,
		width: 36,
		height: 36,
		mask: true
	},
	add_circle: {
		x: 612,
		y: 0,
		width: 36,
		height: 36,
		mask: true
	},
	add_circle_outline: {
		x: 648,
		y: 0,
		width: 36,
		height: 36,
		mask: true
	},
	add_location: {
		x: 684,
		y: 0,
		width: 36,
		height: 36,
		mask: true
	},
	add_shopping_cart: {
		x: 720,
		y: 0,
		width: 36,
		height: 36,
		mask: true
	},
	add_to_photos: {
		x: 756,
		y: 0,
		width: 36,
		height: 36,
		mask: true
	},
	add_to_queue: {
		x: 792,
		y: 0,
		width: 36,
		height: 36,
		mask: true
	},
	adjust: {
		x: 828,
		y: 0,
		width: 36,
		height: 36,
		mask: true
	},
	airline_seat_flat: {
		x: 864,
		y: 0,
		width: 36,
		height: 36,
		mask: true
	},
	airline_seat_flat_angled: {
		x: 900,
		y: 0,
		width: 36,
		height: 36,
		mask: true
	},
	airline_seat_individual_suite: {
		x: 936,
		y: 0,
		width: 36,
		height: 36,
		mask: true
	},
	airline_seat_legroom_extra: {
		x: 972,
		y: 0,
		width: 36,
		height: 36,
		mask: true
	},
	airline_seat_legroom_normal: {
		x: 0,
		y: 36,
		width: 36,
		height: 36,
		mask: true
	},
	airline_seat_legroom_reduced: {
		x: 36,
		y: 36,
		width: 36,
		height: 36,
		mask: true
	},
	airline_seat_recline_extra: {
		x: 72,
		y: 36,
		width: 36,
		height: 36,
		mask: true
	},
	airline_seat_recline_normal: {
		x: 108,
		y: 36,
		width: 36,
		height: 36,
		mask: true
	},
	airplanemode_active: {
		x: 144,
		y: 36,
		width: 36,
		height: 36,
		mask: true
	},
	airplanemode_inactive: {
		x: 180,
		y: 36,
		width: 36,
		height: 36,
		mask: true
	},
	airplay: {
		x: 216,
		y: 36,
		width: 36,
		height: 36,
		mask: true
	},
	airport_shuttle: {
		x: 252,
		y: 36,
		width: 36,
		height: 36,
		mask: true
	},
	alarm: {
		x: 288,
		y: 36,
		width: 36,
		height: 36,
		mask: true
	},
	alarm_add: {
		x: 324,
		y: 36,
		width: 36,
		height: 36,
		mask: true
	},
	alarm_off: {
		x: 360,
		y: 36,
		width: 36,
		height: 36,
		mask: true
	},
	alarm_on: {
		x: 396,
		y: 36,
		width: 36,
		height: 36,
		mask: true
	},
	album: {
		x: 432,
		y: 36,
		width: 36,
		height: 36,
		mask: true
	},
	all_inclusive: {
		x: 468,
		y: 36,
		width: 36,
		height: 36,
		mask: true
	},
	all_out: {
		x: 504,
		y: 36,
		width: 36,
		height: 36,
		mask: true
	},
	android: {
		x: 540,
		y: 36,
		width: 36,
		height: 36,
		mask: true
	},
	announcement: {
		x: 576,
		y: 36,
		width: 36,
		height: 36,
		mask: true
	},
	apps: {
		x: 612,
		y: 36,
		width: 36,
		height: 36,
		mask: true
	},
	archive: {
		x: 648,
		y: 36,
		width: 36,
		height: 36,
		mask: true
	},
	arrow_back: {
		x: 684,
		y: 36,
		width: 36,
		height: 36,
		mask: true
	},
	arrow_downward: {
		x: 720,
		y: 36,
		width: 36,
		height: 36,
		mask: true
	},
	arrow_drop_down: {
		x: 756,
		y: 36,
		width: 36,
		height: 36,
		mask: true
	},
	arrow_drop_down_circle: {
		x: 792,
		y: 36,
		width: 36,
		height: 36,
		mask: true
	},
	arrow_drop_up: {
		x: 828,
		y: 36,
		width: 36,
		height: 36,
		mask: true
	},
	arrow_forward: {
		x: 864,
		y: 36,
		width: 36,
		height: 36,
		mask: true
	},
	arrow_upward: {
		x: 900,
		y: 36,
		width: 36,
		height: 36,
		mask: true
	},
	art_track: {
		x: 936,
		y: 36,
		width: 36,
		height: 36,
		mask: true
	},
	aspect_ratio: {
		x: 972,
		y: 36,
		width: 36,
		height: 36,
		mask: true
	},
	assessment: {
		x: 0,
		y: 72,
		width: 36,
		height: 36,
		mask: true
	},
	assignment: {
		x: 36,
		y: 72,
		width: 36,
		height: 36,
		mask: true
	},
	assignment_ind: {
		x: 72,
		y: 72,
		width: 36,
		height: 36,
		mask: true
	},
	assignment_late: {
		x: 108,
		y: 72,
		width: 36,
		height: 36,
		mask: true
	},
	assignment_return: {
		x: 144,
		y: 72,
		width: 36,
		height: 36,
		mask: true
	},
	assignment_returned: {
		x: 180,
		y: 72,
		width: 36,
		height: 36,
		mask: true
	},
	assignment_turned_in: {
		x: 216,
		y: 72,
		width: 36,
		height: 36,
		mask: true
	},
	assistant: {
		x: 252,
		y: 72,
		width: 36,
		height: 36,
		mask: true
	},
	assistant_photo: {
		x: 288,
		y: 72,
		width: 36,
		height: 36,
		mask: true
	},
	attach_file: {
		x: 324,
		y: 72,
		width: 36,
		height: 36,
		mask: true
	},
	attach_money: {
		x: 360,
		y: 72,
		width: 36,
		height: 36,
		mask: true
	},
	attachment: {
		x: 396,
		y: 72,
		width: 36,
		height: 36,
		mask: true
	},
	audiotrack: {
		x: 432,
		y: 72,
		width: 36,
		height: 36,
		mask: true
	},
	autorenew: {
		x: 468,
		y: 72,
		width: 36,
		height: 36,
		mask: true
	},
	av_timer: {
		x: 504,
		y: 72,
		width: 36,
		height: 36,
		mask: true
	},
	backspace: {
		x: 540,
		y: 72,
		width: 36,
		height: 36,
		mask: true
	},
	backup: {
		x: 576,
		y: 72,
		width: 36,
		height: 36,
		mask: true
	},
	battery_20: {
		x: 612,
		y: 72,
		width: 36,
		height: 36,
		mask: true
	},
	battery_30: {
		x: 648,
		y: 72,
		width: 36,
		height: 36,
		mask: true
	},
	battery_50: {
		x: 684,
		y: 72,
		width: 36,
		height: 36,
		mask: true
	},
	battery_60: {
		x: 720,
		y: 72,
		width: 36,
		height: 36,
		mask: true
	},
	battery_80: {
		x: 756,
		y: 72,
		width: 36,
		height: 36,
		mask: true
	},
	battery_90: {
		x: 792,
		y: 72,
		width: 36,
		height: 36,
		mask: true
	},
	battery_alert: {
		x: 828,
		y: 72,
		width: 36,
		height: 36,
		mask: true
	},
	battery_charging_20: {
		x: 864,
		y: 72,
		width: 36,
		height: 36,
		mask: true
	},
	battery_charging_30: {
		x: 900,
		y: 72,
		width: 36,
		height: 36,
		mask: true
	},
	battery_charging_50: {
		x: 936,
		y: 72,
		width: 36,
		height: 36,
		mask: true
	},
	battery_charging_60: {
		x: 972,
		y: 72,
		width: 36,
		height: 36,
		mask: true
	},
	battery_charging_80: {
		x: 0,
		y: 108,
		width: 36,
		height: 36,
		mask: true
	},
	battery_charging_90: {
		x: 36,
		y: 108,
		width: 36,
		height: 36,
		mask: true
	},
	battery_charging_full: {
		x: 72,
		y: 108,
		width: 36,
		height: 36,
		mask: true
	},
	battery_full: {
		x: 108,
		y: 108,
		width: 36,
		height: 36,
		mask: true
	},
	battery_std: {
		x: 144,
		y: 108,
		width: 36,
		height: 36,
		mask: true
	},
	battery_unknown: {
		x: 180,
		y: 108,
		width: 36,
		height: 36,
		mask: true
	},
	beach_access: {
		x: 216,
		y: 108,
		width: 36,
		height: 36,
		mask: true
	},
	beenhere: {
		x: 252,
		y: 108,
		width: 36,
		height: 36,
		mask: true
	},
	block: {
		x: 288,
		y: 108,
		width: 36,
		height: 36,
		mask: true
	},
	bluetooth: {
		x: 324,
		y: 108,
		width: 36,
		height: 36,
		mask: true
	},
	bluetooth_audio: {
		x: 360,
		y: 108,
		width: 36,
		height: 36,
		mask: true
	},
	bluetooth_connected: {
		x: 396,
		y: 108,
		width: 36,
		height: 36,
		mask: true
	},
	bluetooth_disabled: {
		x: 432,
		y: 108,
		width: 36,
		height: 36,
		mask: true
	},
	bluetooth_searching: {
		x: 468,
		y: 108,
		width: 36,
		height: 36,
		mask: true
	},
	blur_circular: {
		x: 504,
		y: 108,
		width: 36,
		height: 36,
		mask: true
	},
	blur_linear: {
		x: 540,
		y: 108,
		width: 36,
		height: 36,
		mask: true
	},
	blur_off: {
		x: 576,
		y: 108,
		width: 36,
		height: 36,
		mask: true
	},
	blur_on: {
		x: 612,
		y: 108,
		width: 36,
		height: 36,
		mask: true
	},
	book: {
		x: 648,
		y: 108,
		width: 36,
		height: 36,
		mask: true
	},
	bookmark: {
		x: 684,
		y: 108,
		width: 36,
		height: 36,
		mask: true
	},
	bookmark_border: {
		x: 720,
		y: 108,
		width: 36,
		height: 36,
		mask: true
	},
	border_all: {
		x: 756,
		y: 108,
		width: 36,
		height: 36,
		mask: true
	},
	border_bottom: {
		x: 792,
		y: 108,
		width: 36,
		height: 36,
		mask: true
	},
	border_clear: {
		x: 828,
		y: 108,
		width: 36,
		height: 36,
		mask: true
	},
	border_color: {
		x: 864,
		y: 108,
		width: 36,
		height: 36,
		mask: true
	},
	border_horizontal: {
		x: 900,
		y: 108,
		width: 36,
		height: 36,
		mask: true
	},
	border_inner: {
		x: 936,
		y: 108,
		width: 36,
		height: 36,
		mask: true
	},
	border_left: {
		x: 972,
		y: 108,
		width: 36,
		height: 36,
		mask: true
	},
	border_outer: {
		x: 0,
		y: 144,
		width: 36,
		height: 36,
		mask: true
	},
	border_right: {
		x: 36,
		y: 144,
		width: 36,
		height: 36,
		mask: true
	},
	border_style: {
		x: 72,
		y: 144,
		width: 36,
		height: 36,
		mask: true
	},
	border_top: {
		x: 108,
		y: 144,
		width: 36,
		height: 36,
		mask: true
	},
	border_vertical: {
		x: 144,
		y: 144,
		width: 36,
		height: 36,
		mask: true
	},
	branding_watermark: {
		x: 180,
		y: 144,
		width: 36,
		height: 36,
		mask: true
	},
	brightness_1: {
		x: 216,
		y: 144,
		width: 36,
		height: 36,
		mask: true
	},
	brightness_2: {
		x: 252,
		y: 144,
		width: 36,
		height: 36,
		mask: true
	},
	brightness_3: {
		x: 288,
		y: 144,
		width: 36,
		height: 36,
		mask: true
	},
	brightness_4: {
		x: 324,
		y: 144,
		width: 36,
		height: 36,
		mask: true
	},
	brightness_5: {
		x: 360,
		y: 144,
		width: 36,
		height: 36,
		mask: true
	},
	brightness_6: {
		x: 396,
		y: 144,
		width: 36,
		height: 36,
		mask: true
	},
	brightness_7: {
		x: 432,
		y: 144,
		width: 36,
		height: 36,
		mask: true
	},
	brightness_auto: {
		x: 468,
		y: 144,
		width: 36,
		height: 36,
		mask: true
	},
	brightness_high: {
		x: 504,
		y: 144,
		width: 36,
		height: 36,
		mask: true
	},
	brightness_low: {
		x: 540,
		y: 144,
		width: 36,
		height: 36,
		mask: true
	},
	brightness_medium: {
		x: 576,
		y: 144,
		width: 36,
		height: 36,
		mask: true
	},
	broken_image: {
		x: 612,
		y: 144,
		width: 36,
		height: 36,
		mask: true
	},
	brush: {
		x: 648,
		y: 144,
		width: 36,
		height: 36,
		mask: true
	},
	bubble_chart: {
		x: 684,
		y: 144,
		width: 36,
		height: 36,
		mask: true
	},
	bug_report: {
		x: 720,
		y: 144,
		width: 36,
		height: 36,
		mask: true
	},
	build: {
		x: 756,
		y: 144,
		width: 36,
		height: 36,
		mask: true
	},
	burst_mode: {
		x: 792,
		y: 144,
		width: 36,
		height: 36,
		mask: true
	},
	business: {
		x: 828,
		y: 144,
		width: 36,
		height: 36,
		mask: true
	},
	business_center: {
		x: 864,
		y: 144,
		width: 36,
		height: 36,
		mask: true
	},
	cached: {
		x: 900,
		y: 144,
		width: 36,
		height: 36,
		mask: true
	},
	cake: {
		x: 936,
		y: 144,
		width: 36,
		height: 36,
		mask: true
	},
	call: {
		x: 972,
		y: 144,
		width: 36,
		height: 36,
		mask: true
	},
	call_end: {
		x: 0,
		y: 180,
		width: 36,
		height: 36,
		mask: true
	},
	call_made: {
		x: 36,
		y: 180,
		width: 36,
		height: 36,
		mask: true
	},
	call_merge: {
		x: 72,
		y: 180,
		width: 36,
		height: 36,
		mask: true
	},
	call_missed: {
		x: 108,
		y: 180,
		width: 36,
		height: 36,
		mask: true
	},
	call_missed_outgoing: {
		x: 144,
		y: 180,
		width: 36,
		height: 36,
		mask: true
	},
	call_received: {
		x: 180,
		y: 180,
		width: 36,
		height: 36,
		mask: true
	},
	call_split: {
		x: 216,
		y: 180,
		width: 36,
		height: 36,
		mask: true
	},
	call_to_action: {
		x: 252,
		y: 180,
		width: 36,
		height: 36,
		mask: true
	},
	camera: {
		x: 288,
		y: 180,
		width: 36,
		height: 36,
		mask: true
	},
	camera_alt: {
		x: 324,
		y: 180,
		width: 36,
		height: 36,
		mask: true
	},
	camera_enhance: {
		x: 360,
		y: 180,
		width: 36,
		height: 36,
		mask: true
	},
	camera_front: {
		x: 396,
		y: 180,
		width: 36,
		height: 36,
		mask: true
	},
	camera_rear: {
		x: 432,
		y: 180,
		width: 36,
		height: 36,
		mask: true
	},
	camera_roll: {
		x: 468,
		y: 180,
		width: 36,
		height: 36,
		mask: true
	},
	cancel: {
		x: 504,
		y: 180,
		width: 36,
		height: 36,
		mask: true
	},
	card_giftcard: {
		x: 540,
		y: 180,
		width: 36,
		height: 36,
		mask: true
	},
	card_membership: {
		x: 576,
		y: 180,
		width: 36,
		height: 36,
		mask: true
	},
	card_travel: {
		x: 612,
		y: 180,
		width: 36,
		height: 36,
		mask: true
	},
	casino: {
		x: 648,
		y: 180,
		width: 36,
		height: 36,
		mask: true
	},
	cast: {
		x: 684,
		y: 180,
		width: 36,
		height: 36,
		mask: true
	},
	cast_connected: {
		x: 720,
		y: 180,
		width: 36,
		height: 36,
		mask: true
	},
	center_focus_strong: {
		x: 756,
		y: 180,
		width: 36,
		height: 36,
		mask: true
	},
	center_focus_weak: {
		x: 792,
		y: 180,
		width: 36,
		height: 36,
		mask: true
	},
	change_history: {
		x: 828,
		y: 180,
		width: 36,
		height: 36,
		mask: true
	},
	change_history_filled: {
		x: 864,
		y: 180,
		width: 24,
		height: 24,
		mask: true
	},
	chat: {
		x: 888,
		y: 180,
		width: 36,
		height: 36,
		mask: true
	},
	chat_bubble: {
		x: 924,
		y: 180,
		width: 36,
		height: 36,
		mask: true
	},
	chat_bubble_outline: {
		x: 960,
		y: 180,
		width: 36,
		height: 36,
		mask: true
	},
	check: {
		x: 0,
		y: 216,
		width: 36,
		height: 36,
		mask: true
	},
	check_circle: {
		x: 36,
		y: 216,
		width: 36,
		height: 36,
		mask: true
	},
	chevron_left: {
		x: 72,
		y: 216,
		width: 36,
		height: 36,
		mask: true
	},
	chevron_right: {
		x: 108,
		y: 216,
		width: 36,
		height: 36,
		mask: true
	},
	child_care: {
		x: 144,
		y: 216,
		width: 36,
		height: 36,
		mask: true
	},
	child_friendly: {
		x: 180,
		y: 216,
		width: 36,
		height: 36,
		mask: true
	},
	chrome_reader_mode: {
		x: 216,
		y: 216,
		width: 36,
		height: 36,
		mask: true
	},
	class: {
		x: 252,
		y: 216,
		width: 36,
		height: 36,
		mask: true
	},
	clear: {
		x: 288,
		y: 216,
		width: 36,
		height: 36,
		mask: true
	},
	clear_all: {
		x: 324,
		y: 216,
		width: 36,
		height: 36,
		mask: true
	},
	close: {
		x: 360,
		y: 216,
		width: 36,
		height: 36,
		mask: true
	},
	closed_caption: {
		x: 396,
		y: 216,
		width: 36,
		height: 36,
		mask: true
	},
	cloud: {
		x: 432,
		y: 216,
		width: 36,
		height: 36,
		mask: true
	},
	cloud_circle: {
		x: 468,
		y: 216,
		width: 36,
		height: 36,
		mask: true
	},
	cloud_done: {
		x: 504,
		y: 216,
		width: 36,
		height: 36,
		mask: true
	},
	cloud_download: {
		x: 540,
		y: 216,
		width: 36,
		height: 36,
		mask: true
	},
	cloud_off: {
		x: 576,
		y: 216,
		width: 36,
		height: 36,
		mask: true
	},
	cloud_queue: {
		x: 612,
		y: 216,
		width: 36,
		height: 36,
		mask: true
	},
	cloud_upload: {
		x: 648,
		y: 216,
		width: 36,
		height: 36,
		mask: true
	},
	code: {
		x: 684,
		y: 216,
		width: 36,
		height: 36,
		mask: true
	},
	collections: {
		x: 720,
		y: 216,
		width: 36,
		height: 36,
		mask: true
	},
	collections_bookmark: {
		x: 756,
		y: 216,
		width: 36,
		height: 36,
		mask: true
	},
	color_lens: {
		x: 792,
		y: 216,
		width: 36,
		height: 36,
		mask: true
	},
	colorize: {
		x: 828,
		y: 216,
		width: 36,
		height: 36,
		mask: true
	},
	comment: {
		x: 864,
		y: 216,
		width: 36,
		height: 36,
		mask: true
	},
	compare: {
		x: 900,
		y: 216,
		width: 36,
		height: 36,
		mask: true
	},
	compare_arrows: {
		x: 936,
		y: 216,
		width: 36,
		height: 36,
		mask: true
	},
	computer: {
		x: 972,
		y: 216,
		width: 36,
		height: 36,
		mask: true
	},
	confirmation_number: {
		x: 0,
		y: 252,
		width: 36,
		height: 36,
		mask: true
	},
	contact_mail: {
		x: 36,
		y: 252,
		width: 36,
		height: 36,
		mask: true
	},
	contact_phone: {
		x: 72,
		y: 252,
		width: 36,
		height: 36,
		mask: true
	},
	contacts: {
		x: 108,
		y: 252,
		width: 36,
		height: 36,
		mask: true
	},
	content_copy: {
		x: 144,
		y: 252,
		width: 36,
		height: 36,
		mask: true
	},
	content_cut: {
		x: 180,
		y: 252,
		width: 36,
		height: 36,
		mask: true
	},
	content_paste: {
		x: 216,
		y: 252,
		width: 36,
		height: 36,
		mask: true
	},
	control_point: {
		x: 252,
		y: 252,
		width: 36,
		height: 36,
		mask: true
	},
	control_point_duplicate: {
		x: 288,
		y: 252,
		width: 36,
		height: 36,
		mask: true
	},
	copyright: {
		x: 324,
		y: 252,
		width: 36,
		height: 36,
		mask: true
	},
	create: {
		x: 360,
		y: 252,
		width: 36,
		height: 36,
		mask: true
	},
	create_new_folder: {
		x: 396,
		y: 252,
		width: 36,
		height: 36,
		mask: true
	},
	credit_card: {
		x: 432,
		y: 252,
		width: 36,
		height: 36,
		mask: true
	},
	crop: {
		x: 468,
		y: 252,
		width: 36,
		height: 36,
		mask: true
	},
	crop_3_2: {
		x: 504,
		y: 252,
		width: 36,
		height: 36,
		mask: true
	},
	crop_5_4: {
		x: 540,
		y: 252,
		width: 36,
		height: 36,
		mask: true
	},
	crop_7_5: {
		x: 576,
		y: 252,
		width: 36,
		height: 36,
		mask: true
	},
	crop_16_9: {
		x: 612,
		y: 252,
		width: 36,
		height: 36,
		mask: true
	},
	crop_din: {
		x: 648,
		y: 252,
		width: 36,
		height: 36,
		mask: true
	},
	crop_free: {
		x: 684,
		y: 252,
		width: 36,
		height: 36,
		mask: true
	},
	crop_landscape: {
		x: 720,
		y: 252,
		width: 36,
		height: 36,
		mask: true
	},
	crop_original: {
		x: 756,
		y: 252,
		width: 36,
		height: 36,
		mask: true
	},
	crop_portrait: {
		x: 792,
		y: 252,
		width: 36,
		height: 36,
		mask: true
	},
	crop_rotate: {
		x: 828,
		y: 252,
		width: 36,
		height: 36,
		mask: true
	},
	crop_square: {
		x: 864,
		y: 252,
		width: 36,
		height: 36,
		mask: true
	},
	dashboard: {
		x: 900,
		y: 252,
		width: 36,
		height: 36,
		mask: true
	},
	data_usage: {
		x: 936,
		y: 252,
		width: 36,
		height: 36,
		mask: true
	},
	date_range: {
		x: 972,
		y: 252,
		width: 36,
		height: 36,
		mask: true
	},
	dehaze: {
		x: 0,
		y: 288,
		width: 36,
		height: 36,
		mask: true
	},
	delete: {
		x: 36,
		y: 288,
		width: 36,
		height: 36,
		mask: true
	},
	delete_forever: {
		x: 72,
		y: 288,
		width: 36,
		height: 36,
		mask: true
	},
	delete_sweep: {
		x: 108,
		y: 288,
		width: 36,
		height: 36,
		mask: true
	},
	description: {
		x: 144,
		y: 288,
		width: 36,
		height: 36,
		mask: true
	},
	desktop_mac: {
		x: 180,
		y: 288,
		width: 36,
		height: 36,
		mask: true
	},
	desktop_windows: {
		x: 216,
		y: 288,
		width: 36,
		height: 36,
		mask: true
	},
	details: {
		x: 252,
		y: 288,
		width: 36,
		height: 36,
		mask: true
	},
	developer_board: {
		x: 288,
		y: 288,
		width: 36,
		height: 36,
		mask: true
	},
	developer_mode: {
		x: 324,
		y: 288,
		width: 36,
		height: 36,
		mask: true
	},
	device_hub: {
		x: 360,
		y: 288,
		width: 36,
		height: 36,
		mask: true
	},
	devices: {
		x: 396,
		y: 288,
		width: 36,
		height: 36,
		mask: true
	},
	devices_other: {
		x: 432,
		y: 288,
		width: 36,
		height: 36,
		mask: true
	},
	dialer_sip: {
		x: 468,
		y: 288,
		width: 36,
		height: 36,
		mask: true
	},
	dialpad: {
		x: 504,
		y: 288,
		width: 36,
		height: 36,
		mask: true
	},
	diamond: {
		x: 540,
		y: 288,
		width: 24,
		height: 24,
		mask: true
	},
	diamond_filled: {
		x: 564,
		y: 288,
		width: 24,
		height: 24,
		mask: true
	},
	directions: {
		x: 588,
		y: 288,
		width: 36,
		height: 36,
		mask: true
	},
	directions_bike: {
		x: 624,
		y: 288,
		width: 36,
		height: 36,
		mask: true
	},
	directions_boat: {
		x: 660,
		y: 288,
		width: 36,
		height: 36,
		mask: true
	},
	directions_bus: {
		x: 696,
		y: 288,
		width: 36,
		height: 36,
		mask: true
	},
	directions_car: {
		x: 732,
		y: 288,
		width: 36,
		height: 36,
		mask: true
	},
	directions_railway: {
		x: 768,
		y: 288,
		width: 36,
		height: 36,
		mask: true
	},
	directions_run: {
		x: 804,
		y: 288,
		width: 36,
		height: 36,
		mask: true
	},
	directions_subway: {
		x: 840,
		y: 288,
		width: 36,
		height: 36,
		mask: true
	},
	directions_transit: {
		x: 876,
		y: 288,
		width: 36,
		height: 36,
		mask: true
	},
	directions_walk: {
		x: 912,
		y: 288,
		width: 36,
		height: 36,
		mask: true
	},
	disc_full: {
		x: 948,
		y: 288,
		width: 36,
		height: 36,
		mask: true
	},
	dns: {
		x: 0,
		y: 324,
		width: 36,
		height: 36,
		mask: true
	},
	do_not_disturb: {
		x: 36,
		y: 324,
		width: 36,
		height: 36,
		mask: true
	},
	do_not_disturb_alt: {
		x: 72,
		y: 324,
		width: 36,
		height: 36,
		mask: true
	},
	do_not_disturb_off: {
		x: 108,
		y: 324,
		width: 36,
		height: 36,
		mask: true
	},
	do_not_disturb_on: {
		x: 144,
		y: 324,
		width: 36,
		height: 36,
		mask: true
	},
	dock: {
		x: 180,
		y: 324,
		width: 36,
		height: 36,
		mask: true
	},
	domain: {
		x: 216,
		y: 324,
		width: 36,
		height: 36,
		mask: true
	},
	done: {
		x: 252,
		y: 324,
		width: 36,
		height: 36,
		mask: true
	},
	done_all: {
		x: 288,
		y: 324,
		width: 36,
		height: 36,
		mask: true
	},
	donut_large: {
		x: 324,
		y: 324,
		width: 36,
		height: 36,
		mask: true
	},
	donut_small: {
		x: 360,
		y: 324,
		width: 36,
		height: 36,
		mask: true
	},
	drafts: {
		x: 396,
		y: 324,
		width: 36,
		height: 36,
		mask: true
	},
	drag_handle: {
		x: 432,
		y: 324,
		width: 36,
		height: 36,
		mask: true
	},
	drive_eta: {
		x: 468,
		y: 324,
		width: 36,
		height: 36,
		mask: true
	},
	dvr: {
		x: 504,
		y: 324,
		width: 36,
		height: 36,
		mask: true
	},
	edit: {
		x: 540,
		y: 324,
		width: 36,
		height: 36,
		mask: true
	},
	edit_location: {
		x: 576,
		y: 324,
		width: 36,
		height: 36,
		mask: true
	},
	eject: {
		x: 612,
		y: 324,
		width: 36,
		height: 36,
		mask: true
	},
	email: {
		x: 648,
		y: 324,
		width: 36,
		height: 36,
		mask: true
	},
	enhanced_encryption: {
		x: 684,
		y: 324,
		width: 36,
		height: 36,
		mask: true
	},
	equalizer: {
		x: 720,
		y: 324,
		width: 36,
		height: 36,
		mask: true
	},
	error: {
		x: 756,
		y: 324,
		width: 36,
		height: 36,
		mask: true
	},
	error_outline: {
		x: 792,
		y: 324,
		width: 36,
		height: 36,
		mask: true
	},
	euro_symbol: {
		x: 828,
		y: 324,
		width: 36,
		height: 36,
		mask: true
	},
	ev_station: {
		x: 864,
		y: 324,
		width: 36,
		height: 36,
		mask: true
	},
	event: {
		x: 900,
		y: 324,
		width: 36,
		height: 36,
		mask: true
	},
	event_available: {
		x: 936,
		y: 324,
		width: 36,
		height: 36,
		mask: true
	},
	event_busy: {
		x: 972,
		y: 324,
		width: 36,
		height: 36,
		mask: true
	},
	event_note: {
		x: 0,
		y: 360,
		width: 36,
		height: 36,
		mask: true
	},
	event_seat: {
		x: 36,
		y: 360,
		width: 36,
		height: 36,
		mask: true
	},
	exit_to_app: {
		x: 72,
		y: 360,
		width: 36,
		height: 36,
		mask: true
	},
	expand_less: {
		x: 108,
		y: 360,
		width: 36,
		height: 36,
		mask: true
	},
	expand_more: {
		x: 144,
		y: 360,
		width: 36,
		height: 36,
		mask: true
	},
	explicit: {
		x: 180,
		y: 360,
		width: 36,
		height: 36,
		mask: true
	},
	explore: {
		x: 216,
		y: 360,
		width: 36,
		height: 36,
		mask: true
	},
	exposure: {
		x: 252,
		y: 360,
		width: 36,
		height: 36,
		mask: true
	},
	exposure_neg_1: {
		x: 288,
		y: 360,
		width: 36,
		height: 36,
		mask: true
	},
	exposure_neg_2: {
		x: 324,
		y: 360,
		width: 36,
		height: 36,
		mask: true
	},
	exposure_plus_1: {
		x: 360,
		y: 360,
		width: 36,
		height: 36,
		mask: true
	},
	exposure_plus_2: {
		x: 396,
		y: 360,
		width: 36,
		height: 36,
		mask: true
	},
	exposure_zero: {
		x: 432,
		y: 360,
		width: 36,
		height: 36,
		mask: true
	},
	extension: {
		x: 468,
		y: 360,
		width: 36,
		height: 36,
		mask: true
	},
	face: {
		x: 504,
		y: 360,
		width: 36,
		height: 36,
		mask: true
	},
	fast_forward: {
		x: 540,
		y: 360,
		width: 36,
		height: 36,
		mask: true
	},
	fast_rewind: {
		x: 576,
		y: 360,
		width: 36,
		height: 36,
		mask: true
	},
	favorite: {
		x: 612,
		y: 360,
		width: 36,
		height: 36,
		mask: true
	},
	favorite_border: {
		x: 648,
		y: 360,
		width: 36,
		height: 36,
		mask: true
	},
	featured_play_list: {
		x: 684,
		y: 360,
		width: 36,
		height: 36,
		mask: true
	},
	featured_video: {
		x: 720,
		y: 360,
		width: 36,
		height: 36,
		mask: true
	},
	feedback: {
		x: 756,
		y: 360,
		width: 36,
		height: 36,
		mask: true
	},
	fiber_dvr: {
		x: 792,
		y: 360,
		width: 36,
		height: 36,
		mask: true
	},
	fiber_manual_record: {
		x: 828,
		y: 360,
		width: 24,
		height: 24,
		mask: true
	},
	fiber_manual_record_filled: {
		x: 852,
		y: 360,
		width: 24,
		height: 24,
		mask: true
	},
	fiber_new: {
		x: 876,
		y: 360,
		width: 36,
		height: 36,
		mask: true
	},
	fiber_pin: {
		x: 912,
		y: 360,
		width: 36,
		height: 36,
		mask: true
	},
	fiber_smart_record: {
		x: 948,
		y: 360,
		width: 36,
		height: 36,
		mask: true
	},
	file_download: {
		x: 0,
		y: 396,
		width: 36,
		height: 36,
		mask: true
	},
	file_upload: {
		x: 36,
		y: 396,
		width: 36,
		height: 36,
		mask: true
	},
	filter: {
		x: 72,
		y: 396,
		width: 36,
		height: 36,
		mask: true
	},
	filter_1: {
		x: 108,
		y: 396,
		width: 36,
		height: 36,
		mask: true
	},
	filter_2: {
		x: 144,
		y: 396,
		width: 36,
		height: 36,
		mask: true
	},
	filter_3: {
		x: 180,
		y: 396,
		width: 36,
		height: 36,
		mask: true
	},
	filter_4: {
		x: 216,
		y: 396,
		width: 36,
		height: 36,
		mask: true
	},
	filter_5: {
		x: 252,
		y: 396,
		width: 36,
		height: 36,
		mask: true
	},
	filter_6: {
		x: 288,
		y: 396,
		width: 36,
		height: 36,
		mask: true
	},
	filter_7: {
		x: 324,
		y: 396,
		width: 36,
		height: 36,
		mask: true
	},
	filter_8: {
		x: 360,
		y: 396,
		width: 36,
		height: 36,
		mask: true
	},
	filter_9: {
		x: 396,
		y: 396,
		width: 36,
		height: 36,
		mask: true
	},
	filter_9_plus: {
		x: 432,
		y: 396,
		width: 36,
		height: 36,
		mask: true
	},
	filter_b_and_w: {
		x: 468,
		y: 396,
		width: 36,
		height: 36,
		mask: true
	},
	filter_center_focus: {
		x: 504,
		y: 396,
		width: 36,
		height: 36,
		mask: true
	},
	filter_drama: {
		x: 540,
		y: 396,
		width: 36,
		height: 36,
		mask: true
	},
	filter_frames: {
		x: 576,
		y: 396,
		width: 36,
		height: 36,
		mask: true
	},
	filter_hdr: {
		x: 612,
		y: 396,
		width: 36,
		height: 36,
		mask: true
	},
	filter_list: {
		x: 648,
		y: 396,
		width: 36,
		height: 36,
		mask: true
	},
	filter_none: {
		x: 684,
		y: 396,
		width: 36,
		height: 36,
		mask: true
	},
	filter_tilt_shift: {
		x: 720,
		y: 396,
		width: 36,
		height: 36,
		mask: true
	},
	filter_vintage: {
		x: 756,
		y: 396,
		width: 36,
		height: 36,
		mask: true
	},
	find_in_page: {
		x: 792,
		y: 396,
		width: 36,
		height: 36,
		mask: true
	},
	find_replace: {
		x: 828,
		y: 396,
		width: 36,
		height: 36,
		mask: true
	},
	fingerprint: {
		x: 864,
		y: 396,
		width: 36,
		height: 36,
		mask: true
	},
	first_page: {
		x: 900,
		y: 396,
		width: 36,
		height: 36,
		mask: true
	},
	fitness_center: {
		x: 936,
		y: 396,
		width: 36,
		height: 36,
		mask: true
	},
	flag: {
		x: 972,
		y: 396,
		width: 36,
		height: 36,
		mask: true
	},
	flare: {
		x: 0,
		y: 432,
		width: 36,
		height: 36,
		mask: true
	},
	flash_auto: {
		x: 36,
		y: 432,
		width: 36,
		height: 36,
		mask: true
	},
	flash_off: {
		x: 72,
		y: 432,
		width: 36,
		height: 36,
		mask: true
	},
	flash_on: {
		x: 108,
		y: 432,
		width: 36,
		height: 36,
		mask: true
	},
	flight: {
		x: 144,
		y: 432,
		width: 36,
		height: 36,
		mask: true
	},
	flight_land: {
		x: 180,
		y: 432,
		width: 36,
		height: 36,
		mask: true
	},
	flight_takeoff: {
		x: 216,
		y: 432,
		width: 36,
		height: 36,
		mask: true
	},
	flip: {
		x: 252,
		y: 432,
		width: 36,
		height: 36,
		mask: true
	},
	flip_to_back: {
		x: 288,
		y: 432,
		width: 36,
		height: 36,
		mask: true
	},
	flip_to_front: {
		x: 324,
		y: 432,
		width: 36,
		height: 36,
		mask: true
	},
	folder: {
		x: 360,
		y: 432,
		width: 36,
		height: 36,
		mask: true
	},
	folder_open: {
		x: 396,
		y: 432,
		width: 36,
		height: 36,
		mask: true
	},
	folder_shared: {
		x: 432,
		y: 432,
		width: 36,
		height: 36,
		mask: true
	},
	folder_special: {
		x: 468,
		y: 432,
		width: 36,
		height: 36,
		mask: true
	},
	font_download: {
		x: 504,
		y: 432,
		width: 36,
		height: 36,
		mask: true
	},
	format_align_center: {
		x: 540,
		y: 432,
		width: 36,
		height: 36,
		mask: true
	},
	format_align_justify: {
		x: 576,
		y: 432,
		width: 36,
		height: 36,
		mask: true
	},
	format_align_left: {
		x: 612,
		y: 432,
		width: 36,
		height: 36,
		mask: true
	},
	format_align_right: {
		x: 648,
		y: 432,
		width: 36,
		height: 36,
		mask: true
	},
	format_bold: {
		x: 684,
		y: 432,
		width: 36,
		height: 36,
		mask: true
	},
	format_clear: {
		x: 720,
		y: 432,
		width: 36,
		height: 36,
		mask: true
	},
	format_color_fill: {
		x: 756,
		y: 432,
		width: 36,
		height: 36,
		mask: true
	},
	format_color_reset: {
		x: 792,
		y: 432,
		width: 36,
		height: 36,
		mask: true
	},
	format_color_text: {
		x: 828,
		y: 432,
		width: 36,
		height: 36,
		mask: true
	},
	format_indent_decrease: {
		x: 864,
		y: 432,
		width: 36,
		height: 36,
		mask: true
	},
	format_indent_increase: {
		x: 900,
		y: 432,
		width: 36,
		height: 36,
		mask: true
	},
	format_italic: {
		x: 936,
		y: 432,
		width: 36,
		height: 36,
		mask: true
	},
	format_line_spacing: {
		x: 972,
		y: 432,
		width: 36,
		height: 36,
		mask: true
	},
	format_list_bulleted: {
		x: 0,
		y: 468,
		width: 36,
		height: 36,
		mask: true
	},
	format_list_numbered: {
		x: 36,
		y: 468,
		width: 36,
		height: 36,
		mask: true
	},
	format_paint: {
		x: 72,
		y: 468,
		width: 36,
		height: 36,
		mask: true
	},
	format_quote: {
		x: 108,
		y: 468,
		width: 36,
		height: 36,
		mask: true
	},
	format_shapes: {
		x: 144,
		y: 468,
		width: 36,
		height: 36,
		mask: true
	},
	format_size: {
		x: 180,
		y: 468,
		width: 36,
		height: 36,
		mask: true
	},
	format_strikethrough: {
		x: 216,
		y: 468,
		width: 36,
		height: 36,
		mask: true
	},
	format_textdirection_l_to_r: {
		x: 252,
		y: 468,
		width: 36,
		height: 36,
		mask: true
	},
	format_textdirection_r_to_l: {
		x: 288,
		y: 468,
		width: 36,
		height: 36,
		mask: true
	},
	format_underlined: {
		x: 324,
		y: 468,
		width: 36,
		height: 36,
		mask: true
	},
	forum: {
		x: 360,
		y: 468,
		width: 36,
		height: 36,
		mask: true
	},
	forward: {
		x: 396,
		y: 468,
		width: 36,
		height: 36,
		mask: true
	},
	forward_5: {
		x: 432,
		y: 468,
		width: 36,
		height: 36,
		mask: true
	},
	forward_10: {
		x: 468,
		y: 468,
		width: 36,
		height: 36,
		mask: true
	},
	forward_30: {
		x: 504,
		y: 468,
		width: 36,
		height: 36,
		mask: true
	},
	free_breakfast: {
		x: 540,
		y: 468,
		width: 36,
		height: 36,
		mask: true
	},
	fullscreen: {
		x: 576,
		y: 468,
		width: 36,
		height: 36,
		mask: true
	},
	fullscreen_exit: {
		x: 612,
		y: 468,
		width: 36,
		height: 36,
		mask: true
	},
	functions: {
		x: 648,
		y: 468,
		width: 36,
		height: 36,
		mask: true
	},
	g_translate: {
		x: 684,
		y: 468,
		width: 36,
		height: 36,
		mask: true
	},
	gamepad: {
		x: 720,
		y: 468,
		width: 36,
		height: 36,
		mask: true
	},
	games: {
		x: 756,
		y: 468,
		width: 36,
		height: 36,
		mask: true
	},
	gavel: {
		x: 792,
		y: 468,
		width: 36,
		height: 36,
		mask: true
	},
	gesture: {
		x: 828,
		y: 468,
		width: 36,
		height: 36,
		mask: true
	},
	get_app: {
		x: 864,
		y: 468,
		width: 36,
		height: 36,
		mask: true
	},
	gif: {
		x: 900,
		y: 468,
		width: 36,
		height: 36,
		mask: true
	},
	golf_course: {
		x: 936,
		y: 468,
		width: 36,
		height: 36,
		mask: true
	},
	gps_fixed: {
		x: 972,
		y: 468,
		width: 36,
		height: 36,
		mask: true
	},
	gps_not_fixed: {
		x: 0,
		y: 504,
		width: 36,
		height: 36,
		mask: true
	},
	gps_off: {
		x: 36,
		y: 504,
		width: 36,
		height: 36,
		mask: true
	},
	grade: {
		x: 72,
		y: 504,
		width: 36,
		height: 36,
		mask: true
	},
	gradient: {
		x: 108,
		y: 504,
		width: 36,
		height: 36,
		mask: true
	},
	grain: {
		x: 144,
		y: 504,
		width: 36,
		height: 36,
		mask: true
	},
	graphic_eq: {
		x: 180,
		y: 504,
		width: 36,
		height: 36,
		mask: true
	},
	grid_off: {
		x: 216,
		y: 504,
		width: 36,
		height: 36,
		mask: true
	},
	grid_on: {
		x: 252,
		y: 504,
		width: 36,
		height: 36,
		mask: true
	},
	group: {
		x: 288,
		y: 504,
		width: 36,
		height: 36,
		mask: true
	},
	group_add: {
		x: 324,
		y: 504,
		width: 36,
		height: 36,
		mask: true
	},
	group_work: {
		x: 360,
		y: 504,
		width: 36,
		height: 36,
		mask: true
	},
	hd: {
		x: 396,
		y: 504,
		width: 36,
		height: 36,
		mask: true
	},
	hdr_off: {
		x: 432,
		y: 504,
		width: 36,
		height: 36,
		mask: true
	},
	hdr_on: {
		x: 468,
		y: 504,
		width: 36,
		height: 36,
		mask: true
	},
	hdr_strong: {
		x: 504,
		y: 504,
		width: 36,
		height: 36,
		mask: true
	},
	hdr_weak: {
		x: 540,
		y: 504,
		width: 36,
		height: 36,
		mask: true
	},
	headset: {
		x: 576,
		y: 504,
		width: 36,
		height: 36,
		mask: true
	},
	headset_mic: {
		x: 612,
		y: 504,
		width: 36,
		height: 36,
		mask: true
	},
	healing: {
		x: 648,
		y: 504,
		width: 36,
		height: 36,
		mask: true
	},
	hearing: {
		x: 684,
		y: 504,
		width: 36,
		height: 36,
		mask: true
	},
	help: {
		x: 720,
		y: 504,
		width: 36,
		height: 36,
		mask: true
	},
	help_outline: {
		x: 756,
		y: 504,
		width: 36,
		height: 36,
		mask: true
	},
	high_quality: {
		x: 792,
		y: 504,
		width: 36,
		height: 36,
		mask: true
	},
	highlight: {
		x: 828,
		y: 504,
		width: 36,
		height: 36,
		mask: true
	},
	highlight_off: {
		x: 864,
		y: 504,
		width: 36,
		height: 36,
		mask: true
	},
	history: {
		x: 900,
		y: 504,
		width: 36,
		height: 36,
		mask: true
	},
	home: {
		x: 936,
		y: 504,
		width: 36,
		height: 36,
		mask: true
	},
	hot_tub: {
		x: 972,
		y: 504,
		width: 36,
		height: 36,
		mask: true
	},
	hotel: {
		x: 0,
		y: 540,
		width: 36,
		height: 36,
		mask: true
	},
	hourglass_empty: {
		x: 36,
		y: 540,
		width: 36,
		height: 36,
		mask: true
	},
	hourglass_full: {
		x: 72,
		y: 540,
		width: 36,
		height: 36,
		mask: true
	},
	http: {
		x: 108,
		y: 540,
		width: 36,
		height: 36,
		mask: true
	},
	https: {
		x: 144,
		y: 540,
		width: 36,
		height: 36,
		mask: true
	},
	image: {
		x: 180,
		y: 540,
		width: 36,
		height: 36,
		mask: true
	},
	image_aspect_ratio: {
		x: 216,
		y: 540,
		width: 36,
		height: 36,
		mask: true
	},
	import_contacts: {
		x: 252,
		y: 540,
		width: 36,
		height: 36,
		mask: true
	},
	import_export: {
		x: 288,
		y: 540,
		width: 36,
		height: 36,
		mask: true
	},
	important_devices: {
		x: 324,
		y: 540,
		width: 36,
		height: 36,
		mask: true
	},
	inbox: {
		x: 360,
		y: 540,
		width: 36,
		height: 36,
		mask: true
	},
	info: {
		x: 396,
		y: 540,
		width: 36,
		height: 36,
		mask: true
	},
	info_outline: {
		x: 432,
		y: 540,
		width: 36,
		height: 36,
		mask: true
	},
	input: {
		x: 468,
		y: 540,
		width: 36,
		height: 36,
		mask: true
	},
	insert_chart: {
		x: 504,
		y: 540,
		width: 36,
		height: 36,
		mask: true
	},
	insert_comment: {
		x: 540,
		y: 540,
		width: 36,
		height: 36,
		mask: true
	},
	insert_drive_file: {
		x: 576,
		y: 540,
		width: 36,
		height: 36,
		mask: true
	},
	insert_emoticon: {
		x: 612,
		y: 540,
		width: 36,
		height: 36,
		mask: true
	},
	insert_invitation: {
		x: 648,
		y: 540,
		width: 36,
		height: 36,
		mask: true
	},
	insert_link: {
		x: 684,
		y: 540,
		width: 36,
		height: 36,
		mask: true
	},
	insert_photo: {
		x: 720,
		y: 540,
		width: 36,
		height: 36,
		mask: true
	},
	invert_colors: {
		x: 756,
		y: 540,
		width: 36,
		height: 36,
		mask: true
	},
	invert_colors_off: {
		x: 792,
		y: 540,
		width: 36,
		height: 36,
		mask: true
	},
	iso: {
		x: 828,
		y: 540,
		width: 36,
		height: 36,
		mask: true
	},
	keyboard: {
		x: 864,
		y: 540,
		width: 36,
		height: 36,
		mask: true
	},
	keyboard_arrow_down: {
		x: 900,
		y: 540,
		width: 36,
		height: 36,
		mask: true
	},
	keyboard_arrow_left: {
		x: 936,
		y: 540,
		width: 36,
		height: 36,
		mask: true
	},
	keyboard_arrow_right: {
		x: 972,
		y: 540,
		width: 36,
		height: 36,
		mask: true
	},
	keyboard_arrow_up: {
		x: 0,
		y: 576,
		width: 36,
		height: 36,
		mask: true
	},
	keyboard_backspace: {
		x: 36,
		y: 576,
		width: 36,
		height: 36,
		mask: true
	},
	keyboard_capslock: {
		x: 72,
		y: 576,
		width: 36,
		height: 36,
		mask: true
	},
	keyboard_hide: {
		x: 108,
		y: 576,
		width: 36,
		height: 36,
		mask: true
	},
	keyboard_return: {
		x: 144,
		y: 576,
		width: 36,
		height: 36,
		mask: true
	},
	keyboard_tab: {
		x: 180,
		y: 576,
		width: 36,
		height: 36,
		mask: true
	},
	keyboard_voice: {
		x: 216,
		y: 576,
		width: 36,
		height: 36,
		mask: true
	},
	kitchen: {
		x: 252,
		y: 576,
		width: 36,
		height: 36,
		mask: true
	},
	label: {
		x: 288,
		y: 576,
		width: 36,
		height: 36,
		mask: true
	},
	label_outline: {
		x: 324,
		y: 576,
		width: 36,
		height: 36,
		mask: true
	},
	landscape: {
		x: 360,
		y: 576,
		width: 36,
		height: 36,
		mask: true
	},
	language: {
		x: 396,
		y: 576,
		width: 36,
		height: 36,
		mask: true
	},
	laptop: {
		x: 432,
		y: 576,
		width: 36,
		height: 36,
		mask: true
	},
	laptop_chromebook: {
		x: 468,
		y: 576,
		width: 36,
		height: 36,
		mask: true
	},
	laptop_mac: {
		x: 504,
		y: 576,
		width: 36,
		height: 36,
		mask: true
	},
	laptop_windows: {
		x: 540,
		y: 576,
		width: 36,
		height: 36,
		mask: true
	},
	last_page: {
		x: 576,
		y: 576,
		width: 36,
		height: 36,
		mask: true
	},
	launch: {
		x: 612,
		y: 576,
		width: 36,
		height: 36,
		mask: true
	},
	layers: {
		x: 648,
		y: 576,
		width: 36,
		height: 36,
		mask: true
	},
	layers_clear: {
		x: 684,
		y: 576,
		width: 36,
		height: 36,
		mask: true
	},
	leak_add: {
		x: 720,
		y: 576,
		width: 36,
		height: 36,
		mask: true
	},
	leak_remove: {
		x: 756,
		y: 576,
		width: 36,
		height: 36,
		mask: true
	},
	lens: {
		x: 792,
		y: 576,
		width: 36,
		height: 36,
		mask: true
	},
	library_add: {
		x: 828,
		y: 576,
		width: 36,
		height: 36,
		mask: true
	},
	library_books: {
		x: 864,
		y: 576,
		width: 36,
		height: 36,
		mask: true
	},
	library_music: {
		x: 900,
		y: 576,
		width: 36,
		height: 36,
		mask: true
	},
	lightbulb_outline: {
		x: 936,
		y: 576,
		width: 36,
		height: 36,
		mask: true
	},
	line_style: {
		x: 972,
		y: 576,
		width: 36,
		height: 36,
		mask: true
	},
	line_weight: {
		x: 0,
		y: 612,
		width: 36,
		height: 36,
		mask: true
	},
	linear_scale: {
		x: 36,
		y: 612,
		width: 36,
		height: 36,
		mask: true
	},
	link: {
		x: 72,
		y: 612,
		width: 36,
		height: 36,
		mask: true
	},
	linked_camera: {
		x: 108,
		y: 612,
		width: 36,
		height: 36,
		mask: true
	},
	list: {
		x: 144,
		y: 612,
		width: 36,
		height: 36,
		mask: true
	},
	live_help: {
		x: 180,
		y: 612,
		width: 36,
		height: 36,
		mask: true
	},
	live_tv: {
		x: 216,
		y: 612,
		width: 36,
		height: 36,
		mask: true
	},
	local_activity: {
		x: 252,
		y: 612,
		width: 36,
		height: 36,
		mask: true
	},
	local_airport: {
		x: 288,
		y: 612,
		width: 36,
		height: 36,
		mask: true
	},
	local_atm: {
		x: 324,
		y: 612,
		width: 36,
		height: 36,
		mask: true
	},
	local_bar: {
		x: 360,
		y: 612,
		width: 36,
		height: 36,
		mask: true
	},
	local_cafe: {
		x: 396,
		y: 612,
		width: 36,
		height: 36,
		mask: true
	},
	local_car_wash: {
		x: 432,
		y: 612,
		width: 36,
		height: 36,
		mask: true
	},
	local_convenience_store: {
		x: 468,
		y: 612,
		width: 36,
		height: 36,
		mask: true
	},
	local_dining: {
		x: 504,
		y: 612,
		width: 36,
		height: 36,
		mask: true
	},
	local_drink: {
		x: 540,
		y: 612,
		width: 36,
		height: 36,
		mask: true
	},
	local_florist: {
		x: 576,
		y: 612,
		width: 36,
		height: 36,
		mask: true
	},
	local_gas_station: {
		x: 612,
		y: 612,
		width: 36,
		height: 36,
		mask: true
	},
	local_grocery_store: {
		x: 648,
		y: 612,
		width: 36,
		height: 36,
		mask: true
	},
	local_hospital: {
		x: 684,
		y: 612,
		width: 36,
		height: 36,
		mask: true
	},
	local_hotel: {
		x: 720,
		y: 612,
		width: 36,
		height: 36,
		mask: true
	},
	local_laundry_service: {
		x: 756,
		y: 612,
		width: 36,
		height: 36,
		mask: true
	},
	local_library: {
		x: 792,
		y: 612,
		width: 36,
		height: 36,
		mask: true
	},
	local_mall: {
		x: 828,
		y: 612,
		width: 36,
		height: 36,
		mask: true
	},
	local_movies: {
		x: 864,
		y: 612,
		width: 36,
		height: 36,
		mask: true
	},
	local_offer: {
		x: 900,
		y: 612,
		width: 36,
		height: 36,
		mask: true
	},
	local_parking: {
		x: 936,
		y: 612,
		width: 36,
		height: 36,
		mask: true
	},
	local_pharmacy: {
		x: 972,
		y: 612,
		width: 36,
		height: 36,
		mask: true
	},
	local_phone: {
		x: 0,
		y: 648,
		width: 36,
		height: 36,
		mask: true
	},
	local_pizza: {
		x: 36,
		y: 648,
		width: 36,
		height: 36,
		mask: true
	},
	local_play: {
		x: 72,
		y: 648,
		width: 36,
		height: 36,
		mask: true
	},
	local_post_office: {
		x: 108,
		y: 648,
		width: 36,
		height: 36,
		mask: true
	},
	local_printshop: {
		x: 144,
		y: 648,
		width: 36,
		height: 36,
		mask: true
	},
	local_see: {
		x: 180,
		y: 648,
		width: 36,
		height: 36,
		mask: true
	},
	local_shipping: {
		x: 216,
		y: 648,
		width: 36,
		height: 36,
		mask: true
	},
	local_taxi: {
		x: 252,
		y: 648,
		width: 36,
		height: 36,
		mask: true
	},
	location_city: {
		x: 288,
		y: 648,
		width: 36,
		height: 36,
		mask: true
	},
	location_disabled: {
		x: 324,
		y: 648,
		width: 36,
		height: 36,
		mask: true
	},
	location_off: {
		x: 360,
		y: 648,
		width: 36,
		height: 36,
		mask: true
	},
	location_on: {
		x: 396,
		y: 648,
		width: 36,
		height: 36,
		mask: true
	},
	location_searching: {
		x: 432,
		y: 648,
		width: 36,
		height: 36,
		mask: true
	},
	lock: {
		x: 468,
		y: 648,
		width: 36,
		height: 36,
		mask: true
	},
	lock_open: {
		x: 504,
		y: 648,
		width: 36,
		height: 36,
		mask: true
	},
	lock_outline: {
		x: 540,
		y: 648,
		width: 36,
		height: 36,
		mask: true
	},
	looks: {
		x: 576,
		y: 648,
		width: 36,
		height: 36,
		mask: true
	},
	looks_3: {
		x: 612,
		y: 648,
		width: 36,
		height: 36,
		mask: true
	},
	looks_4: {
		x: 648,
		y: 648,
		width: 36,
		height: 36,
		mask: true
	},
	looks_5: {
		x: 684,
		y: 648,
		width: 36,
		height: 36,
		mask: true
	},
	looks_6: {
		x: 720,
		y: 648,
		width: 36,
		height: 36,
		mask: true
	},
	looks_one: {
		x: 756,
		y: 648,
		width: 36,
		height: 36,
		mask: true
	},
	looks_two: {
		x: 792,
		y: 648,
		width: 36,
		height: 36,
		mask: true
	},
	loop: {
		x: 828,
		y: 648,
		width: 36,
		height: 36,
		mask: true
	},
	loupe: {
		x: 864,
		y: 648,
		width: 36,
		height: 36,
		mask: true
	},
	low_priority: {
		x: 900,
		y: 648,
		width: 36,
		height: 36,
		mask: true
	},
	loyalty: {
		x: 936,
		y: 648,
		width: 36,
		height: 36,
		mask: true
	},
	mail: {
		x: 972,
		y: 648,
		width: 36,
		height: 36,
		mask: true
	},
	mail_outline: {
		x: 0,
		y: 684,
		width: 36,
		height: 36,
		mask: true
	},
	map: {
		x: 36,
		y: 684,
		width: 36,
		height: 36,
		mask: true
	},
	markunread: {
		x: 72,
		y: 684,
		width: 36,
		height: 36,
		mask: true
	},
	markunread_mailbox: {
		x: 108,
		y: 684,
		width: 36,
		height: 36,
		mask: true
	},
	memory: {
		x: 144,
		y: 684,
		width: 36,
		height: 36,
		mask: true
	},
	menu: {
		x: 180,
		y: 684,
		width: 36,
		height: 36,
		mask: true
	},
	merge_type: {
		x: 216,
		y: 684,
		width: 36,
		height: 36,
		mask: true
	},
	message: {
		x: 252,
		y: 684,
		width: 36,
		height: 36,
		mask: true
	},
	mic: {
		x: 288,
		y: 684,
		width: 36,
		height: 36,
		mask: true
	},
	mic_none: {
		x: 324,
		y: 684,
		width: 36,
		height: 36,
		mask: true
	},
	mic_off: {
		x: 360,
		y: 684,
		width: 36,
		height: 36,
		mask: true
	},
	mms: {
		x: 396,
		y: 684,
		width: 36,
		height: 36,
		mask: true
	},
	mode_comment: {
		x: 432,
		y: 684,
		width: 36,
		height: 36,
		mask: true
	},
	mode_edit: {
		x: 468,
		y: 684,
		width: 36,
		height: 36,
		mask: true
	},
	monetization_on: {
		x: 504,
		y: 684,
		width: 36,
		height: 36,
		mask: true
	},
	money_off: {
		x: 540,
		y: 684,
		width: 36,
		height: 36,
		mask: true
	},
	monochrome_photos: {
		x: 576,
		y: 684,
		width: 36,
		height: 36,
		mask: true
	},
	mood: {
		x: 612,
		y: 684,
		width: 36,
		height: 36,
		mask: true
	},
	mood_bad: {
		x: 648,
		y: 684,
		width: 36,
		height: 36,
		mask: true
	},
	more: {
		x: 684,
		y: 684,
		width: 36,
		height: 36,
		mask: true
	},
	more_horiz: {
		x: 720,
		y: 684,
		width: 36,
		height: 36,
		mask: true
	},
	more_vert: {
		x: 756,
		y: 684,
		width: 36,
		height: 36,
		mask: true
	},
	motorcycle: {
		x: 792,
		y: 684,
		width: 36,
		height: 36,
		mask: true
	},
	mouse: {
		x: 828,
		y: 684,
		width: 36,
		height: 36,
		mask: true
	},
	move_to_inbox: {
		x: 864,
		y: 684,
		width: 36,
		height: 36,
		mask: true
	},
	movie: {
		x: 900,
		y: 684,
		width: 36,
		height: 36,
		mask: true
	},
	movie_creation: {
		x: 936,
		y: 684,
		width: 36,
		height: 36,
		mask: true
	},
	movie_filter: {
		x: 972,
		y: 684,
		width: 36,
		height: 36,
		mask: true
	},
	multiline_chart: {
		x: 0,
		y: 720,
		width: 36,
		height: 36,
		mask: true
	},
	music_note: {
		x: 36,
		y: 720,
		width: 36,
		height: 36,
		mask: true
	},
	music_video: {
		x: 72,
		y: 720,
		width: 36,
		height: 36,
		mask: true
	},
	my_location: {
		x: 108,
		y: 720,
		width: 36,
		height: 36,
		mask: true
	},
	nature: {
		x: 144,
		y: 720,
		width: 36,
		height: 36,
		mask: true
	},
	nature_people: {
		x: 180,
		y: 720,
		width: 36,
		height: 36,
		mask: true
	},
	navigate_before: {
		x: 216,
		y: 720,
		width: 36,
		height: 36,
		mask: true
	},
	navigate_next: {
		x: 252,
		y: 720,
		width: 36,
		height: 36,
		mask: true
	},
	navigation: {
		x: 288,
		y: 720,
		width: 36,
		height: 36,
		mask: true
	},
	near_me: {
		x: 324,
		y: 720,
		width: 36,
		height: 36,
		mask: true
	},
	network_cell: {
		x: 360,
		y: 720,
		width: 36,
		height: 36,
		mask: true
	},
	network_check: {
		x: 396,
		y: 720,
		width: 36,
		height: 36,
		mask: true
	},
	network_locked: {
		x: 432,
		y: 720,
		width: 36,
		height: 36,
		mask: true
	},
	network_wifi: {
		x: 468,
		y: 720,
		width: 36,
		height: 36,
		mask: true
	},
	new_releases: {
		x: 504,
		y: 720,
		width: 36,
		height: 36,
		mask: true
	},
	next_week: {
		x: 540,
		y: 720,
		width: 36,
		height: 36,
		mask: true
	},
	nfc: {
		x: 576,
		y: 720,
		width: 36,
		height: 36,
		mask: true
	},
	no_encryption: {
		x: 612,
		y: 720,
		width: 36,
		height: 36,
		mask: true
	},
	no_sim: {
		x: 648,
		y: 720,
		width: 36,
		height: 36,
		mask: true
	},
	not_interested: {
		x: 684,
		y: 720,
		width: 36,
		height: 36,
		mask: true
	},
	note: {
		x: 720,
		y: 720,
		width: 36,
		height: 36,
		mask: true
	},
	note_add: {
		x: 756,
		y: 720,
		width: 36,
		height: 36,
		mask: true
	},
	notifications: {
		x: 792,
		y: 720,
		width: 36,
		height: 36,
		mask: true
	},
	notifications_active: {
		x: 828,
		y: 720,
		width: 36,
		height: 36,
		mask: true
	},
	notifications_none: {
		x: 864,
		y: 720,
		width: 36,
		height: 36,
		mask: true
	},
	notifications_off: {
		x: 900,
		y: 720,
		width: 36,
		height: 36,
		mask: true
	},
	notifications_paused: {
		x: 936,
		y: 720,
		width: 36,
		height: 36,
		mask: true
	},
	offline_pin: {
		x: 972,
		y: 720,
		width: 36,
		height: 36,
		mask: true
	},
	ondemand_video: {
		x: 0,
		y: 756,
		width: 36,
		height: 36,
		mask: true
	},
	opacity: {
		x: 36,
		y: 756,
		width: 36,
		height: 36,
		mask: true
	},
	open_in_browser: {
		x: 72,
		y: 756,
		width: 36,
		height: 36,
		mask: true
	},
	open_in_new: {
		x: 108,
		y: 756,
		width: 36,
		height: 36,
		mask: true
	},
	open_with: {
		x: 144,
		y: 756,
		width: 36,
		height: 36,
		mask: true
	},
	pages: {
		x: 180,
		y: 756,
		width: 36,
		height: 36,
		mask: true
	},
	pageview: {
		x: 216,
		y: 756,
		width: 36,
		height: 36,
		mask: true
	},
	palette: {
		x: 252,
		y: 756,
		width: 36,
		height: 36,
		mask: true
	},
	pan_tool: {
		x: 288,
		y: 756,
		width: 36,
		height: 36,
		mask: true
	},
	panorama: {
		x: 324,
		y: 756,
		width: 36,
		height: 36,
		mask: true
	},
	panorama_fish_eye: {
		x: 360,
		y: 756,
		width: 36,
		height: 36,
		mask: true
	},
	panorama_horizontal: {
		x: 396,
		y: 756,
		width: 36,
		height: 36,
		mask: true
	},
	panorama_vertical: {
		x: 432,
		y: 756,
		width: 36,
		height: 36,
		mask: true
	},
	panorama_wide_angle: {
		x: 468,
		y: 756,
		width: 36,
		height: 36,
		mask: true
	},
	party_mode: {
		x: 504,
		y: 756,
		width: 36,
		height: 36,
		mask: true
	},
	pause: {
		x: 540,
		y: 756,
		width: 36,
		height: 36,
		mask: true
	},
	pause_circle_filled: {
		x: 576,
		y: 756,
		width: 36,
		height: 36,
		mask: true
	},
	pause_circle_outline: {
		x: 612,
		y: 756,
		width: 36,
		height: 36,
		mask: true
	},
	payment: {
		x: 648,
		y: 756,
		width: 36,
		height: 36,
		mask: true
	},
	people: {
		x: 684,
		y: 756,
		width: 36,
		height: 36,
		mask: true
	},
	people_outline: {
		x: 720,
		y: 756,
		width: 36,
		height: 36,
		mask: true
	},
	perm_camera_mic: {
		x: 756,
		y: 756,
		width: 36,
		height: 36,
		mask: true
	},
	perm_contact_calendar: {
		x: 792,
		y: 756,
		width: 36,
		height: 36,
		mask: true
	},
	perm_data_setting: {
		x: 828,
		y: 756,
		width: 36,
		height: 36,
		mask: true
	},
	perm_device_information: {
		x: 864,
		y: 756,
		width: 36,
		height: 36,
		mask: true
	},
	perm_identity: {
		x: 900,
		y: 756,
		width: 36,
		height: 36,
		mask: true
	},
	perm_media: {
		x: 936,
		y: 756,
		width: 36,
		height: 36,
		mask: true
	},
	perm_phone_msg: {
		x: 972,
		y: 756,
		width: 36,
		height: 36,
		mask: true
	},
	perm_scan_wifi: {
		x: 0,
		y: 792,
		width: 36,
		height: 36,
		mask: true
	},
	person: {
		x: 36,
		y: 792,
		width: 36,
		height: 36,
		mask: true
	},
	person_add: {
		x: 72,
		y: 792,
		width: 36,
		height: 36,
		mask: true
	},
	person_outline: {
		x: 108,
		y: 792,
		width: 36,
		height: 36,
		mask: true
	},
	person_pin: {
		x: 144,
		y: 792,
		width: 36,
		height: 36,
		mask: true
	},
	person_pin_circle: {
		x: 180,
		y: 792,
		width: 36,
		height: 36,
		mask: true
	},
	personal_video: {
		x: 216,
		y: 792,
		width: 36,
		height: 36,
		mask: true
	},
	pets: {
		x: 252,
		y: 792,
		width: 36,
		height: 36,
		mask: true
	},
	phone: {
		x: 288,
		y: 792,
		width: 36,
		height: 36,
		mask: true
	},
	phone_android: {
		x: 324,
		y: 792,
		width: 36,
		height: 36,
		mask: true
	},
	phone_bluetooth_speaker: {
		x: 360,
		y: 792,
		width: 36,
		height: 36,
		mask: true
	},
	phone_forwarded: {
		x: 396,
		y: 792,
		width: 36,
		height: 36,
		mask: true
	},
	phone_in_talk: {
		x: 432,
		y: 792,
		width: 36,
		height: 36,
		mask: true
	},
	phone_iphone: {
		x: 468,
		y: 792,
		width: 36,
		height: 36,
		mask: true
	},
	phone_locked: {
		x: 504,
		y: 792,
		width: 36,
		height: 36,
		mask: true
	},
	phone_missed: {
		x: 540,
		y: 792,
		width: 36,
		height: 36,
		mask: true
	},
	phone_paused: {
		x: 576,
		y: 792,
		width: 36,
		height: 36,
		mask: true
	},
	phonelink: {
		x: 612,
		y: 792,
		width: 36,
		height: 36,
		mask: true
	},
	phonelink_erase: {
		x: 648,
		y: 792,
		width: 36,
		height: 36,
		mask: true
	},
	phonelink_lock: {
		x: 684,
		y: 792,
		width: 36,
		height: 36,
		mask: true
	},
	phonelink_off: {
		x: 720,
		y: 792,
		width: 36,
		height: 36,
		mask: true
	},
	phonelink_ring: {
		x: 756,
		y: 792,
		width: 36,
		height: 36,
		mask: true
	},
	phonelink_setup: {
		x: 792,
		y: 792,
		width: 36,
		height: 36,
		mask: true
	},
	photo: {
		x: 828,
		y: 792,
		width: 36,
		height: 36,
		mask: true
	},
	photo_album: {
		x: 864,
		y: 792,
		width: 36,
		height: 36,
		mask: true
	},
	photo_camera: {
		x: 900,
		y: 792,
		width: 36,
		height: 36,
		mask: true
	},
	photo_filter: {
		x: 936,
		y: 792,
		width: 36,
		height: 36,
		mask: true
	},
	photo_library: {
		x: 972,
		y: 792,
		width: 36,
		height: 36,
		mask: true
	},
	photo_size_select_actual: {
		x: 0,
		y: 828,
		width: 36,
		height: 36,
		mask: true
	},
	photo_size_select_large: {
		x: 36,
		y: 828,
		width: 36,
		height: 36,
		mask: true
	},
	photo_size_select_small: {
		x: 72,
		y: 828,
		width: 36,
		height: 36,
		mask: true
	},
	picture_as_pdf: {
		x: 108,
		y: 828,
		width: 36,
		height: 36,
		mask: true
	},
	picture_in_picture: {
		x: 144,
		y: 828,
		width: 36,
		height: 36,
		mask: true
	},
	picture_in_picture_alt: {
		x: 180,
		y: 828,
		width: 36,
		height: 36,
		mask: true
	},
	pie_chart: {
		x: 216,
		y: 828,
		width: 36,
		height: 36,
		mask: true
	},
	pie_chart_outlined: {
		x: 252,
		y: 828,
		width: 36,
		height: 36,
		mask: true
	},
	pin_drop: {
		x: 288,
		y: 828,
		width: 36,
		height: 36,
		mask: true
	},
	place: {
		x: 324,
		y: 828,
		width: 36,
		height: 36,
		mask: true
	},
	play_arrow: {
		x: 360,
		y: 828,
		width: 36,
		height: 36,
		mask: true
	},
	play_circle_filled: {
		x: 396,
		y: 828,
		width: 36,
		height: 36,
		mask: true
	},
	play_circle_filled_white: {
		x: 432,
		y: 828,
		width: 36,
		height: 36,
		mask: true
	},
	play_circle_outline: {
		x: 468,
		y: 828,
		width: 36,
		height: 36,
		mask: true
	},
	play_for_work: {
		x: 504,
		y: 828,
		width: 36,
		height: 36,
		mask: true
	},
	playlist_add: {
		x: 540,
		y: 828,
		width: 36,
		height: 36,
		mask: true
	},
	playlist_add_check: {
		x: 576,
		y: 828,
		width: 36,
		height: 36,
		mask: true
	},
	playlist_play: {
		x: 612,
		y: 828,
		width: 36,
		height: 36,
		mask: true
	},
	plus_one: {
		x: 648,
		y: 828,
		width: 36,
		height: 36,
		mask: true
	},
	poll: {
		x: 684,
		y: 828,
		width: 36,
		height: 36,
		mask: true
	},
	polymer: {
		x: 720,
		y: 828,
		width: 36,
		height: 36,
		mask: true
	},
	pool: {
		x: 756,
		y: 828,
		width: 36,
		height: 36,
		mask: true
	},
	portable_wifi_off: {
		x: 792,
		y: 828,
		width: 36,
		height: 36,
		mask: true
	},
	portrait: {
		x: 828,
		y: 828,
		width: 36,
		height: 36,
		mask: true
	},
	power: {
		x: 864,
		y: 828,
		width: 36,
		height: 36,
		mask: true
	},
	power_input: {
		x: 900,
		y: 828,
		width: 36,
		height: 36,
		mask: true
	},
	power_settings_new: {
		x: 936,
		y: 828,
		width: 36,
		height: 36,
		mask: true
	},
	pregnant_woman: {
		x: 972,
		y: 828,
		width: 36,
		height: 36,
		mask: true
	},
	present_to_all: {
		x: 0,
		y: 864,
		width: 36,
		height: 36,
		mask: true
	},
	print: {
		x: 36,
		y: 864,
		width: 36,
		height: 36,
		mask: true
	},
	priority_high: {
		x: 72,
		y: 864,
		width: 36,
		height: 36,
		mask: true
	},
	public: {
		x: 108,
		y: 864,
		width: 36,
		height: 36,
		mask: true
	},
	publish: {
		x: 144,
		y: 864,
		width: 36,
		height: 36,
		mask: true
	},
	query_builder: {
		x: 180,
		y: 864,
		width: 36,
		height: 36,
		mask: true
	},
	question_answer: {
		x: 216,
		y: 864,
		width: 36,
		height: 36,
		mask: true
	},
	queue: {
		x: 252,
		y: 864,
		width: 36,
		height: 36,
		mask: true
	},
	queue_music: {
		x: 288,
		y: 864,
		width: 36,
		height: 36,
		mask: true
	},
	queue_play_next: {
		x: 324,
		y: 864,
		width: 36,
		height: 36,
		mask: true
	},
	radio: {
		x: 360,
		y: 864,
		width: 36,
		height: 36,
		mask: true
	},
	rate_review: {
		x: 396,
		y: 864,
		width: 36,
		height: 36,
		mask: true
	},
	receipt: {
		x: 432,
		y: 864,
		width: 36,
		height: 36,
		mask: true
	},
	recent_actors: {
		x: 468,
		y: 864,
		width: 36,
		height: 36,
		mask: true
	},
	record_voice_over: {
		x: 504,
		y: 864,
		width: 36,
		height: 36,
		mask: true
	},
	redeem: {
		x: 540,
		y: 864,
		width: 36,
		height: 36,
		mask: true
	},
	redo: {
		x: 576,
		y: 864,
		width: 36,
		height: 36,
		mask: true
	},
	refresh: {
		x: 612,
		y: 864,
		width: 36,
		height: 36,
		mask: true
	},
	remove: {
		x: 648,
		y: 864,
		width: 36,
		height: 36,
		mask: true
	},
	remove_circle: {
		x: 684,
		y: 864,
		width: 36,
		height: 36,
		mask: true
	},
	remove_circle_outline: {
		x: 720,
		y: 864,
		width: 36,
		height: 36,
		mask: true
	},
	remove_from_queue: {
		x: 756,
		y: 864,
		width: 36,
		height: 36,
		mask: true
	},
	remove_red_eye: {
		x: 792,
		y: 864,
		width: 36,
		height: 36,
		mask: true
	},
	remove_shopping_cart: {
		x: 828,
		y: 864,
		width: 36,
		height: 36,
		mask: true
	},
	reorder: {
		x: 864,
		y: 864,
		width: 36,
		height: 36,
		mask: true
	},
	repeat: {
		x: 900,
		y: 864,
		width: 36,
		height: 36,
		mask: true
	},
	repeat_one: {
		x: 936,
		y: 864,
		width: 36,
		height: 36,
		mask: true
	},
	replay: {
		x: 972,
		y: 864,
		width: 36,
		height: 36,
		mask: true
	},
	replay_5: {
		x: 0,
		y: 900,
		width: 36,
		height: 36,
		mask: true
	},
	replay_10: {
		x: 36,
		y: 900,
		width: 36,
		height: 36,
		mask: true
	},
	replay_30: {
		x: 72,
		y: 900,
		width: 36,
		height: 36,
		mask: true
	},
	reply: {
		x: 108,
		y: 900,
		width: 36,
		height: 36,
		mask: true
	},
	reply_all: {
		x: 144,
		y: 900,
		width: 36,
		height: 36,
		mask: true
	},
	report: {
		x: 180,
		y: 900,
		width: 36,
		height: 36,
		mask: true
	},
	report_problem: {
		x: 216,
		y: 900,
		width: 36,
		height: 36,
		mask: true
	},
	restaurant: {
		x: 252,
		y: 900,
		width: 36,
		height: 36,
		mask: true
	},
	restaurant_menu: {
		x: 288,
		y: 900,
		width: 36,
		height: 36,
		mask: true
	},
	restore: {
		x: 324,
		y: 900,
		width: 36,
		height: 36,
		mask: true
	},
	restore_page: {
		x: 360,
		y: 900,
		width: 36,
		height: 36,
		mask: true
	},
	ring_volume: {
		x: 396,
		y: 900,
		width: 36,
		height: 36,
		mask: true
	},
	room: {
		x: 432,
		y: 900,
		width: 36,
		height: 36,
		mask: true
	},
	room_service: {
		x: 468,
		y: 900,
		width: 36,
		height: 36,
		mask: true
	},
	rotate_90_degrees_ccw: {
		x: 504,
		y: 900,
		width: 36,
		height: 36,
		mask: true
	},
	rotate_left: {
		x: 540,
		y: 900,
		width: 36,
		height: 36,
		mask: true
	},
	rotate_right: {
		x: 576,
		y: 900,
		width: 36,
		height: 36,
		mask: true
	},
	rounded_corner: {
		x: 612,
		y: 900,
		width: 36,
		height: 36,
		mask: true
	},
	router: {
		x: 648,
		y: 900,
		width: 36,
		height: 36,
		mask: true
	},
	rowing: {
		x: 684,
		y: 900,
		width: 36,
		height: 36,
		mask: true
	},
	rss_feed: {
		x: 720,
		y: 900,
		width: 36,
		height: 36,
		mask: true
	},
	rv_hookup: {
		x: 756,
		y: 900,
		width: 36,
		height: 36,
		mask: true
	},
	satellite: {
		x: 792,
		y: 900,
		width: 36,
		height: 36,
		mask: true
	},
	save: {
		x: 828,
		y: 900,
		width: 36,
		height: 36,
		mask: true
	},
	scanner: {
		x: 864,
		y: 900,
		width: 36,
		height: 36,
		mask: true
	},
	schedule: {
		x: 900,
		y: 900,
		width: 36,
		height: 36,
		mask: true
	},
	school: {
		x: 936,
		y: 900,
		width: 36,
		height: 36,
		mask: true
	},
	screen_lock_landscape: {
		x: 972,
		y: 900,
		width: 36,
		height: 36,
		mask: true
	},
	screen_lock_portrait: {
		x: 0,
		y: 936,
		width: 36,
		height: 36,
		mask: true
	},
	screen_lock_rotation: {
		x: 36,
		y: 936,
		width: 36,
		height: 36,
		mask: true
	},
	screen_rotation: {
		x: 72,
		y: 936,
		width: 36,
		height: 36,
		mask: true
	},
	screen_share: {
		x: 108,
		y: 936,
		width: 36,
		height: 36,
		mask: true
	},
	sd_card: {
		x: 144,
		y: 936,
		width: 36,
		height: 36,
		mask: true
	},
	sd_storage: {
		x: 180,
		y: 936,
		width: 36,
		height: 36,
		mask: true
	},
	search: {
		x: 216,
		y: 936,
		width: 36,
		height: 36,
		mask: true
	},
	security: {
		x: 252,
		y: 936,
		width: 36,
		height: 36,
		mask: true
	},
	select_all: {
		x: 288,
		y: 936,
		width: 36,
		height: 36,
		mask: true
	},
	send: {
		x: 324,
		y: 936,
		width: 36,
		height: 36,
		mask: true
	},
	sentiment_dissatisfied: {
		x: 360,
		y: 936,
		width: 36,
		height: 36,
		mask: true
	},
	sentiment_neutral: {
		x: 396,
		y: 936,
		width: 36,
		height: 36,
		mask: true
	},
	sentiment_satisfied: {
		x: 432,
		y: 936,
		width: 36,
		height: 36,
		mask: true
	},
	sentiment_very_dissatisfied: {
		x: 468,
		y: 936,
		width: 36,
		height: 36,
		mask: true
	},
	sentiment_very_satisfied: {
		x: 504,
		y: 936,
		width: 36,
		height: 36,
		mask: true
	},
	settings: {
		x: 540,
		y: 936,
		width: 36,
		height: 36,
		mask: true
	},
	settings_applications: {
		x: 576,
		y: 936,
		width: 36,
		height: 36,
		mask: true
	},
	settings_backup_restore: {
		x: 612,
		y: 936,
		width: 36,
		height: 36,
		mask: true
	},
	settings_bluetooth: {
		x: 648,
		y: 936,
		width: 36,
		height: 36,
		mask: true
	},
	settings_brightness: {
		x: 684,
		y: 936,
		width: 36,
		height: 36,
		mask: true
	},
	settings_cell: {
		x: 720,
		y: 936,
		width: 36,
		height: 36,
		mask: true
	},
	settings_ethernet: {
		x: 756,
		y: 936,
		width: 36,
		height: 36,
		mask: true
	},
	settings_input_antenna: {
		x: 792,
		y: 936,
		width: 36,
		height: 36,
		mask: true
	},
	settings_input_component: {
		x: 828,
		y: 936,
		width: 36,
		height: 36,
		mask: true
	},
	settings_input_composite: {
		x: 864,
		y: 936,
		width: 36,
		height: 36,
		mask: true
	},
	settings_input_hdmi: {
		x: 900,
		y: 936,
		width: 36,
		height: 36,
		mask: true
	},
	settings_input_svideo: {
		x: 936,
		y: 936,
		width: 36,
		height: 36,
		mask: true
	},
	settings_overscan: {
		x: 972,
		y: 936,
		width: 36,
		height: 36,
		mask: true
	},
	settings_phone: {
		x: 0,
		y: 972,
		width: 36,
		height: 36,
		mask: true
	},
	settings_power: {
		x: 36,
		y: 972,
		width: 36,
		height: 36,
		mask: true
	},
	settings_remote: {
		x: 72,
		y: 972,
		width: 36,
		height: 36,
		mask: true
	},
	settings_system_daydream: {
		x: 108,
		y: 972,
		width: 36,
		height: 36,
		mask: true
	},
	settings_voice: {
		x: 144,
		y: 972,
		width: 36,
		height: 36,
		mask: true
	},
	share: {
		x: 180,
		y: 972,
		width: 36,
		height: 36,
		mask: true
	},
	shop: {
		x: 216,
		y: 972,
		width: 36,
		height: 36,
		mask: true
	},
	shop_two: {
		x: 252,
		y: 972,
		width: 36,
		height: 36,
		mask: true
	},
	shopping_basket: {
		x: 288,
		y: 972,
		width: 36,
		height: 36,
		mask: true
	},
	shopping_cart: {
		x: 324,
		y: 972,
		width: 36,
		height: 36,
		mask: true
	},
	short_text: {
		x: 360,
		y: 972,
		width: 36,
		height: 36,
		mask: true
	},
	show_chart: {
		x: 396,
		y: 972,
		width: 36,
		height: 36,
		mask: true
	},
	shuffle: {
		x: 432,
		y: 972,
		width: 36,
		height: 36,
		mask: true
	},
	signal_cellular_0_bar: {
		x: 468,
		y: 972,
		width: 36,
		height: 36,
		mask: true
	},
	signal_cellular_1_bar: {
		x: 504,
		y: 972,
		width: 36,
		height: 36,
		mask: true
	},
	signal_cellular_2_bar: {
		x: 540,
		y: 972,
		width: 36,
		height: 36,
		mask: true
	},
	signal_cellular_3_bar: {
		x: 576,
		y: 972,
		width: 36,
		height: 36,
		mask: true
	},
	signal_cellular_4_bar: {
		x: 612,
		y: 972,
		width: 36,
		height: 36,
		mask: true
	},
	signal_cellular_connected_no_internet_0_bar: {
		x: 648,
		y: 972,
		width: 36,
		height: 36,
		mask: true
	},
	signal_cellular_connected_no_internet_1_bar: {
		x: 684,
		y: 972,
		width: 36,
		height: 36,
		mask: true
	},
	signal_cellular_connected_no_internet_2_bar: {
		x: 720,
		y: 972,
		width: 36,
		height: 36,
		mask: true
	},
	signal_cellular_connected_no_internet_3_bar: {
		x: 756,
		y: 972,
		width: 36,
		height: 36,
		mask: true
	},
	signal_cellular_connected_no_internet_4_bar: {
		x: 792,
		y: 972,
		width: 36,
		height: 36,
		mask: true
	},
	signal_cellular_no_sim: {
		x: 828,
		y: 972,
		width: 36,
		height: 36,
		mask: true
	},
	signal_cellular_null: {
		x: 864,
		y: 972,
		width: 36,
		height: 36,
		mask: true
	},
	signal_cellular_off: {
		x: 900,
		y: 972,
		width: 36,
		height: 36,
		mask: true
	},
	signal_wifi_0_bar: {
		x: 936,
		y: 972,
		width: 36,
		height: 36,
		mask: true
	},
	signal_wifi_1_bar: {
		x: 972,
		y: 972,
		width: 36,
		height: 36,
		mask: true
	},
	signal_wifi_1_bar_lock: {
		x: 0,
		y: 1008,
		width: 36,
		height: 36,
		mask: true
	},
	signal_wifi_2_bar: {
		x: 36,
		y: 1008,
		width: 36,
		height: 36,
		mask: true
	},
	signal_wifi_2_bar_lock: {
		x: 72,
		y: 1008,
		width: 36,
		height: 36,
		mask: true
	},
	signal_wifi_3_bar: {
		x: 108,
		y: 1008,
		width: 36,
		height: 36,
		mask: true
	},
	signal_wifi_3_bar_lock: {
		x: 144,
		y: 1008,
		width: 36,
		height: 36,
		mask: true
	},
	signal_wifi_4_bar: {
		x: 180,
		y: 1008,
		width: 36,
		height: 36,
		mask: true
	},
	signal_wifi_4_bar_lock: {
		x: 216,
		y: 1008,
		width: 36,
		height: 36,
		mask: true
	},
	signal_wifi_off: {
		x: 252,
		y: 1008,
		width: 36,
		height: 36,
		mask: true
	},
	sim_card: {
		x: 288,
		y: 1008,
		width: 36,
		height: 36,
		mask: true
	},
	sim_card_alert: {
		x: 324,
		y: 1008,
		width: 36,
		height: 36,
		mask: true
	},
	skip_next: {
		x: 360,
		y: 1008,
		width: 36,
		height: 36,
		mask: true
	},
	skip_previous: {
		x: 396,
		y: 1008,
		width: 36,
		height: 36,
		mask: true
	},
	slideshow: {
		x: 432,
		y: 1008,
		width: 36,
		height: 36,
		mask: true
	},
	slow_motion_video: {
		x: 468,
		y: 1008,
		width: 36,
		height: 36,
		mask: true
	},
	smartphone: {
		x: 504,
		y: 1008,
		width: 36,
		height: 36,
		mask: true
	},
	smoke_free: {
		x: 540,
		y: 1008,
		width: 36,
		height: 36,
		mask: true
	},
	smoking_rooms: {
		x: 576,
		y: 1008,
		width: 36,
		height: 36,
		mask: true
	},
	sms: {
		x: 612,
		y: 1008,
		width: 36,
		height: 36,
		mask: true
	},
	sms_failed: {
		x: 648,
		y: 1008,
		width: 36,
		height: 36,
		mask: true
	},
	snooze: {
		x: 684,
		y: 1008,
		width: 36,
		height: 36,
		mask: true
	},
	sort: {
		x: 720,
		y: 1008,
		width: 36,
		height: 36,
		mask: true
	},
	sort_by_alpha: {
		x: 756,
		y: 1008,
		width: 36,
		height: 36,
		mask: true
	},
	spa: {
		x: 792,
		y: 1008,
		width: 36,
		height: 36,
		mask: true
	},
	space_bar: {
		x: 828,
		y: 1008,
		width: 36,
		height: 36,
		mask: true
	},
	speaker: {
		x: 864,
		y: 1008,
		width: 36,
		height: 36,
		mask: true
	},
	speaker_group: {
		x: 900,
		y: 1008,
		width: 36,
		height: 36,
		mask: true
	},
	speaker_notes: {
		x: 936,
		y: 1008,
		width: 36,
		height: 36,
		mask: true
	},
	speaker_notes_off: {
		x: 972,
		y: 1008,
		width: 36,
		height: 36,
		mask: true
	},
	speaker_phone: {
		x: 0,
		y: 1044,
		width: 36,
		height: 36,
		mask: true
	},
	spellcheck: {
		x: 36,
		y: 1044,
		width: 36,
		height: 36,
		mask: true
	},
	square: {
		x: 72,
		y: 1044,
		width: 24,
		height: 24,
		mask: true
	},
	square_filled: {
		x: 96,
		y: 1044,
		width: 24,
		height: 24,
		mask: true
	},
	star: {
		x: 120,
		y: 1044,
		width: 36,
		height: 36,
		mask: true
	},
	star_border: {
		x: 156,
		y: 1044,
		width: 36,
		height: 36,
		mask: true
	},
	star_half: {
		x: 192,
		y: 1044,
		width: 36,
		height: 36,
		mask: true
	},
	stars: {
		x: 228,
		y: 1044,
		width: 36,
		height: 36,
		mask: true
	},
	stay_current_landscape: {
		x: 264,
		y: 1044,
		width: 36,
		height: 36,
		mask: true
	},
	stay_current_portrait: {
		x: 300,
		y: 1044,
		width: 36,
		height: 36,
		mask: true
	},
	stay_primary_landscape: {
		x: 336,
		y: 1044,
		width: 36,
		height: 36,
		mask: true
	},
	stay_primary_portrait: {
		x: 372,
		y: 1044,
		width: 36,
		height: 36,
		mask: true
	},
	stop: {
		x: 408,
		y: 1044,
		width: 36,
		height: 36,
		mask: true
	},
	stop_screen_share: {
		x: 444,
		y: 1044,
		width: 36,
		height: 36,
		mask: true
	},
	storage: {
		x: 480,
		y: 1044,
		width: 36,
		height: 36,
		mask: true
	},
	store: {
		x: 516,
		y: 1044,
		width: 36,
		height: 36,
		mask: true
	},
	store_mall_directory: {
		x: 552,
		y: 1044,
		width: 36,
		height: 36,
		mask: true
	},
	straighten: {
		x: 588,
		y: 1044,
		width: 36,
		height: 36,
		mask: true
	},
	streetview: {
		x: 624,
		y: 1044,
		width: 36,
		height: 36,
		mask: true
	},
	strikethrough_s: {
		x: 660,
		y: 1044,
		width: 36,
		height: 36,
		mask: true
	},
	style: {
		x: 696,
		y: 1044,
		width: 36,
		height: 36,
		mask: true
	},
	subdirectory_arrow_left: {
		x: 732,
		y: 1044,
		width: 36,
		height: 36,
		mask: true
	},
	subdirectory_arrow_right: {
		x: 768,
		y: 1044,
		width: 36,
		height: 36,
		mask: true
	},
	subject: {
		x: 804,
		y: 1044,
		width: 36,
		height: 36,
		mask: true
	},
	subscriptions: {
		x: 840,
		y: 1044,
		width: 36,
		height: 36,
		mask: true
	},
	subtitles: {
		x: 876,
		y: 1044,
		width: 36,
		height: 36,
		mask: true
	},
	subway: {
		x: 912,
		y: 1044,
		width: 36,
		height: 36,
		mask: true
	},
	supervisor_account: {
		x: 948,
		y: 1044,
		width: 36,
		height: 36,
		mask: true
	},
	surround_sound: {
		x: 0,
		y: 1080,
		width: 36,
		height: 36,
		mask: true
	},
	swap_calls: {
		x: 36,
		y: 1080,
		width: 36,
		height: 36,
		mask: true
	},
	swap_horiz: {
		x: 72,
		y: 1080,
		width: 36,
		height: 36,
		mask: true
	},
	swap_vert: {
		x: 108,
		y: 1080,
		width: 36,
		height: 36,
		mask: true
	},
	swap_vertical_circle: {
		x: 144,
		y: 1080,
		width: 36,
		height: 36,
		mask: true
	},
	switch_camera: {
		x: 180,
		y: 1080,
		width: 36,
		height: 36,
		mask: true
	},
	switch_video: {
		x: 216,
		y: 1080,
		width: 36,
		height: 36,
		mask: true
	},
	sync: {
		x: 252,
		y: 1080,
		width: 36,
		height: 36,
		mask: true
	},
	sync_disabled: {
		x: 288,
		y: 1080,
		width: 36,
		height: 36,
		mask: true
	},
	sync_problem: {
		x: 324,
		y: 1080,
		width: 36,
		height: 36,
		mask: true
	},
	system_update: {
		x: 360,
		y: 1080,
		width: 36,
		height: 36,
		mask: true
	},
	system_update_alt: {
		x: 396,
		y: 1080,
		width: 36,
		height: 36,
		mask: true
	},
	tab: {
		x: 432,
		y: 1080,
		width: 36,
		height: 36,
		mask: true
	},
	tab_unselected: {
		x: 468,
		y: 1080,
		width: 36,
		height: 36,
		mask: true
	},
	tablet: {
		x: 504,
		y: 1080,
		width: 36,
		height: 36,
		mask: true
	},
	tablet_android: {
		x: 540,
		y: 1080,
		width: 36,
		height: 36,
		mask: true
	},
	tablet_mac: {
		x: 576,
		y: 1080,
		width: 36,
		height: 36,
		mask: true
	},
	tag_faces: {
		x: 612,
		y: 1080,
		width: 36,
		height: 36,
		mask: true
	},
	tap_and_play: {
		x: 648,
		y: 1080,
		width: 36,
		height: 36,
		mask: true
	},
	terrain: {
		x: 684,
		y: 1080,
		width: 36,
		height: 36,
		mask: true
	},
	text_fields: {
		x: 720,
		y: 1080,
		width: 36,
		height: 36,
		mask: true
	},
	text_format: {
		x: 756,
		y: 1080,
		width: 36,
		height: 36,
		mask: true
	},
	textsms: {
		x: 792,
		y: 1080,
		width: 36,
		height: 36,
		mask: true
	},
	texture: {
		x: 828,
		y: 1080,
		width: 36,
		height: 36,
		mask: true
	},
	theaters: {
		x: 864,
		y: 1080,
		width: 36,
		height: 36,
		mask: true
	},
	thumb_down: {
		x: 900,
		y: 1080,
		width: 36,
		height: 36,
		mask: true
	},
	thumb_up: {
		x: 936,
		y: 1080,
		width: 36,
		height: 36,
		mask: true
	},
	thumbs_up_down: {
		x: 972,
		y: 1080,
		width: 36,
		height: 36,
		mask: true
	},
	time_to_leave: {
		x: 0,
		y: 1116,
		width: 36,
		height: 36,
		mask: true
	},
	timelapse: {
		x: 36,
		y: 1116,
		width: 36,
		height: 36,
		mask: true
	},
	timeline: {
		x: 72,
		y: 1116,
		width: 36,
		height: 36,
		mask: true
	},
	timer: {
		x: 108,
		y: 1116,
		width: 36,
		height: 36,
		mask: true
	},
	timer_3: {
		x: 144,
		y: 1116,
		width: 36,
		height: 36,
		mask: true
	},
	timer_10: {
		x: 180,
		y: 1116,
		width: 36,
		height: 36,
		mask: true
	},
	timer_off: {
		x: 216,
		y: 1116,
		width: 36,
		height: 36,
		mask: true
	},
	title: {
		x: 252,
		y: 1116,
		width: 36,
		height: 36,
		mask: true
	},
	toc: {
		x: 288,
		y: 1116,
		width: 36,
		height: 36,
		mask: true
	},
	today: {
		x: 324,
		y: 1116,
		width: 36,
		height: 36,
		mask: true
	},
	toll: {
		x: 360,
		y: 1116,
		width: 36,
		height: 36,
		mask: true
	},
	tonality: {
		x: 396,
		y: 1116,
		width: 36,
		height: 36,
		mask: true
	},
	touch_app: {
		x: 432,
		y: 1116,
		width: 36,
		height: 36,
		mask: true
	},
	toys: {
		x: 468,
		y: 1116,
		width: 36,
		height: 36,
		mask: true
	},
	track_changes: {
		x: 504,
		y: 1116,
		width: 36,
		height: 36,
		mask: true
	},
	traffic: {
		x: 540,
		y: 1116,
		width: 36,
		height: 36,
		mask: true
	},
	train: {
		x: 576,
		y: 1116,
		width: 36,
		height: 36,
		mask: true
	},
	tram: {
		x: 612,
		y: 1116,
		width: 36,
		height: 36,
		mask: true
	},
	transfer_within_a_station: {
		x: 648,
		y: 1116,
		width: 36,
		height: 36,
		mask: true
	},
	transform: {
		x: 684,
		y: 1116,
		width: 36,
		height: 36,
		mask: true
	},
	translate: {
		x: 720,
		y: 1116,
		width: 36,
		height: 36,
		mask: true
	},
	trending_down: {
		x: 756,
		y: 1116,
		width: 36,
		height: 36,
		mask: true
	},
	trending_flat: {
		x: 792,
		y: 1116,
		width: 36,
		height: 36,
		mask: true
	},
	trending_up: {
		x: 828,
		y: 1116,
		width: 36,
		height: 36,
		mask: true
	},
	triangle: {
		x: 864,
		y: 1116,
		width: 24,
		height: 24,
		mask: true
	},
	tune: {
		x: 888,
		y: 1116,
		width: 36,
		height: 36,
		mask: true
	},
	turned_in: {
		x: 924,
		y: 1116,
		width: 36,
		height: 36,
		mask: true
	},
	turned_in_not: {
		x: 960,
		y: 1116,
		width: 36,
		height: 36,
		mask: true
	},
	tv: {
		x: 0,
		y: 1152,
		width: 36,
		height: 36,
		mask: true
	},
	unarchive: {
		x: 36,
		y: 1152,
		width: 36,
		height: 36,
		mask: true
	},
	undo: {
		x: 72,
		y: 1152,
		width: 36,
		height: 36,
		mask: true
	},
	unfold_less: {
		x: 108,
		y: 1152,
		width: 36,
		height: 36,
		mask: true
	},
	unfold_more: {
		x: 144,
		y: 1152,
		width: 36,
		height: 36,
		mask: true
	},
	update: {
		x: 180,
		y: 1152,
		width: 36,
		height: 36,
		mask: true
	},
	usb: {
		x: 216,
		y: 1152,
		width: 36,
		height: 36,
		mask: true
	},
	verified_user: {
		x: 252,
		y: 1152,
		width: 36,
		height: 36,
		mask: true
	},
	vertical_align_bottom: {
		x: 288,
		y: 1152,
		width: 36,
		height: 36,
		mask: true
	},
	vertical_align_center: {
		x: 324,
		y: 1152,
		width: 36,
		height: 36,
		mask: true
	},
	vertical_align_top: {
		x: 360,
		y: 1152,
		width: 36,
		height: 36,
		mask: true
	},
	vibration: {
		x: 396,
		y: 1152,
		width: 36,
		height: 36,
		mask: true
	},
	video_call: {
		x: 432,
		y: 1152,
		width: 36,
		height: 36,
		mask: true
	},
	video_label: {
		x: 468,
		y: 1152,
		width: 36,
		height: 36,
		mask: true
	},
	video_library: {
		x: 504,
		y: 1152,
		width: 36,
		height: 36,
		mask: true
	},
	videocam: {
		x: 540,
		y: 1152,
		width: 36,
		height: 36,
		mask: true
	},
	videocam_off: {
		x: 576,
		y: 1152,
		width: 36,
		height: 36,
		mask: true
	},
	videogame_asset: {
		x: 612,
		y: 1152,
		width: 36,
		height: 36,
		mask: true
	},
	view_agenda: {
		x: 648,
		y: 1152,
		width: 36,
		height: 36,
		mask: true
	},
	view_array: {
		x: 684,
		y: 1152,
		width: 36,
		height: 36,
		mask: true
	},
	view_carousel: {
		x: 720,
		y: 1152,
		width: 36,
		height: 36,
		mask: true
	},
	view_column: {
		x: 756,
		y: 1152,
		width: 36,
		height: 36,
		mask: true
	},
	view_comfy: {
		x: 792,
		y: 1152,
		width: 36,
		height: 36,
		mask: true
	},
	view_compact: {
		x: 828,
		y: 1152,
		width: 36,
		height: 36,
		mask: true
	},
	view_day: {
		x: 864,
		y: 1152,
		width: 36,
		height: 36,
		mask: true
	},
	view_headline: {
		x: 900,
		y: 1152,
		width: 36,
		height: 36,
		mask: true
	},
	view_list: {
		x: 936,
		y: 1152,
		width: 36,
		height: 36,
		mask: true
	},
	view_module: {
		x: 972,
		y: 1152,
		width: 36,
		height: 36,
		mask: true
	},
	view_quilt: {
		x: 0,
		y: 1188,
		width: 36,
		height: 36,
		mask: true
	},
	view_stream: {
		x: 36,
		y: 1188,
		width: 36,
		height: 36,
		mask: true
	},
	view_week: {
		x: 72,
		y: 1188,
		width: 36,
		height: 36,
		mask: true
	},
	vignette: {
		x: 108,
		y: 1188,
		width: 36,
		height: 36,
		mask: true
	},
	visibility: {
		x: 144,
		y: 1188,
		width: 36,
		height: 36,
		mask: true
	},
	visibility_off: {
		x: 180,
		y: 1188,
		width: 36,
		height: 36,
		mask: true
	},
	voice_chat: {
		x: 216,
		y: 1188,
		width: 36,
		height: 36,
		mask: true
	},
	voicemail: {
		x: 252,
		y: 1188,
		width: 36,
		height: 36,
		mask: true
	},
	volume_down: {
		x: 288,
		y: 1188,
		width: 36,
		height: 36,
		mask: true
	},
	volume_mute: {
		x: 324,
		y: 1188,
		width: 36,
		height: 36,
		mask: true
	},
	volume_off: {
		x: 360,
		y: 1188,
		width: 36,
		height: 36,
		mask: true
	},
	volume_up: {
		x: 396,
		y: 1188,
		width: 36,
		height: 36,
		mask: true
	},
	vpn_key: {
		x: 432,
		y: 1188,
		width: 36,
		height: 36,
		mask: true
	},
	vpn_lock: {
		x: 468,
		y: 1188,
		width: 36,
		height: 36,
		mask: true
	},
	wallpaper: {
		x: 504,
		y: 1188,
		width: 36,
		height: 36,
		mask: true
	},
	warning: {
		x: 540,
		y: 1188,
		width: 36,
		height: 36,
		mask: true
	},
	watch: {
		x: 576,
		y: 1188,
		width: 36,
		height: 36,
		mask: true
	},
	watch_later: {
		x: 612,
		y: 1188,
		width: 36,
		height: 36,
		mask: true
	},
	wb_auto: {
		x: 648,
		y: 1188,
		width: 36,
		height: 36,
		mask: true
	},
	wb_cloudy: {
		x: 684,
		y: 1188,
		width: 36,
		height: 36,
		mask: true
	},
	wb_incandescent: {
		x: 720,
		y: 1188,
		width: 36,
		height: 36,
		mask: true
	},
	wb_iridescent: {
		x: 756,
		y: 1188,
		width: 36,
		height: 36,
		mask: true
	},
	wb_sunny: {
		x: 792,
		y: 1188,
		width: 36,
		height: 36,
		mask: true
	},
	wc: {
		x: 828,
		y: 1188,
		width: 36,
		height: 36,
		mask: true
	},
	web: {
		x: 864,
		y: 1188,
		width: 36,
		height: 36,
		mask: true
	},
	web_asset: {
		x: 900,
		y: 1188,
		width: 36,
		height: 36,
		mask: true
	},
	weekend: {
		x: 936,
		y: 1188,
		width: 36,
		height: 36,
		mask: true
	},
	whatshot: {
		x: 972,
		y: 1188,
		width: 36,
		height: 36,
		mask: true
	},
	widgets: {
		x: 0,
		y: 1224,
		width: 36,
		height: 36,
		mask: true
	},
	wifi: {
		x: 36,
		y: 1224,
		width: 36,
		height: 36,
		mask: true
	},
	wifi_lock: {
		x: 72,
		y: 1224,
		width: 36,
		height: 36,
		mask: true
	},
	wifi_tethering: {
		x: 108,
		y: 1224,
		width: 36,
		height: 36,
		mask: true
	},
	work: {
		x: 144,
		y: 1224,
		width: 36,
		height: 36,
		mask: true
	},
	wrap_text: {
		x: 180,
		y: 1224,
		width: 36,
		height: 36,
		mask: true
	},
	youtube_searched_for: {
		x: 216,
		y: 1224,
		width: 36,
		height: 36,
		mask: true
	},
	zoom_in: {
		x: 252,
		y: 1224,
		width: 36,
		height: 36,
		mask: true
	},
	zoom_out: {
		x: 288,
		y: 1224,
		width: 36,
		height: 36,
		mask: true
	},
	zoom_out_map: {
		x: 324,
		y: 1224,
		width: 36,
		height: 36,
		mask: true
	}
}