import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import React from "react";
import { SimpleMap as Map } from "./index";
import "./stories.css";

export interface GridProps {
  controls: Array<any>;
  layers: Array<any>;
  view?: any;
  baseMap?: any;
}

export const DemoGrid = (props: GridProps) => {
  const { controls, layers, view, baseMap } = props;

  return (
    <div>
      <div style={{ margin: "1em auto 0.5em" }} id="parentA">
        {controls.map((c, i) => (
          <Grid key={`controls-${i}`} container spacing={1} id="cont">
            <Grid key={`item-label-${i}`} item xs={1}>
              <Typography id="control-lb" gutterBottom variant="subtitle2">
                {c.label}
              </Typography>
            </Grid>
            <Grid key={`item-${i}`} item xs={11}>
              {c.control}
            </Grid>
          </Grid>
        ))}
      </div>
      <div style={{ position: "relative" }} id="parentB">
        <Grid>
          <Grid item xs={12} style={{ fontFamily: "Material" }}>
            <Map
              height={600}
              layers={layers}
              initialView={view}
              baseMap={baseMap}
            />
          </Grid>
        </Grid>
      </div>
    </div>
  );
};

export default DemoGrid;
