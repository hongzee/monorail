import FontIconPicker from '@fonticonpicker/react-fonticonpicker';
import Icon from '@material-ui/core/Icon';
import React, { useState } from 'react';
// import '@fonticonpicker/react-fonticonpicker/dist/fonticonpicker.base-theme.react.css';
// import '@fonticonpicker/react-fonticonpicker/dist/fonticonpicker.material-theme.react.css';
import './fonticonpicker.base-theme.react.css';
import './fonticonpicker.material-theme.react.css';



export default {
  title: 'Icon Picker',
};

const ICONS = [
  { rotatable: true, rotation: 0, key: 'none', label: 'none' },
  { rotatable: true, rotation: 0, key: 'navigation', label: 'navigation' },
  { rotatable: false, rotation: 0, key: 'fiber_manual_record', label: 'circle' },
  { rotatable: false, rotation: 0, key: 'fiber_manual_record_filled', label: 'circle_filled' },
  { rotatable: false, rotation: 0, key: 'diamond', label: 'diamond' },
  { rotatable: false, rotation: 0, key: 'diamond_filled', label: 'diamond_filled' },
  { rotatable: false, rotation: 0, key: 'square', label: 'square' },
  { rotatable: false, rotation: 0, key: 'square_filled', label: 'square_filled' },
  { rotatable: true, rotation: 0, key: 'expand_less', label: 'small_arrow' },
  { rotatable: true, rotation: 0, key: 'change_history', label: 'triangle' },
  { rotatable: true, rotation: 0, key: 'change_history_filled', label: 'triangle_filled' },
  { rotatable: false, rotation: 0, key: 'directions_car', label: 'car' },
  { rotatable: false, rotation: 0, key: 'adjust', label: 'circle_dot' },
  { rotatable: false, rotation: 0, key: 'my_location', label: 'my_location' },
  { rotatable: false, rotation: 0, key: 'place', label: 'place' },
  { rotatable: false, rotation: 0, key: 'location_searching', label: 'location_searching' },
  { rotatable: false, rotation: 0, key: 'account_box', label: 'account_box' },
  { rotatable: false, rotation: 0, key: 'account_circle', label: 'account_circle' },
  { rotatable: true, rotation: 0, key: 'arrow_drop_up', label: 'arrow_head' },
  { rotatable: false, rotation: 0, key: 'speaker_phone', label: 'speaker_phone' },
  { rotatable: false, rotation: 0, key: 'add_box', label: 'add_box' },
  { rotatable: false, rotation: 0, key: 'add_circle', label: 'add_circle' },
  { rotatable: false, rotation: 0, key: 'add_circle_outline', label: 'add_circle_outline' },
  { rotatable: false, rotation: 0, key: 'phone_iphone', label: 'phone_iphone' },
  { rotatable: false, rotation: 0, key: 'tap_and_play', label: 'phone_signal' },
  { rotatable: true, rotation: -90, key: 'keyboard_backspace', label: 'arrow' },
  { rotatable: true, rotation: 0, key: 'phonelink_ring', label: 'phonelink_ring' },
  { rotatable: true, rotation: 0, key: 'commute', label: 'commute' },
  { rotatable: true, rotation: 0, key: 'timer', label: 'timer' },
  { rotatable: true, rotation: 0, key: 'report', label: 'stop' },
  { rotatable: true, rotation: 0, key: 'payment', label: 'payment' },
  { rotatable: true, rotation: 0, key: 'monetization_on', label: 'money' },
  { rotatable: true, rotation: 0, key: 'local_atm', label: 'local_atm' },
  { rotatable: true, rotation: 0, key: 'receipt_long', label: 'receipt_long' },
];

const renderIcon = (svg) => {
  console.log('svg:', svg);
  return (<Icon color='primary'>{svg}</Icon>);
};

export const IconPicker = () => {
  const [data, setData] = useState('');
  const handleChange = (value) => {
    console.log(value);
    setData(value);
  };
  //these are custom markers that do not exist in material-ui
  const customIcons = ['diamond', 'diamond_filled', 'square', 'square_filled'];

  const filteredIcons = ICONS.filter((i) => !customIcons.includes(i.key)).map((i) => i.key);

  const props = {
    icons: filteredIcons,
    renderUsing: 'class',
    value: data,
    onChange: handleChange,
    isMulti: false,
    closeOnSelect: true,
    renderFunc:renderIcon 
  };
  return (
    <div>
      <FontIconPicker {...props} />
    </div>
  );
};
