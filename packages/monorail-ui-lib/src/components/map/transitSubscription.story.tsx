import React from 'react';
import keyMirror from 'keymirror';
import { withKnobs, select,  optionsKnob as options } from '@storybook/addon-knobs';
import { TransitSubscriptionDataScroller, LiveTransitMap as LiveMap , PRESET_FILTERS } from './transitSubscription';

export default {
  title: 'Live Transit',
  decorators: [withKnobs],
};

const filterKeys = keyMirror(PRESET_FILTERS);

const BASE_MAPS = {
  TOPO: 'https://api.maptiler.com/maps/topo/style.json?key=R01BPnJMraEGqoIU2zM6',
  DARKMATTER: 'https://api.maptiler.com/maps/darkmatter/style.json?key=R01BPnJMraEGqoIU2zM6',
  STREETS_3D: 'https://api.maptiler.com/maps/streets/style.json?key=R01BPnJMraEGqoIU2zM6',
  BRIGHT: 'https://api.maptiler.com/maps/bright/style.json?key=R01BPnJMraEGqoIU2zM6',
  TONER: 'https://api.maptiler.com/maps/toner/style.json?key=R01BPnJMraEGqoIU2zM6',
  SATELLITE_LOW_RES: 'https://api.maptiler.com/maps/hybrid/style.json?key=R01BPnJMraEGqoIU2zM6',
};

const createBaseLayerKnob = () => {  
  const defaultValue = BASE_MAPS.STREETS_3D;
  return options('Select base map', BASE_MAPS, defaultValue, {display: 'inline-radio'}, "livemap");
};

export const LiveTransitDataTable = () => {
  const filterKey = select('Choose Live data:', filterKeys, filterKeys['all_trams'], 'livemap');  
  const filter = PRESET_FILTERS[filterKey];
  return <TransitSubscriptionDataScroller filter={filter} />;
};

export const LiveTransitMap = () => {
  const filterKey = select('Choose Live data:', filterKeys, filterKeys['all_trams'], 'livemap');  
  const filter = PRESET_FILTERS[filterKey];
  const baseMap = createBaseLayerKnob();
  return <LiveMap filter={filter} baseMap={baseMap}/>;
};
