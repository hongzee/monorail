import { DataFilterExtension, PathStyleExtension } from '@deck.gl/extensions';
import { IconLayer, PathLayer, ScatterplotLayer } from '@deck.gl/layers';
import { makeStyles } from '@material-ui/core/styles';
import { boolean, number, optionsKnob as options, select, withKnobs } from '@storybook/addon-knobs';
import destination from '@turf/destination';
import { getCoord } from '@turf/invariant';
import chroma from 'chroma-js';
import Icon from '@material-ui/core/Icon';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Link from '@material-ui/core/Link';
import * as d3Array from 'd3-array';
import { json as jsonFetch } from 'd3-fetch';
import _pick from 'lodash/pick';
import _random from 'lodash/random';
import _min from 'lodash/min';
import _max from 'lodash/max';
import _range from 'lodash/range';
import _reduce from 'lodash/reduce';
import Slider from '@material-ui/core/Slider';
import _values from 'lodash/values';
import _sortBy from 'lodash/sortBy';
import React, { useEffect, useState, useCallback } from 'react';
import Typography from '@material-ui/core/Typography';
import DemoGrid from './demoGrid';
import { ICON_MAPPING } from './icon-mapping';
import moment from 'moment-timezone';
import { MAP_DEFAULT_VIEW } from './cache';
import './stories.css';

const VIEWS = {
  ottawa: {
    ...MAP_DEFAULT_VIEW,
    longitude: -75.690308,
    latitude: 45.421106,
    zoom: 13,
  },
  ottawaNorth: {
    longitude: -75.7882,
    latitude: 45.4822,
    zoom: 11,
    pitch: 40,
    bearing: 180,
  },
  helsinki: { ...MAP_DEFAULT_VIEW, longitude: 24.9413548, latitude: 60.1717794, zoom: 10 },
};

export default {
  title: 'Map Tests/UI',
  decorators: [withKnobs],
};

const TYPE1_GROUP = 'Type 1 Data Controls';
const TYPE2_GROUP = 'Type 2 Data Controls';
const PATH_GROUP = 'Path Controls';
const FILTER_GROUP = 'Data Filter Controls';
const BASE_GROUP = 'Colours and Base Map';

const createTrailKnob = () => {
  const times = {
    '10 min': '10',
    '30 min': '30',
    '1 hour': '60',
    '2 hours': '120',
    '6 hours': '360',
    '1 day': '1440',
    '2 days': '2480',
    '3 days': '4320',
  };
  return options('trail length (minutes/hours/days)', times, '1440', { display: 'inline-radio' }, FILTER_GROUP);
};

const BASE_MAPS = {
  TOPO: 'https://api.maptiler.com/maps/topo/style.json?key=R01BPnJMraEGqoIU2zM6',
  DARKMATTER: 'https://api.maptiler.com/maps/darkmatter/style.json?key=R01BPnJMraEGqoIU2zM6',
  STREETS_3D: 'https://api.maptiler.com/maps/streets/style.json?key=R01BPnJMraEGqoIU2zM6',
  BASIC: 'https://api.maptiler.com/maps/basic/style.json?key=R01BPnJMraEGqoIU2zM6',
  BRIGHT: 'https://api.maptiler.com/maps/bright/style.json?key=R01BPnJMraEGqoIU2zM6',
  POSITRON: 'https://api.maptiler.com/maps/positron/style.json?key=R01BPnJMraEGqoIU2zM6',
  TONER: 'https://api.maptiler.com/maps/toner/style.json?key=R01BPnJMraEGqoIU2zM6',
  SATELLITE_LOW_RES: 'https://api.maptiler.com/maps/hybrid/style.json?key=R01BPnJMraEGqoIU2zM6',
};

const createBaseLayerKnob = () => {
  const defaultValue = BASE_MAPS.STREETS_3D;
  return options('base map', BASE_MAPS, defaultValue, { display: 'inline-radio' }, BASE_GROUP);
};

const TRIPS_URLS = ['1', '10', '20', '50', '100', '200'].reduce((acc, val, i, arr) => {
  acc[val] = `https://infratrode.gitlab.io/public-data/transit/trips-${val}.json`;
  return acc;
}, {});
const SINGLE_TRIP_URL = Object.values(TRIPS_URLS)[0];
const TRIPS_5DAYS_URL = 'https://infratrode.gitlab.io/public-data/transit/events-days-5.json';

const createTripsKnob = () => {
  const defaultURL = Object.values(TRIPS_URLS)[3];
  return options('Number of Trips', TRIPS_URLS, defaultURL, { display: 'inline-radio' }, TYPE1_GROUP);
};

const createColourKnob = () => {
  const schemes = {
    Paired: 'Paired',
    Dark2: 'Dark2',
    Accent: 'Accent',
    Set1: 'Set1',
    Set2: 'Set2',
    Set3: 'Set3',
    Spectral: 'Spectral',
    RedYellowGreen: 'RdYlGn',
  };
  return options('colour scheme', schemes, 'Paired', { display: 'inline-radio' }, BASE_GROUP);
};

const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: '8px',
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: '16px',
  },
  root: {
    flexGrow: 1,
  },
  iconsPaper: {
    width: 110,
    height: 90,
    textAlign: 'center',
  },
  control: {
    padding: theme.spacing(2),
  },
  icon: { fontSize: 60, display: 'block', marginLeft: 'auto', marginRight: 'auto' },
}));

const TimeRangeControl = ({ minTs, currentTime, trailLength }) => {
  const timeZone = 'Europe/Helsinki';
  const tsMillisEnd = (minTs + currentTime) * 1000;
  const tsMillisStart = tsMillisEnd - trailLength * 60 * 1000;
  const dStart = moment.tz(tsMillisStart, timeZone);
  const dEnd = moment.tz(tsMillisEnd, timeZone);
  const duration = moment.duration(dEnd.diff(dStart));

  return (
    <div>
      <Typography variant='body2'>{`${duration.humanize()}`}</Typography>
      <Typography variant='body2'>{`${dStart.format('LLLL')}`}</Typography>
      <Typography variant='body2'>{`${dEnd.format('LLLL')}`}</Typography>
    </div>
  );
};

const createDefaultControls = ({ currentTime, timeChangeCallback, minTs, maxTs, trailLength }) => {
  return [
    {
      label: 'Time',
      control: (
        <Slider value={currentTime} onChange={timeChangeCallback} defaultValue={0} step={30} min={minTs} max={maxTs} />
      ),
    },
    {
      label: 'Time Range',
      control: <TimeRangeControl minTs={minTs} currentTime={currentTime} trailLength={trailLength} />,
    },
  ];
};

//this short list to be fomalized in a config
const ICONS = [
  { rotatable: true, rotation: 0, key: 'none', label: 'none' },
  { rotatable: true, rotation: 0, key: 'navigation', label: 'navigation' },
  { rotatable: false, rotation: 0, key: 'fiber_manual_record', label: 'circle' },
  { rotatable: false, rotation: 0, key: 'fiber_manual_record_filled', label: 'circle_filled' },
  { rotatable: false, rotation: 0, key: 'diamond', label: 'diamond' },
  { rotatable: false, rotation: 0, key: 'diamond_filled', label: 'diamond_filled' },
  { rotatable: false, rotation: 0, key: 'square', label: 'square' },
  { rotatable: false, rotation: 0, key: 'square_filled', label: 'square_filled' },
  { rotatable: true, rotation: 0, key: 'expand_less', label: 'small_arrow' },
  { rotatable: true, rotation: 0, key: 'change_history', label: 'triangle' },
  { rotatable: true, rotation: 0, key: 'change_history_filled', label: 'triangle_filled' },
  { rotatable: false, rotation: 0, key: 'directions_car', label: 'car' },
  { rotatable: false, rotation: 0, key: 'adjust', label: 'circle_dot' },
  { rotatable: false, rotation: 0, key: 'my_location', label: 'my_location' },
  { rotatable: false, rotation: 0, key: 'place', label: 'place' },
  { rotatable: false, rotation: 0, key: 'location_searching', label: 'location_searching' },
  { rotatable: false, rotation: 0, key: 'account_box', label: 'account_box' },
  { rotatable: false, rotation: 0, key: 'account_circle', label: 'account_circle' },
  { rotatable: true, rotation: 0, key: 'arrow_drop_up', label: 'arrow_head' },
  { rotatable: false, rotation: 0, key: 'speaker_phone', label: 'speaker_phone' },
  { rotatable: false, rotation: 0, key: 'add_box', label: 'add_box' },
  { rotatable: false, rotation: 0, key: 'add_circle', label: 'add_circle' },
  { rotatable: false, rotation: 0, key: 'add_circle_outline', label: 'add_circle_outline' },
  { rotatable: false, rotation: 0, key: 'phone_iphone', label: 'phone_iphone' },
  { rotatable: false, rotation: 0, key: 'tap_and_play', label: 'phone_signal' },
  { rotatable: true, rotation: -90, key: 'keyboard_backspace', label: 'arrow' },
  { rotatable: true, rotation: 0, key: 'phonelink_ring', label: 'phonelink_ring' },
  { rotatable: true, rotation: 0, key: 'commute', label: 'commute' },
  { rotatable: true, rotation: 0, key: 'timer', label: 'timer' },
  { rotatable: true, rotation: 0, key: 'report', label: 'stop' },
  { rotatable: true, rotation: 0, key: 'payment', label: 'payment' },
  { rotatable: true, rotation: 0, key: 'monetization_on', label: 'money' },
  { rotatable: true, rotation: 0, key: 'local_atm', label: 'local_atm' },
  { rotatable: true, rotation: 0, key: 'receipt_long', label: 'receipt_long' },
];

const DEFAULT_COLOUR_FILL = [255, 65, 0];
const DEFAULT_COLOUR_LINE = chroma([...DEFAULT_COLOUR_FILL])
  .darken(0.8)
  .rgb(); //chroma darken mods the passed array (adds alpha value element)- booooo, shame!

const pathToEventsTransformer = (data) => {
  const newData = _reduce(
    data,
    (result, d) => {
      const obj = _pick(d.properties, [
        'oday',
        'operator_id',
        'vehicle_number',
        'route_id',
        'direction_id',
        'transport_mode',
        'start_time',
        'colour',
        'lineColour',
      ]);

      d.coordinates.forEach((c, i, a) => {
        result.push({
          properties: { ...obj, hdg: d.properties.hdg[i], tsi: d.properties.tsi[i] },
          index: i,
          coordinates: c,
          coordinatesTarget: a[i + 1] ? a[i + 1] : a[i], //only provide a target when available
        });
      });
      return result;
    },
    []
  );
  return newData.reverse();
};

const eventsToPathTransformer = (data) => {
  const groupedByVeh = d3Array.group(data, (d) => {
    return _values(
      _pick(d.properties, [
        'start_time',
        'oday',
        'operator_id',
        'vehicle_number',
        'route_id',
        'direction_id',
        'transport_mode',
      ])
    ).join('');
  });
  let paths = [];
  groupedByVeh.forEach((events, k) => {
    const coordinates = _sortBy(events, (e) => e.properties.tsi).map((e) => e.coordinates);
    const { vehicle_number, colour, lineColour } = events[0].properties;
    paths.push({ properties: { vehicle_number, colour, lineColour }, coordinates });
  });

  return paths;
};

export const Icons = () => {
  const customIcons = ['diamond', 'diamond_filled', 'square', 'square_filled'];
  const icons = ICONS.filter((i) => !customIcons.includes(i.key));
  const classes = useStyles();
  // const preventDefault = (event) => event.preventDefault();
  return (
    <Grid container className={classes.root} spacing={2}>
      <Grid item xs={12}>
        <Grid container justify='center' spacing={2}>
          {icons.map((icon) => (
            <Grid key={icon.key} item>
              <Paper className={classes.iconsPaper} justifyContent='center'>
                <Icon className={classes.icon} color='primary'>
                  {icon.key}
                </Icon>
                <Typography style={{ textAlign: 'center' }} display='block' variant='caption'>
                  {icon.label}
                </Typography>
              </Paper>
            </Grid>
          ))}
        </Grid>
        <Grid item xs={12}>
          <Typography style={{ textAlign: 'center', marginTop:"20px" }} display='block' variant='h6' gutterBottom>
            <Link href='https://material.io/resources/icons/?style=baseline' target="_blank" rel="noreferrer">
              To view the full set of available icons visit: https://material.io/resources/icons/?style=baseline
            </Link>
          </Typography>
        </Grid>
      </Grid>
    </Grid>
  );
};

export const PathIconOptions = () => {
  const iconHead = select('head icon', ICONS, ICONS[1], TYPE1_GROUP);
  const iconTail = select('tail icon', ICONS, ICONS[2], TYPE1_GROUP);
  const widthLine = number('path width', 3, { range: true, min: 0, max: 10, step: 1 }, TYPE1_GROUP);

  const dashed = boolean('use dashed path', true, TYPE1_GROUP);
  const dashOptions = { range: true, min: 0, max: 15, step: 1 };
  const sizeGap = number('dashed gap size', 2, dashOptions, TYPE1_GROUP);
  const sizeDash = number('dashed dash size', 5, dashOptions, TYPE1_GROUP);

  const baseMap = createBaseLayerKnob();

  const layers = [
    new IconLayer({
      id: 'icon-layer-head',
      data: SINGLE_TRIP_URL,
      dataTransform: pathToEventsTransformer,
      getPosition: (d) => d.coordinates,
      filterRange: [-1, 0],
      extensions: [new DataFilterExtension({ filterSize: 1 })],
      getFilterValue: (d, info) => info.index,
      getColor: DEFAULT_COLOUR_FILL,
      sizeUnits: 'meters',
      getSize: 200,
      sizeScale: 10,
      sizeMaxPixels: 28,
      sizeMinPixels: 4,
      iconAtlas: 'images/icons/icon-atlas.png',
      iconMapping: ICON_MAPPING,
      getIcon: (d, info) => (iconHead.key !== 'none' ? iconHead.key : ''),
      opacity: 1.0,
      getAngle: (d) => (iconHead.rotatable ? -d['hdg'] + iconHead.rotation : 0),
      billboard: true,
      updateTriggers: {
        getIcon: [iconHead],
        getAngle: [iconHead],
      },
    }),
    new IconLayer({
      id: 'icon-layer-tail',
      data: SINGLE_TRIP_URL,
      dataTransform: pathToEventsTransformer,
      getPosition: (d) => d.coordinates,
      filterRange: [2, Number.MAX_SAFE_INTEGER],
      extensions: [new DataFilterExtension({ filterSize: 1 })],
      getFilterValue: (d, info) => info.index,
      getColor: DEFAULT_COLOUR_FILL,
      sizeUnits: 'meters',
      getSize: 5,
      sizeScale: 10,
      sizeMaxPixels: 28,
      sizeMinPixels: 4,
      iconAtlas: 'images/icons/icon-atlas.png',
      iconMapping: ICON_MAPPING,
      getIcon: (d, info) => (iconTail.key !== 'none' ? iconTail.key : ''),
      opacity: 1.0,
      getAngle: (d) => (iconTail.rotatable ? -d['hdg'] + iconTail.rotation : 0),
      billboard: true,
      updateTriggers: {
        getIcon: [iconTail],
        getAngle: [iconTail],
      },
    }),
    new PathLayer({
      id: 'path-layer-demo',
      data: SINGLE_TRIP_URL,
      getPath: (d) => d.coordinates,
      getColor: DEFAULT_COLOUR_LINE,
      getWidth: widthLine,
      widthMinPixels: 2,
      widthMaxPixels: widthLine,
      opacity: 1.0,
      getDashArray: [sizeDash, sizeGap],
      dashJustified: true,
      extensions: [new PathStyleExtension({ dash: dashed })],
      updateTriggers: {
        getDashArray: [dashed, sizeDash, sizeGap],
        getWidth: widthLine,
      },
    }),
  ];

  return (
    <div>
      <DemoGrid
        controls={[]}
        layers={layers}
        view={{ ...VIEWS.helsinki, zoom: 12, longitude: 24.80303, latitude: 60.16837 }}
        baseMap={baseMap}
      />
    </div>
    /*  */
  );
};

export const PathPlotOptions = () => {
  const [data, setData] = useState([]);
  const [dataType2, setDataType2] = useState([]);
  const [paths, setPaths] = useState([]);
  const [points, setPoints] = useState([]);
  const [pointsType2, setPointsType2] = useState([]);
  const [timeExtent, setTimeExtent] = useState({ minTs: 0, maxTs: 0 });
  const [currentTime, setCurrentTime] = useState(0);

  const listOfPeople = { Alice: '00456', Bob: '00452', Charlie: '00462', Eve: '00951' };
  const vehicleFilter = options(
    'vehicles',
    listOfPeople,
    ['00456', '00452', '00462', '00951'],
    { display: 'inline-check' },
    FILTER_GROUP
  );
  const trailLength = createTrailKnob();

  const isFilled = boolean('point fill', true, TYPE1_GROUP);
  const plotOpacity = number('opacity', 0.5, { range: true, min: 0, max: 1, step: 0.1 }, TYPE1_GROUP);
  const isStroked = boolean('outline', true, TYPE1_GROUP);
  const widthOutine = number('outline width', 3, { range: true, min: 1, max: 5, step: 1 }, TYPE1_GROUP);

  const widthLine = number('path width', 3, { range: true, min: 0, max: 10, step: 1 }, PATH_GROUP);
  const dashed = boolean('use dashed path', true, PATH_GROUP);
  const dashOptions = { range: true, min: 0, max: 15, step: 1 };
  const sizeGap = number('dashed gap size', 2, dashOptions, PATH_GROUP);
  const sizeDash = number('dashed dash size', 5, dashOptions, PATH_GROUP);

  const showTypeTwo = boolean('show type 2 data', false, TYPE2_GROUP);
  const maxRadius = number('max radius (m)', 100, { range: true, min: 20, max: 1500, step: 10 }, TYPE2_GROUP);
  const frequency = number('reporting period', 10, { range: true, min: 1, max: 130, step: 1 }, TYPE2_GROUP);
  const colourScheme = createColourKnob();
  const baseMap = createBaseLayerKnob();

  useEffect(() => {
    jsonFetch(TRIPS_5DAYS_URL).then((data) => {
      const dataColoured = colourByVehicleIdTransformer(data);
      const tsiMinMax = d3Array.extent(data, (d) => d.properties.tsi);
      setTimeExtent({ minTs: tsiMinMax[0], maxTs: tsiMinMax[1] });
      setData(dataColoured);
      setDataType2(sparsifyEventsTransformer({ events: dataColoured, maxRadius, frequency }));
      //set current time to middle of the range
      setCurrentTime(tsiMinMax[0] + (tsiMinMax[1] - tsiMinMax[0]) * 0.5);
    });
  }, [colourScheme]);

  useEffect(() => {
    //filter for vehicle number and timestamp
    const vehicleAndTimeRangeFilter = (d) => {
      return (
        vehicleFilter.includes(d.properties.vehicle_number) &&
        d.properties.tsi < currentTime &&
        d.properties.tsi > currentTime - trailLength * 60
      );
    };

    const filteredPoints = data.filter(vehicleAndTimeRangeFilter);
    const filteredPointsType2 = dataType2.filter(vehicleAndTimeRangeFilter);

    setPoints(filteredPoints);
    setPointsType2(filteredPointsType2);
    setPaths(eventsToPathTransformer(filteredPoints));
  }, [data, dataType2, vehicleFilter, currentTime, trailLength]);

  useEffect(() => {
    //reassign the type 2 data if the specs change
    setDataType2(sparsifyEventsTransformer({ events: data, maxRadius, frequency }));
  }, [maxRadius, frequency]);

  const handleTimeChange = useCallback((ev, ts) => {
    setCurrentTime(ts);
  }, []);

  const colourByVehicleIdTransformer = (data) => {
    let vehicles = [...new Set(data.map((p) => p.properties.vehicle_number))]; //unique set of vehicles
    const colourScaleFill = chroma.scale(colourScheme).colors(vehicles.length, 'rgb');
    const colourScaleLine = colourScaleFill.map((c) => {
      return chroma([...c])
        .darken(0.7)
        .rgb();
    });

    const dataWithColour = data.map((d, i) => {
      const vehicleIndex = vehicles.indexOf(d.properties.vehicle_number);
      d.properties.colour = colourScaleFill[vehicleIndex];
      d.properties.lineColour = colourScaleLine[vehicleIndex];
      return d;
    });
    return dataWithColour;
  };

  const sparsifyEventsTransformer = ({ events, maxRadius, frequency }) => {
    const accuracy_range = _range(20, maxRadius, maxRadius / 10);
    const overlap_percentage = 0.8;

    return events
      .filter((d, i) => i % frequency === 0)
      .map((d) => {
        const bearing = _random(-180, 180);
        const acc = accuracy_range[_random(0, accuracy_range.length - 1)];
        const newCoord = getCoord(destination(d.coordinates, acc * overlap_percentage, bearing, { units: 'meters' }));
        const ev = { ...d, coordinates: newCoord, properties: { ...d.properties, radius: acc } };
        return ev;
      });
  };

  const plotOptions = {
    getPosition: (d) => d.coordinates,
    getFillColor: (d) => d.properties.colour,
    getLineColor: (d) => d.properties.lineColour,
    getLineWidth: widthOutine,
    radiusMinPixels: 2,
    getRadius: (d) => d.properties.radius,
    filled: isFilled,
    opacity: plotOpacity,
    stroked: isStroked,
    updateTriggers: {
      opacity: [plotOpacity],
      stroked: [isStroked],
      getLineWidth: [widthOutine],
      filled: [isFilled],
    },
  };

  const layers = [
    new ScatterplotLayer({
      ...plotOptions,
      id: 'plot-layer',
      data: points,
      getRadius: 10,
    }),
    new ScatterplotLayer({
      ...plotOptions,
      id: 'plot-two-layer',
      data: pointsType2,
      visible: showTypeTwo,
    }),
    new PathLayer({
      id: 'path-layer-demo',
      data: paths,
      getPath: (d) => d.coordinates,
      getColor: (d) => d.properties.colour,
      getWidth: widthLine,
      widthMinPixels: 9,
      widthMaxPixels: widthLine,
      opacity: 1.0,
      getDashArray: [sizeDash, sizeGap],
      dashJustified: true,
      extensions: [new PathStyleExtension({ dash: dashed })],
      updateTriggers: {
        getDashArray: [dashed, sizeDash, sizeGap],
        getWidth: widthLine,
      },
    }),
  ];

  const controls = createDefaultControls({
    currentTime,
    timeChangeCallback: handleTimeChange,
    ...timeExtent,
    trailLength,
  });

  return (
    <div>
      <DemoGrid
        controls={controls}
        layers={layers}
        view={{ ...VIEWS.helsinki, zoom: 10, longitude: 24.7553043, latitude: 60.24340547 }}
        baseMap={baseMap}
      />
    </div>
    /*  */
  );
};
