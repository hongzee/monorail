import { InMemoryCache, ReactiveVar, makeVar } from "@apollo/client";

export const BASE_MAPS = {
  TOPO:
    "https://api.maptiler.com/maps/topo/style.json?key=R01BPnJMraEGqoIU2zM6",
  DARKMATTER:
    "https://api.maptiler.com/maps/darkmatter/style.json?key=R01BPnJMraEGqoIU2zM6",
  STREETS_3D:
    "https://api.maptiler.com/maps/streets/style.json?key=R01BPnJMraEGqoIU2zM6",
  BASIC:
    "https://api.maptiler.com/maps/basic/style.json?key=R01BPnJMraEGqoIU2zM6",
  BRIGHT:
    "https://api.maptiler.com/maps/bright/style.json?key=R01BPnJMraEGqoIU2zM6",
  POSITRON:
    "https://api.maptiler.com/maps/positron/style.json?key=R01BPnJMraEGqoIU2zM6",
  TONER:
    "https://api.maptiler.com/maps/toner/style.json?key=R01BPnJMraEGqoIU2zM6",
  SATELLITE_LOW_RES:
    "https://api.maptiler.com/maps/hybrid/style.json?key=R01BPnJMraEGqoIU2zM6",
};

// Create the initial value
export const MAP_DEFAULT_VIEW = {
  longitude: 24.9413548,
  latitude: 60.1717794,
  zoom: 13,
  pitch: 0,
  bearing: 0,
};

const OTTAWA_VIEW = {
  ...MAP_DEFAULT_VIEW,
  longitude: -75.690308,
  latitude: 45.421106,
  zoom: 13,
};
export const MAP_DEFAULT_BASEMAP = BASE_MAPS.DARKMATTER;

// Create the defaultMapView var and initialize it with the initial value
export const mapView: ReactiveVar<any> = makeVar<any>(MAP_DEFAULT_VIEW);
export const baseMap: ReactiveVar<string> = makeVar<string>(
  MAP_DEFAULT_BASEMAP
);

export const cache = new InMemoryCache({
  typePolicies: {
    Query: {
      fields: {
        baseMap: {
          read() {
            return baseMap();
          },
        },
        mapView: {
          read() {
            return mapView();
          },
        },
      },
    },
  },
});
