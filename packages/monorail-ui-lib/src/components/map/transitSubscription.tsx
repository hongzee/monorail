import {
  ApolloClient,
  HttpLink,
  InMemoryCache,
  useSubscription,
  gql,
  split,
} from "@apollo/client";
import { getMainDefinition } from "@apollo/client/utilities";
import { WebSocketLink } from "@apollo/client/link/ws";

import React, { useState, useEffect } from "react";
import _get from "lodash/get";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import { MAP_DEFAULT_VIEW } from "./cache";

import { DeckMap } from "./index";
import { ScatterplotLayer, PathLayer, IconLayer } from "@deck.gl/layers";
import { ICON_MAPPING } from "./icon-mapping";

const VIEW = {
  ...MAP_DEFAULT_VIEW,
  longitude: 24.9413548,
  latitude: 60.1717794,
  zoom: 10,
};

// Create an http link:
const httpLink = new HttpLink({
  uri: "https://gql.monorail.infratrode.com/graphql",
});

// Create a WebSocket link:
const wsLink = new WebSocketLink({
  uri: `wss://gql.monorail.infratrode.com/graphql`,
  options: {
    reconnect: true,
  },
});

const link = split(
  // split based on operation type
  ({ query }) => {
    const definition = getMainDefinition(query);
    return (
      definition.kind === "OperationDefinition" &&
      definition.operation === "subscription"
    );
  },
  wsLink,
  httpLink
);

const apolloClient = new ApolloClient({
  cache: new InMemoryCache(),
  link,
});

//This takes the digi-transit MQTT topic as a param
//example = /hfp/v2/journey/ongoing/vp/tram/+/+/+/+/+/+/+/3/#
const TRANSIT_SUBSCRIPTION_BY_MQTT_TOPIC = gql`
  subscription($topic: String!) {
    subscribeToJourney(topic: $topic) {
      oper
      veh
      tsi
      tst
      spd
      hdg
      lat
      long
      acc
    }
  }
`;

// subscribeToKafkaJourney(filter: {operatorId: 90}) {
// subscribeToKafkaJourney(filter: {vehicleNumber: 1049}) {
// subscribeToKafkaJourney(filter: {routeId: 2543}) {
const TRANSIT_SUBSCRIPTION_BY_KAFKA_WITH_FILTER = gql`
  subscription($filter: JourneyFilter!) {
    subscribeToKafkaJourney(filter: $filter) {
      oper
      veh
      tsi
      tst
      spd
      hdg
      lat
      long
      acc
    }
  }
`;

const CURRENT_SUBSCRIPTION = TRANSIT_SUBSCRIPTION_BY_KAFKA_WITH_FILTER;

//<prefix>/<version>/<journey_type>/<temporal_type>/<event_type>/<transport_mode>/<operator_id>/<vehicle_number>/<route_id>/<direction_id>/<headsign>/<start_time>/<next_stop>/<geohash_level>/<geohash>/#

export const TOPICS = {
  all_transit: "/hfp/v2/journey/ongoing/vp/#",
  all_trams: "/hfp/v2/journey/ongoing/vp/tram/+/+/+/+/+/+/+/3/#",
  all_buses: "/hfp/v2/journey/ongoing/vp/bus/0030/+/+/+/+/+/+/3/#",
  all_trains: "/hfp/v2/journey/ongoing/vp/train/+/+/+/+/+/+/+/3/#",
  all_metro: "/hfp/v2/journey/ongoing/vp/metro/+/+/+/+/+/+/+/3/#",
  single_tram: "/hfp/v2/journey/ongoing/vp/tram/0040/00077/+/+/+/+/+/3/#",
  Kauklahti: "/hfp/v2/journey/ongoing/vp/+/+/+/+/+/Kauklahti/+/+/3/#",
  Kirkkonummi: "/hfp/v2/journey/ongoing/vp/train/+/+/+/+/Kirkkonummi/+/+/3/#",
  Matinkylä: "/hfp/v2/journey/ongoing/vp/+/+/+/+/+/Matinkylä/+/+/4/#",
  Vuosaari: "/hfp/v2/journey/ongoing/vp/+/+/+/+/+/Vuosaari/+/+/4/#",
  Soukanniemi: "/hfp/v2/journey/ongoing/vp/+/+/+/+/+/Soukanniemi/+/+/4/#",
  test: "/hfp/v2/journey/ongoing/vp/bus/+/+/+/+/Kauklahti/+/+/3/#",
};

export const PRESET_FILTERS = {
  route_1003: { routeId: ["1003"] },
  route_1043: { routeId: ["1043"] },
  all_metros: { operatorId: 50 },
  all_trains: { operatorId: 90 },
  all_trams: { operatorId: 40 },
  two_vehicles: { operatorId: 30, vehicleNumber: [53, 52] },
  five_vehicles: { operatorId: 40, vehicleNumber: [472, 464, 451, 454, 455] },
  company_22: { operatorId: 22 },
  company_18: { operatorId: 18 },
};

const COLOURS = {
  orange: [255, 65, 0],
};

interface JourneyEvent {
  oper: number;
  veh: number;
  tsi: number;
  spd: number;
  hdg: number;
  lat: number;
  long: number;
  acc: number;
}

interface EventProps {
  event: JourneyEvent;
}

const isValidCoord = (lat, lon) => {
  const latF = parseFloat(lat);
  const lonF = parseFloat(lon);
  if (
    !isNaN(lonF) &&
    !isNaN(latF) &&
    Math.abs(latF) + Math.abs(lonF) > 0 &&
    latF <= 90 &&
    latF >= -90 &&
    lonF <= 180 &&
    lonF >= -180
  )
    return true;
  else return false;
};

export interface LiveTransitProps {
  filter?: any;
  baseMap?: any;
}

const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
});

export const TransitSubscriptionDataScroller = (props: LiveTransitProps) => {
  const { filter, baseMap } = props;
  const classes = useStyles();

  const [events, setEvents] = useState([]);

  useEffect(() => {
    if (events && events.length > 0) {
      //reset the map of vehicles when topic changes
      setEvents([]);
    }
  }, [filter]);

  // useSubscription hook, can update a state var, or use client to update cache
  const { data, loading, error } = useSubscription(CURRENT_SUBSCRIPTION, {
    variables: { filter: filter },
    onSubscriptionData: (data: any) => {
      const event = _get(
        data,
        "subscriptionData.data.subscribeToKafkaJourney",
        false
      );

      if (event) {
        setEvents([].concat(event).concat(events.slice(0, 10)));
      } else {
        console.log(data);
      }
    },
    client: apolloClient,
  });

  if (loading) {
    return <div>loading...</div>;
  }
  if (error) {
    console.log("error:", error);
    return <div>...error</div>;
  }

  return (
    <TableContainer component={Paper}>
      <Table className={classes.table} size="small" aria-label="a dense table">
        <TableHead>
          <TableRow>
            <TableCell>Operator</TableCell>
            <TableCell align="right">Vehicle</TableCell>
            <TableCell align="right">Timestamp</TableCell>
            <TableCell align="right">Speed (m/s)</TableCell>
            <TableCell align="right">Heading(⁰)</TableCell>
            <TableCell align="right">Lat/Lon</TableCell>
            <TableCell align="right">Acceleration (m/s^2)</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {events.map((r, i) => (
            <TableRow key={`${i.toString()}_${r.tsi}`}>
              <TableCell component="th" scope="row">
                {r.oper}
              </TableCell>
              <TableCell align="right">{r.veh}</TableCell>
              <TableCell align="right">{r.tst}</TableCell>
              <TableCell align="right">{r.spd}</TableCell>
              <TableCell align="right">{r.hdg}</TableCell>
              <TableCell align="right">
                {r.lat}/{r.long}
              </TableCell>
              <TableCell align="right">{r.acc}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

export const LiveTransitMap = (props: LiveTransitProps) => {
  const { filter, baseMap } = props;

  const [events, setEvents] = useState(new Map());

  useEffect(() => {
    if (events && events.size > 0) {
      //reset the map of vehicles when topic changes
      setEvents(new Map());
    }
  }, [filter]);

  const updateEvents = (event) => {
    if (!isValidCoord(event.lat, event.long)) {
      return;
    }
    const key = `${event.oper}_${event.veh}`;
    let data = new Map(events);
    let obj = data.get(key) || { coords: [], tsi: [], hdg: [] };

    obj.coords.unshift([event.long, event.lat]);
    obj.hdg.unshift(event.hdg);
    obj.tsi.unshift(event.tsi);

    if (obj.coords.length > 6) {
      obj.coords.pop();
      obj.tsi.pop();
      obj.hdg.pop();
    }
    data.set(key, obj);
    setEvents(data);
  };

  // useSubscription hook, can update a state var, or use client to update cache
  const { data, loading, error } = useSubscription(CURRENT_SUBSCRIPTION, {
    variables: { filter },
    onSubscriptionData: (data: any) => {
      const event = _get(
        data,
        "subscriptionData.data.subscribeToKafkaJourney",
        false
      );

      if (event) {
        updateEvents(event);
      } else {
        console.log(data);
      }
    },
    client: apolloClient,
  });

  if (loading) {
    return <div>loading...</div>;
  }
  if (error) {
    console.log(error);
    return <div>error....</div>;
  }

  if (!events || events.size == 0) {
    return <div>Waiting for data ...</div>;
  }

  const mapToArrayTransformer = (data) => {
    let newData = [];
    data.forEach((v, k) => {
      v.coords.forEach((c, i) => {
        if (i > 0) {
          newData.push({ coords: c, tsi: v.tsi[i], hdg: v.hdg[i] });
        }
      });
    });
    return newData;
  };

  const scatterplot_options = {
    data: events,
    getFillColor: COLOURS.orange,
    getLineColor: COLOURS.orange,
    getRadius: 3,
    radiusMinPixels: 3,
    opacity: 1.0,
    filled: true,
    stroked: false,
  };

  const icon_options = {
    getColor: COLOURS.orange,
    getSize: 20,
    sizeUnits: "meters",
    sizeMinPixels: 20,
    iconAtlas: "images/icons/icon-atlas.png",
    iconMapping: ICON_MAPPING,
    getIcon: (d) => "expand_less",
    opacity: 0.8,
  };

  const pastPositionScatterplot = new ScatterplotLayer({
    ...scatterplot_options,
    id: "past-scatterplot",
    data: mapToArrayTransformer(events),
    getPosition: (d) => d.coords,
  });

  const SVG = encodeURIComponent(
    '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 24 24"><defs><filter id="dropshadow" width="200%" height="200%"><feDropShadow dx="1.5" dy="1.5" stdDeviation="0.8" /></filter></defs><path d="M12,2L4.5,20.29L5.21,21L12,18L18.79,21L19.5,20.29L12,2Z" fill="#ff4100" filter="url(#dropshadow)"/></svg>'
  );

  const currentPositionIcon = new IconLayer({
    id: "current-icon",
    data: events,
    getPosition: (d) => d[1]["coords"][0],
    getSize: 30,
    sizeUnits: "meters",
    sizeMinPixels: 20,
    getAngle: (d) => -d[1]["hdg"][0],
    getIcon: (d) => ({
      id: (d) => `${d.veh}_${d.tsi}`,
      url: `data:image/svg+xml;charset=utf-8,${SVG}`,
      width: 64,
      height: 64,
    }),
    opacity: 0.8,
    billboard: false,
  });

  const pastPositionIcon = new IconLayer({
    ...icon_options,
    id: "past-postion-icon",
    data: mapToArrayTransformer(events),
    getPosition: (d) => d.coords,
    getAngle: (d) => -d["hdg"],
    getIcon: (d) => "expand_less",
  });

  const pathLayer = new PathLayer({
    id: "path-layer",
    data: events,
    getPath: (d) => d[1].coords,
    getColor: [255, 65, 0],
    getWidth: 3,
    opacity: 0.5,
    widthUnits: "meters",
    billboard: true,
  });

  const base = baseMap
    ? baseMap
    : "https://api.maptiler.com/maps/hybrid/style.json?key=R01BPnJMraEGqoIU2zM6";

  return (
    <div>
      <DeckMap
        height={600}
        layers={[pathLayer, currentPositionIcon, pastPositionScatterplot]}
        initialView={VIEW}
        baseMap={base}
      />
    </div>
  );
};
