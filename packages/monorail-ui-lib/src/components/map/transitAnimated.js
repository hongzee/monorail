/* global window */
import { AmbientLight, LightingEffect, PointLight } from "@deck.gl/core";
import { TripsLayer } from "@deck.gl/geo-layers";
import DeckGL from "@deck.gl/react";
import _max from "lodash/max";
import _min from "lodash/min";
import React, { useRef, useState } from "react";
import { StaticMap } from "react-map-gl";
import { MAP_DEFAULT_VIEW } from "./cache";

const BASEMAP =
  "https://api.maptiler.com/maps/darkmatter/style.json?key=R01BPnJMraEGqoIU2zM6";

// Source data CSV
const DATA_URL = {
  TRIPS: "https://infratrode.gitlab.io/public-data/transit/trips-200.json", // eslint-disable-line
};

const ambientLight = new AmbientLight({
  color: [255, 255, 255],
  intensity: 1.0,
});

const pointLight = new PointLight({
  color: [255, 255, 255],
  intensity: 2.0,
  position: [-74.05, 40.7, 8000],
});

const lightingEffect = new LightingEffect({ ambientLight, pointLight });

const material = {
  ambient: 0.1,
  diffuse: 0.6,
  shininess: 32,
  specularColor: [60, 64, 70],
};

const DEFAULT_THEME = {
  buildingColor: [74, 80, 87],
  trailColor0: [253, 128, 93],
  trailColor1: [23, 184, 190],
  material,
  effects: [lightingEffect],
};

const INITIAL_VIEW_STATE = {
  ...MAP_DEFAULT_VIEW,
  longitude: 24.9413548,
  latitude: 60.1717784,
  zoom: 13,
  pitch: 55,
};

const TransitAnimated = () => {
  const [count, setCount] = useState(0);

  const requestRef = useRef();
  const previousTimeRef = useRef();

  const animate = (time) => {
    if (previousTimeRef.current != undefined) {
      const deltaTime = time - previousTimeRef.current;

      // Pass on a function to the setter of the state
      // to make sure we always have the latest state
      setCount((prevCount) => (prevCount + deltaTime * 0.1) % 9000);
    }

    previousTimeRef.current = time;
    requestRef.current = window.requestAnimationFrame(animate);
  };

  React.useEffect(() => {
    requestRef.current = window.requestAnimationFrame(animate);
    return () => window.cancelAnimationFrame(requestRef.current);
  }, []); // Make sure the effect runs only once

  const _renderLayers = () => {
    const trips = DATA_URL.TRIPS,
      trailLength = 180;

    const transformer = (data, prevData) => {
      const minRs = _min(
        data.map((r) => {
          return _min(r.properties.tsi);
        })
      );
      const maxRs = _max(
        data.map((r) => {
          return _max(r.properties.tsi);
        })
      );
      console.log("[minRs, maxRs]:", [minRs, maxRs, maxRs - minRs]);
      return data;
    };

    return [
      new TripsLayer({
        id: "trips",
        data: trips,
        dataTransform: transformer,
        getPath: (d) => d.coordinates,
        getTimestamps: (d) => d.properties.tsi.map((t) => t - 1584654245),
        getColor: (d) =>
          d.properties.direction_id === "1" ? [23, 184, 190] : [230, 74, 25],
        opacity: 0.3,
        widthMinPixels: 3,
        rounded: true,
        trailLength,
        currentTime: count,
        shadowEnabled: false,
      }),
    ];
  };

  return (
    <DeckGL
      layers={_renderLayers()}
      effects={DEFAULT_THEME.effects}
      initialViewState={INITIAL_VIEW_STATE}
      controller={true}
    >
      <StaticMap reuseMaps mapStyle={BASEMAP} preventStyleDiffing={true} />
    </DeckGL>
  );
};

export default TransitAnimated;
