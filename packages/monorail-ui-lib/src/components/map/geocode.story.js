import { GeoJsonLayer } from "@deck.gl/layers";
import { optionsKnob as options, withKnobs } from "@storybook/addon-knobs";
import chroma from "chroma-js";
import React from "react";
import DemoGrid from "./demoGrid";
import { MAP_DEFAULT_VIEW } from "./cache";
import "./stories.css";

const KNOB_GROUP = "controls";

const VIEWS = {
  ottawa: {
    ...MAP_DEFAULT_VIEW,
    longitude: -75.690308,
    latitude: 45.421106,
    zoom: 13,
  },
  ottawaNorth: {
    longitude: -75.7882,
    latitude: 45.4822,
    zoom: 11,
    pitch: 40,
    bearing: 180,
  },
  helsinki: {
    ...MAP_DEFAULT_VIEW,
    longitude: 24.9413548,
    latitude: 60.1717794,
    zoom: 10,
  },
};

export default {
  title: "Geocoder",
  decorators: [withKnobs],
};

const sampleFilesObj = [
  { id: 'cell_tower_sector', path: '0', lonlat: [-74.6738, 45.1209] },
  { id: 'cell_tower_mast', path: '1', lonlat: [-79.0203, 48.242] },
  { id: 'cell_towers', path: '2', lonlat: [174.6446, -36.9082] },
  { id: 'MAC_WIFI', path: '3', lonlat: [-83.7556, 42.285] },
  { id: 'IP', path: 'street_address/f', lonlat: [138.5999, -34.9281] },
  { id: 'payphone', path: '4', lonlat: [-122.4938, 52.9781] },
  { id: 'reverse_geocode_street_address', path: '5', lonlat: [-75.643555, 45.407787] },
  { id: 'telephone_area_code', path: '6', lonlat: [-76.7489, 45.062] },
  { id: 'towers', path: '7', lonlat: [-123.3663, 48.4277] },
  { id: 'single_house_exact', path: 'street_address/a', lonlat: [-75.681268, 45.221532] },
  { id: 'all_houses', path: 'street_address/b', lonlat: [-75.681268, 45.221532] },
  { id: 'street', path: 'street_address/c', lonlat: [-75.681268, 45.221532] },
  { id: 'postal_code', path: 'street_address/d', lonlat: [-75.681268, 45.221532] },
  { id: 'typo_correction', path: 'street_address/e', lonlat: [-75.681268, 45.221532] },
  { id: 'new_zealnd', path: 'street_address/z', lonlat: [174.904369, -41.199102] },
].map((d) => {
  return {
    ...d,
    url: `https://infratrode.gitlab.io/public-data/geocoder/${d.path}.json`,
    lon: d.lonlat[0],
    lat: d.lonlat[1],
  };
});

const BASE_MAPS = {
  TOPO:
    "https://api.maptiler.com/maps/topo/style.json?key=R01BPnJMraEGqoIU2zM6",
  DARKMATTER:
    "https://api.maptiler.com/maps/darkmatter/style.json?key=R01BPnJMraEGqoIU2zM6",
  STREETS_3D:
    "https://api.maptiler.com/maps/streets/style.json?key=R01BPnJMraEGqoIU2zM6",
  BASIC:
    "https://api.maptiler.com/maps/basic/style.json?key=R01BPnJMraEGqoIU2zM6",
  BRIGHT:
    "https://api.maptiler.com/maps/bright/style.json?key=R01BPnJMraEGqoIU2zM6",
  POSITRON:
    "https://api.maptiler.com/maps/positron/style.json?key=R01BPnJMraEGqoIU2zM6",
  TONER:
    "https://api.maptiler.com/maps/toner/style.json?key=R01BPnJMraEGqoIU2zM6",
  SATELLITE_LOW_RES:
    "https://api.maptiler.com/maps/hybrid/style.json?key=R01BPnJMraEGqoIU2zM6",
};

const createBaseLayerKnob = () => {
  const defaultValue = BASE_MAPS.STREETS_3D;
  return options(
    "base map",
    BASE_MAPS,
    defaultValue,
    { display: "inline-radio" },
    KNOB_GROUP
  );
};

const DEFAULT_COLOUR_FILL = [255, 65, 0];
const DEFAULT_COLOUR_LINE = chroma([...DEFAULT_COLOUR_FILL])
  .darken(0.8)
  .rgb(); //chroma darken mods the passed array (adds alpha value element)- booooo, shame!

export const CellTowerSector = () => {
  const baseMap = createBaseLayerKnob();
  const { url, lon, lat } = sampleFilesObj.find(({ id }) => id == 'cell_tower_sector');
  return <GeocoderSample url={url} lat={lat} lon={lon} zoom={8} baseMap={baseMap} />;
};
export const CellTowerMast = () => {
  const baseMap = createBaseLayerKnob();
  const { url, lon, lat } = sampleFilesObj.find(({ id }) => id == 'cell_tower_mast');
  return <GeocoderSample url={url} lat={lat} lon={lon} zoom={11} baseMap={baseMap} />;
};
export const CellTowers = () => {
  const baseMap = createBaseLayerKnob();
  const { url, lon, lat } = sampleFilesObj.find(({ id }) => id == 'cell_towers');
  return <GeocoderSample url={url} lat={lat} lon={lon} zoom={10} baseMap={baseMap} />;
};
export const MAC_WIFI = () => {
  const baseMap = createBaseLayerKnob();
  const { url, lon, lat } = sampleFilesObj.find(({ id }) => id == 'MAC_WIFI');
  return <GeocoderSample url={url} lat={lat} lon={lon} zoom={13} baseMap={baseMap} />;
};
export const IP = () => {
  const baseMap = createBaseLayerKnob();
  const { url, lon, lat } = sampleFilesObj.find(({ id }) => id == 'IP');
  return <GeocoderSample url={url} lat={lat} lon={lon} zoom={4} baseMap={baseMap} />;
};
export const Payphone = () => {
  const baseMap = createBaseLayerKnob();
  const { url, lon, lat } = sampleFilesObj.find(({ id }) => id == 'payphone');
  return <GeocoderSample url={url} lat={lat} lon={lon} zoom={15} baseMap={baseMap} />;
};
export const ReverseGeocoderAddress = () => {
  const baseMap = createBaseLayerKnob();
  const { url, lon, lat } = sampleFilesObj.find(({ id }) => id == 'reverse_geocode_street_address');
  return <GeocoderSample url={url} lat={lat} lon={lon} zoom={15} baseMap={baseMap} />;
};
export const TelephoneAreaCode = () => {
  const baseMap = createBaseLayerKnob();
  const { url, lon, lat } = sampleFilesObj.find(({ id }) => id == 'telephone_area_code');
  return <GeocoderSample url={url} lat={lat} lon={lon} zoom={6} baseMap={baseMap} />;
};
export const Towers = () => {
  const baseMap = createBaseLayerKnob();
  const { url, lon, lat } = sampleFilesObj.find(({ id }) => id == 'towers');
  return <GeocoderSample url={url} lat={lat} lon={lon} zoom={11} baseMap={baseMap} />;
};
export const SingleHouseExact = () => {
  const baseMap = createBaseLayerKnob();
  const { url, lon, lat } = sampleFilesObj.find(({ id }) => id == 'single_house_exact');
  return <GeocoderSample url={url} lat={lat} lon={lon} zoom={15} baseMap={baseMap} />;
};
export const AllHousesOnStreet = () => {
  const baseMap = createBaseLayerKnob();
  const { url, lon, lat } = sampleFilesObj.find(({ id }) => id == 'all_houses');
  return <GeocoderSample url={url} lat={lat} lon={lon} zoom={15} baseMap={baseMap} />;
};
export const StreetAddress = () => {
  const baseMap = createBaseLayerKnob();
  const { url, lon, lat } = sampleFilesObj.find(({ id }) => id == 'street');
  return <GeocoderSample url={url} lat={lat} lon={lon} zoom={15} baseMap={baseMap} />;
};

export const StreetAddressNZ = () => {
  const baseMap = createBaseLayerKnob();
  const { url, lon, lat } = sampleFilesObj.find(({ id }) => id == 'new_zealnd');
  return <GeocoderSample url={url} lat={lat} lon={lon} zoom={16} baseMap={baseMap} />;
};

export const PostalCode = () => {
  const baseMap = createBaseLayerKnob();
  const { url, lon, lat } = sampleFilesObj.find(({ id }) => id == 'postal_code');
  return <GeocoderSample url={url} lat={lat} lon={lon} zoom={15} baseMap={baseMap} />;
};

export const TypoCorrection = () => {
  const baseMap = createBaseLayerKnob();
  const { url, lon, lat } = sampleFilesObj.find(({ id }) => id == 'typo_correction');
  return <GeocoderSample url={url} lat={lat} lon={lon} zoom={15} baseMap={baseMap} />;
};

const GeocoderSample = (props) => {
  const { url, lat, lon, zoom, baseMap } = props;
  const layers = [
    new GeoJsonLayer({
      data: url,
      getRadius: 10,
      getFillColor: DEFAULT_COLOUR_FILL,
      getLineColor: DEFAULT_COLOUR_LINE,
      opacity: 0.3,
      stroked: true,
      filled: true,
      pickable: true,
      autoHighlight: true,
      id: "paulgeojsonlayer",
    }),
  ];

  return (
    <div>
      <DemoGrid
        controls={[]}
        layers={layers}
        view={{
          ...VIEWS.helsinki,
          zoom,
          longitude: lon,
          latitude: lat,
        }}
        baseMap={baseMap}
      />
    </div>
  );
};
