import {
  GridLayer,
  HeatmapLayer,
  HexagonLayer,
  ScreenGridLayer,
} from "@deck.gl/aggregation-layers";
import { DataFilterExtension } from "@deck.gl/extensions";
import { TripsLayer } from "@deck.gl/geo-layers";
import {
  ArcLayer,
  ColumnLayer,
  GeoJsonLayer,
  IconLayer,
  PathLayer,
  PolygonLayer,
  ScatterplotLayer,
  TextLayer,
} from "@deck.gl/layers";
import { ScenegraphLayer } from "@deck.gl/mesh-layers";
import { registerLoaders } from "@loaders.gl/core";
import { GLTFLoader } from "@loaders.gl/gltf";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";
import Slider from "@material-ui/core/Slider";
import { makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import {
  boolean,
  number,
  optionsKnob as options,
  radios,
  select,
  withKnobs,
} from "@storybook/addon-knobs";
import bearing from "@turf/bearing";
import { bearingToAzimuth } from "@turf/helpers";
import pointGrid from "@turf/point-grid";
import cheapRuler from "cheap-ruler";
import chroma from "chroma-js";
import { json as jsonFetch } from "d3-fetch";
import _max from "lodash/max";
import _min from "lodash/min";
import _pick from "lodash/pick";
import _reduce from "lodash/reduce";
import _uniq from "lodash/uniq";
import moment from "moment-timezone";
import React, { useCallback, useEffect, useReducer, useState } from "react";
import StaticMap from "react-map-gl";
import DemoGrid from "./demoGrid";
import FollowPathMap from "./followPathMap";
import { ICON_MAPPING } from "./icon-mapping";
import { SimpleMap as Map } from "./index";
import { MAP_DEFAULT_VIEW, BASE_MAPS } from "./cache";
import "./stories.css";
import TransitAnimated from "./transitAnimated";

registerLoaders([GLTFLoader]);

const VIEWS = {
  ottawa: {
    ...MAP_DEFAULT_VIEW,
    longitude: -75.690308,
    latitude: 45.421106,
    zoom: 13,
  },
  ottawaNorth: {
    longitude: -75.7882,
    latitude: 45.4822,
    zoom: 11,
    pitch: 40,
    bearing: 180,
  },
  helsinki: {
    ...MAP_DEFAULT_VIEW,
    longitude: 24.9413548,
    latitude: 60.1717794,
    zoom: 10,
  },
};

export default {
  title: "Map Tests",
  decorators: [withKnobs],
};

const KNOB_GROUP = "trips controls";

const createTrailKnob = () => {
  return number(
    "Time Range Visible (Minutes)",
    2,
    {
      range: true,
      min: 0.5,
      max: 30,
      step: 0.5,
    },
    KNOB_GROUP
  );
};

const createBaseLayerKnob = () => {
  const defaultValue = BASE_MAPS.STREETS_3D;
  return options(
    "Select base map",
    BASE_MAPS,
    defaultValue,
    { display: "inline-radio" },
    KNOB_GROUP
  );
};

const createTripsKnob = () => {
  const numTrips = ["1", "10", "20", "50", "100", "200", "42"].reduce(
    (acc, val, i, arr) => {
      acc[
        val
      ] = `https://infratrode.gitlab.io/public-data/transit/trips-${val}.json`;
      return acc;
    },
    {}
  );

  const defaultURL = Object.values(numTrips)[3];
  return options(
    "Number of Trips",
    numTrips,
    defaultURL,
    { display: "inline-radio" },
    KNOB_GROUP
  );
};

const TimeRangeControl = ({ minTs, currentTime, trailLength }) => {
  const timeZone = "Europe/Helsinki";
  const tsMillisEnd = (minTs + currentTime) * 1000;
  const tsMillisStart = tsMillisEnd - trailLength * 60 * 1000;
  const dStart = moment.tz(tsMillisStart, timeZone);
  const dEnd = moment.tz(tsMillisEnd, timeZone);
  const duration = moment.duration(dEnd.diff(dStart));

  return (
    <div>
      <Typography variant="body2">{`${duration.humanize()}`}</Typography>
      <Typography variant="body2">{`${dStart.format("LLLL")}`}</Typography>
      <Typography variant="body2">{`${dEnd.format("LLLL")}`}</Typography>
    </div>
  );
};

const createDefaultControls = ({
  currentTime,
  timeChangeCallback,
  minTs,
  maxTs,
  trailLength,
}) => {
  return [
    {
      label: "Time",
      control: (
        <Slider
          value={currentTime}
          onChange={timeChangeCallback}
          defaultValue={0}
          step={30}
          min={0}
          max={maxTs - minTs}
        />
      ),
    },
    {
      label: "Time Range",
      control: (
        <TimeRangeControl
          minTs={minTs}
          currentTime={currentTime}
          trailLength={trailLength}
        />
      ),
    },
  ];
};

// transformer to convert geoJSON trips (path layers) into individual events (scatterplot and icon layers)
const tripToEventsTransformer = (data) => {
  const newData = _reduce(
    data,
    (result, d) => {
      const obj = _pick(d.properties, [
        "start_time",
        "oday",
        "operator_id",
        "vehicle_number",
        "route_id",
        "direction_id",
        "transport_mode",
      ]);

      d.coordinates.forEach((c, i, a) => {
        result.push({
          ...obj,
          coordinates: c,
          acc: d.properties.acc[i],
          spd: d.properties.spd[i],
          hdg: d.properties.hdg[i],
          hdgCalc: calculateBearing(c, a[i + 1]),
          tsi: d.properties.tsi[i],
        });
      });
      return result;
    },
    []
  );
  return newData;
};

const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: "8px",
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: "16px",
  },
}));

export const mapStaticOnly = () => {
  const { latitude, longitude, zoom } = VIEWS.ottawa;
  return (
    <StaticMap
      width={400}
      height={400}
      latitude={latitude}
      longitude={longitude}
      zoom={zoom}
      mapStyle="https://api.maptiler.com/maps/darkmatter/style.json?key=R01BPnJMraEGqoIU2zM6"
    />
  );
};

export const SimpleMap = () => <Map />;

export const SimpleMap400x400 = () => <Map height={400} width={400} />;

export const Ottawa = () => <Map initialView={VIEWS.ottawa} />;

export const OttawaTiltRotate = () => <Map initialView={VIEWS.ottawaNorth} />;

export const Topo = () => {
  return <Map initialView={VIEWS.ottawaNorth} baseMap={BASE_MAPS.TOPO} />;
};

export const ChangeBase = () => {
  const baseMap = createBaseLayerKnob();
  return (
    <Map
      initialView={{ ...VIEWS.ottawa, zoom: 15, pitch: 45 }}
      baseMap={baseMap}
    />
  );
};

let dataMaxDistance = 0;
const createData = (centerCoord, bboxRadius, density = 300) => {
  const ruler = cheapRuler(centerCoord[1], "meters");
  const bbox = ruler.bufferPoint(centerCoord, bboxRadius);
  let grid = pointGrid(bbox, density, { units: "meters" });
  const lowerRightCoord = [bbox[2], bbox[1]];
  const colorScale = chroma.scale("Spectral").domain([0, 6]);

  grid.features.forEach((f, i) => {
    const currentPoint = f.geometry.coordinates;
    const d = ruler.distance(currentPoint, lowerRightCoord);
    dataMaxDistance = Math.round(Math.max(dataMaxDistance, d));
    f.properties["index"] = i;
    f.properties["distance"] = d;
    f.properties["rotation"] = d % 360;
    f.properties["colour"] = colorScale(d % 6).rgb();
  });
  grid.features.slice(0, 3);
  return grid;
};

const data = createData(
  [VIEWS.helsinki.longitude, VIEWS.helsinki.latitude],
  55000 * 2
);
const featureCount = data.features.length;

export const GridStressTest = () => {
  const [currentIndex, setCurrentIndex] = useState(0);

  const range = select(
    "Select range",
    [500, 1000, 2000, 5000, 10000, 20000, featureCount / 2, featureCount],
    2000,
    KNOB_GROUP
  );

  const handler = useCallback(
    (e, i) => {
      setCurrentIndex(i);
    },
    [currentIndex]
  );

  const layerOptions = {
    getFilterValue: (d) => d.properties.distance,
    filterRange: [currentIndex, currentIndex + range],
    filterSoftRange: [currentIndex + range * 0.3, currentIndex + range * 0.7],
    extensions: [new DataFilterExtension({ filterSize: 1 })],
    getFillColor: (d) => d.properties.colour,
  };

  const layers = [
    new GeoJsonLayer({
      ...layerOptions,
      data: data,
      getRadius: 100,
      id: "sp-geojsonlayer",
    }),
    new IconLayer({
      ...layerOptions,
      id: "sp-iconlayer",
      data: data.features,
      getPosition: (d) => d.geometry.coordinates,
      iconAtlas: "images/icons/icon-atlas.png",
      iconMapping: ICON_MAPPING,
      getIcon: (d) => "navigation",
      getSize: 100,
      sizeUnits: "meters",
      getAngle: (d) => d.properties.rotation,
    }),
  ];

  const timeSlider = () => {
    return {
      label: `Distance: ${Math.round(currentIndex)} Objects: ${
        featureCount * layers.length
      }`,
      control: (
        <Slider
          value={currentIndex}
          onChange={handler}
          defaultValue={dataMaxDistance / 2}
          step={dataMaxDistance / 200}
          min={0}
          max={dataMaxDistance}
        />
      ),
    };
  };

  return (
    <DemoGrid
      controls={[timeSlider()]}
      layers={layers}
      view={{ ...VIEWS.helsinki, zoom: 8 }}
    />
  );
};

export const TripLayerTransit = () => {
  const trailLength = createTrailKnob();
  const [currentTime, setCurrentTime] = useState(0);
  const [data, setData] = useState([]);
  const [timeExtent, setTimeExtent] = useState({ minTs: 0, maxTs: 0 });

  const TRANSIT_TRIP_DATA_URL = createTripsKnob();

  useEffect(() => {
    jsonFetch(TRANSIT_TRIP_DATA_URL).then((paths) => {
      //set the min and max timestamps based on the current data
      let extents = [];
      paths.forEach((d) => {
        extents.push(_min(d.properties.tsi));
        extents.push(_max(d.properties.tsi));
      });
      const minTs = _min(extents);
      const maxTs = _max(extents);
      setTimeExtent({ minTs, maxTs });

      setData(colorTransformer(paths));
    });
  }, [TRANSIT_TRIP_DATA_URL]);

  const handleTimeChange = useCallback((ev, ts) => {
    setCurrentTime(ts);
  }, []);

  const colorTransformer = (data, prevData) => {
    const colorRange = chroma.scale("Purples").domain([0, data.length]);
    const dataWithColor = data.map((d, i) => {
      d.properties.color = colorRange(i).rgb();
      return d;
    });
    return dataWithColor;
  };

  const tripLayerUS = new TripsLayer({
    id: "trips-layer-transit",
    data,
    getPath: (d) => d.coordinates,
    // deduct start timestamp from each data point to avoid overflow
    getTimestamps: (d) => d.properties.tsi.map((t) => t - timeExtent.minTs),
    getColor: (d) => d.properties.color,
    opacity: 0.8,
    widthMinPixels: 3,
    rounded: true,
    trailLength: trailLength * 60,
    currentTime,
  });
  const controls = createDefaultControls({
    currentTime,
    timeChangeCallback: handleTimeChange,
    ...timeExtent,
    trailLength,
  });

  return (
    <DemoGrid
      controls={controls}
      layers={[tripLayerUS]}
      view={VIEWS.helsinki}
    />
  );
};

const calculateBearing = (c1, c2) => {
  let b = 0;
  if (c1 && c2) {
    b = bearingToAzimuth(bearing(c1, c2));
  }
  return Math.round(b);
};

export const DirectionOfTravel = () => {
  const showHeadings = boolean("display headings", false, KNOB_GROUP);

  const trailLength = createTrailKnob();
  const TRANSIT_TRIP_DATA_URL = createTripsKnob();
  const [data, setData] = useState([]);
  const [timeExtent, setTimeExtent] = useState({ minTs: 0, maxTs: 0 });

  const [currentTime, setCurrentTime] = useState(0);

  useEffect(() => {
    jsonFetch(TRANSIT_TRIP_DATA_URL).then((paths) => {
      //set the min and max timestamps based on the current data
      let extents = [];
      paths.forEach((d) => {
        extents.push(_min(d.properties.tsi));
        extents.push(_max(d.properties.tsi));
      });
      setTimeExtent({ minTs: _min(extents), maxTs: _max(extents) });

      setData(tripToEventsTransformer(paths));
    });
  }, [TRANSIT_TRIP_DATA_URL]);

  const handleTimeChange = useCallback((ev, ts) => {
    setCurrentTime(ts);
  }, []);

  const textLayerHeading = new TextLayer({
    id: "text-layer-heading",
    data,
    getPosition: (d) => d.coordinates,
    getText: (d) => "" + d.hdgCalc,
    getColor: (d) => [255, 165, 0],
    getFilterValue: (d) => d.tsi - timeExtent.minTs,
    filterRange: [currentTime - trailLength * 60, currentTime],
    extensions: [new DataFilterExtension({ filterSize: 1 })],
    getPixelOffset: [0, 35],
    sizeMaxPixels: 30,
    sizeMinPixels: 10,
    getSize: 5,
    sizeScale: 3,
    billboard: true,
    visible: showHeadings,
    fontFamily: "monospace",
    opacity: 1,
    updateTriggers: {
      getColor: [currentTime, trailLength],
      visible: showHeadings,
    },
  });

  const iconLayerHeadingCalc = new IconLayer({
    id: "icon-layer-heading-calc",
    data,
    getPosition: (d) => d.coordinates,
    getColor: (d) => [255, 65, 0],
    getFilterValue: (d) => d.tsi - timeExtent.minTs,
    filterRange: [currentTime - trailLength * 60, currentTime],
    extensions: [new DataFilterExtension({ filterSize: 1 })],
    getSize: 4,
    sizeScale: 10,
    sizeMaxPixels: 28,
    getAngle: (d) => -d.hdgCalc,
    iconAtlas: "images/icons/icon-atlas.png",
    iconMapping: ICON_MAPPING,
    getIcon: (d) => "navigation",
    opacity: 0.5,
    updateTriggers: {
      getColor: [currentTime, trailLength],
    },
  });

  const controls = createDefaultControls({
    currentTime,
    timeChangeCallback: handleTimeChange,
    ...timeExtent,
    trailLength,
  });

  return (
    <DemoGrid
      controls={controls}
      layers={[iconLayerHeadingCalc, textLayerHeading]}
      view={VIEWS.helsinki}
    />
  );
};

export const SymbologyByType = () => {
  let layerType = null;
  layerType = radios(
    "Symbol Layer Type",
    {
      Icons: "icons",
      "Web Font": "font",
    },
    "icons",
    KNOB_GROUP
  );

  const trailLength = createTrailKnob();
  const [currentTime, setCurrentTime] = useState(0);
  const [data, setData] = useState([]);
  const [timeExtent, setTimeExtent] = useState({ minTs: 0, maxTs: 0 });

  const TRANSIT_TRIP_DATA_URL = createTripsKnob();

  useEffect(() => {
    jsonFetch(TRANSIT_TRIP_DATA_URL).then((paths) => {
      //set the min and max timestamps based on the current data
      let extents = [];
      paths.forEach((d) => {
        extents.push(_min(d.properties.tsi));
        extents.push(_max(d.properties.tsi));
      });
      const minTs = _min(extents);
      const maxTs = _max(extents);
      setTimeExtent({ minTs, maxTs });

      setData(eventIconToTypeTransformer(tripToEventsTransformer(paths)));
    });
  }, [TRANSIT_TRIP_DATA_URL]);

  const handleTimeChange = useCallback((ev, ts) => {
    setCurrentTime(ts);
  }, []);

  const TRANSIT_TYPES_TO_ICONS = {
    bus: { iconHex: "\ue530", icon: "directions_bus", colorIndex: 0 },
    tram: { iconHex: "\ue535", icon: "tram", colorIndex: 1 },
    train: { iconHex: "\ue534", icon: "directions_railway", colorIndex: 2 },
    ferry: { iconHex: "\ue532", icon: "directions_boat", colorIndex: 5 },
    metro: { iconHex: "\ue533", icon: "directions_subway", colorIndex: 3 },
    ubus: { iconHex: "\ue530", icon: "tram", colorIndex: 4 },
  };

  const eventIconToTypeTransformer = (data) => {
    const colorScale = chroma.scale("Paired").colors(6, "rgb");

    const newData = data.map((d) => {
      const mode = TRANSIT_TYPES_TO_ICONS[d.transport_mode];
      return {
        ...d,
        iconHex: mode.iconHex,
        color: colorScale[mode.colorIndex],
        icon: mode.icon,
      };
    });

    return newData;
  };

  const textLayerByType = new TextLayer({
    id: "text-layer-by-type",
    data,
    getPosition: (d) => d.coordinates,
    getText: (d) => d.iconHex,
    getColor: (d) => d.color,
    getFilterValue: (d) => d.tsi - timeExtent.minTs,
    filterRange: [currentTime - trailLength * 60, currentTime],
    extensions: [new DataFilterExtension({ filterSize: 1 })],
    sizeMaxPixels: 40,
    sizeMinPixels: 25,
    getSize: 3,
    sizeScale: 10,
    billboard: false,
    fontFamily: "Material",
    characterSet: Object.values(TRANSIT_TYPES_TO_ICONS).map((v) => {
      return v.iconHex;
    }),
    opacity: 0.5,
    visible: layerType !== "icons",
    updateTriggers: {
      getColor: [currentTime, trailLength],
      visible: layerType,
    },
  });

  const iconLayerByType = new IconLayer({
    id: "icon-layer-heading-calc",
    data,
    getPosition: (d) => d.coordinates,
    getColor: (d) => d.color,
    getFilterValue: (d) => d.tsi - timeExtent.minTs,
    filterRange: [currentTime - trailLength * 60, currentTime],
    extensions: [new DataFilterExtension({ filterSize: 1 })],
    sizeMaxPixels: 40,
    getSize: 4,
    sizeScale: 10,
    iconAtlas: "images/icons/icon-atlas.png",
    iconMapping: ICON_MAPPING,
    getIcon: (d) => d.icon,
    opacity: 0.5,
    visible: layerType == "icons",
    updateTriggers: {
      getColor: [currentTime, trailLength],
      visible: layerType,
    },
  });

  const controls = createDefaultControls({
    currentTime,
    timeChangeCallback: handleTimeChange,
    ...timeExtent,
    trailLength,
  });

  return (
    <DemoGrid
      controls={controls}
      layers={[iconLayerByType, textLayerByType]}
      view={VIEWS.helsinki}
    />
  );
};

export const Acceleration = () => {
  const trailLength = createTrailKnob();
  const [currentTime, setCurrentTime] = useState(0);
  const [data, setData] = useState([]);
  const [timeExtent, setTimeExtent] = useState({ minTs: 0, maxTs: 0 });

  const TRANSIT_TRIP_DATA_URL = createTripsKnob();

  useEffect(() => {
    jsonFetch(TRANSIT_TRIP_DATA_URL).then((paths) => {
      //set the min and max timestamps based on the current data
      let extents = [];
      paths.forEach((d) => {
        extents.push(_min(d.properties.tsi));
        extents.push(_max(d.properties.tsi));
      });
      const minTs = _min(extents);
      const maxTs = _max(extents);
      setTimeExtent({ minTs, maxTs });

      setData(accelerationToColourTransformer(tripToEventsTransformer(paths)));
    });
  }, [TRANSIT_TRIP_DATA_URL]);

  const handleTimeChange = useCallback((ev, ts) => {
    setCurrentTime(ts);
  }, []);

  const layerStyle = options(
    "visualization style",
    ["points", "column"],
    "points",
    { display: "inline-radio" },
    KNOB_GROUP
  );

  const accelerationToColourTransformer = (data, prevData) => {
    const speeds = _reduce(
      data,
      (res, d) => {
        return res.concat(d.spd);
      },
      []
    );
    // collect the full set of possible accelerations (positve only) to use for color domain calculations
    const accelerations = _reduce(
      data,
      (res, d) => {
        return res.concat([].concat(d.acc).filter((a) => a > 0)); //filter out decellerations negative
      },
      []
    );

    // chroma quantile domain limits - In the quantile mode, the input domain is divided by quantile ranges
    const accelerationDomains = chroma.limits(accelerations, "q", 3);
    //only the last two quantile should be red - highest acceleration (setting them explicitly to avoid blending)
    var colorRange = chroma
      .scale(["green", "green", "red", "red"])
      .domain(accelerationDomains);

    const dataWithColor = data.map((d, i) => {
      const acceleration = d.acc ? d.acc : 0.03;
      const c = colorRange(acceleration).rgb();
      d.color = c;
      return d;
    });
    return dataWithColor;
  };

  const layers = [
    new ScatterplotLayer({
      id: "plot-layer-acceleration",
      data,
      getPosition: (d) => d.coordinates,
      getFillColor: (d) => d.color,
      getFilterValue: (d) => d.tsi - timeExtent.minTs,
      filterRange: [currentTime - trailLength * 60, currentTime],
      extensions: [new DataFilterExtension({ filterSize: 1 })],
      getLineColor: [0, 0, 0],
      getRadius: 12,
      radiusMinPixels: 3,
      opacity: 1,
      filled: true,
      stroked: false,
      visible: layerStyle == "points",
      updateTriggers: {
        getFillColor: [currentTime, trailLength],
        visible: layerStyle,
      },
    }),
    new ColumnLayer({
      id: "column-layer-acceleration",
      data,
      getPosition: (d) => d.coordinates,
      getFillColor: (d) => d.color,
      getLineColor: [0, 0, 0],
      getFilterValue: (d) => d.tsi - timeExtent.minTs,
      filterRange: [currentTime - trailLength * 60, currentTime],
      extensions: [new DataFilterExtension({ filterSize: 1 })],
      getElevation: (d) => d.spd,
      elevationScale: 100,
      coverage: 1,
      extruded: true,
      radius: 12,
      opacity: 1,
      filled: true,
      stroked: false,
      pickable: true,
      autoHighlight: true,
      visible: layerStyle == "column",
      updateTriggers: {
        getFillColor: [currentTime, trailLength],
        visible: layerStyle,
      },
    }),
  ];

  const controls = createDefaultControls({
    currentTime,
    timeChangeCallback: handleTimeChange,
    ...timeExtent,
    trailLength,
  });

  return (
    <DemoGrid
      controls={controls}
      layers={layers}
      view={{ ...VIEWS.helsinki, pitch: 54 }}
    />
  );
};

export const AnimatedTransit = () => {
  return <TransitAnimated />;
};

AnimatedTransit.story = {
  parameters: {
    options: {
      withKnobs: {
        disabled: true, // do not show the knobs addon on this story
      },
    },
  },
};

export const FollowPath = () => {
  return (
    <div>
      <FollowPathMap />
    </div>
  );
};

export const VisualizationOptions = () => {
  const [data, setData] = useState({ paths: [], points: [] });
  const [filteredData, setFilteredData] = useState({ paths: [], points: [] });
  const [vehicles, setVehicles] = useState([]);
  const classes = useStyles();

  const layerStyle = options(
    "visualization style",
    [
      "points",
      "tracks",
      "arcs",
      "icons",
      "column",
      "polygon",
      "heatmap",
      "3D grid",
      "screen grid",
      "hexagon",
      "ducks",
      "trucks",
    ],
    "points",
    { display: "inline-radio" },
    KNOB_GROUP
  );

  const baseMap = createBaseLayerKnob();

  const colorScale = options(
    "colour scheme",
    [...Object.keys(chroma.brewer)],
    "YlOrRd",
    { display: "inline-radio" },
    KNOB_GROUP
  );
  const colorRange = chroma.scale(colorScale).colors(6, "rgb");

  const stopsToEventsTransformer = (data) => {
    const newData = _reduce(
      data,
      (result, d) => {
        const obj = _pick(d.properties, [
          "oday",
          "operator_id",
          "vehicle_number",
          "route_id",
          "direction_id",
          "transport_mode",
          "start_time",
        ]);

        const heatMapWeight =
          d.properties.duration_seconds / d.properties.coordinates.length;

        d.properties.coordinates.forEach((c, i, a) => {
          result.push({
            ...obj,
            coordinates: c,
            weight: heatMapWeight,
            coordinatesTarget: a[i + 1] ? a[i + 1] : a[i], //only provide a target when available
          });
        });
        return result;
      },
      []
    );

    return newData;
  };

  const vehiclesListFromData = (data) => {
    const allV = data.map((d) => d.properties.vehicle_number);
    return _uniq(allV);
  };

  useEffect(() => {
    jsonFetch(
      "https://infratrode.gitlab.io/public-data/transit/stops-500m-10minutes.json"
    ).then((stops) => {
      setVehicles(vehiclesListFromData(stops));
      let points = stopsToEventsTransformer(stops);

      const pathDurations = stops.map((p) => p.properties.duration_seconds);
      const pointDurations = points.map((p) => p.weight);
      const pathColorScale = chroma
        .scale(colorScale)
        .classes(chroma.limits(pathDurations, "q", 6));
      const pointsColorScale = chroma
        .scale(colorScale)
        .classes(chroma.limits(pointDurations, "q", 6));
      //update the paths and points with colors
      stops.forEach((p) => {
        p.properties.color = pathColorScale(
          p.properties.duration_seconds
        ).rgb();
      });
      points.forEach((p) => {
        p.color = pointsColorScale(p.weight).rgb();
      });

      const dataObj = { paths: stops, points };
      setData(dataObj);
      setFilteredData(dataObj);
    });
  }, [colorScale]);

  const dataFilterReducer = (state, newState) => {
    const filter = { ...state, ...newState };

    const vehicleFilter = (d) => {
      return !filter.vehicle || d.properties.vehicle_number === filter.vehicle;
    };
    const timeFilter = (d) => {
      return (
        !filter.timeRange ||
        (d.properties.tsi > filter.timeRange.min &&
          d.properties.tsi < filter.timeRange.max)
      );
    };

    const paths = data.paths.filter(vehicleFilter);
    setFilteredData({ paths, points: pathToEventsTransformer(paths) });
    return filter;
  };

  const [filter, setFilter] = useReducer(dataFilterReducer, {
    timeRange: "",
    vehicle: "",
  });

  const handleVehicleChange = (ev) => {
    const vehicle = ev.target.value;
    setFilter({ vehicle });
  };

  const showThreeD = boolean("display 3D", true, KNOB_GROUP);

  const radiusFactor = number(
    "radius",
    1,
    { range: true, min: 0.1, max: 4, step: 0.1 },
    KNOB_GROUP
  );

  const layers = [
    new ColumnLayer({
      id: "column-layer",
      data: filteredData.points,
      diskResolution: 12,
      coverage: 1,
      radius: 10 * radiusFactor,
      extruded: showThreeD,
      pickable: true,
      autoHighlight: true,
      elevationScale: 2,
      getPosition: (d) => d.coordinates,
      getFillColor: (d) => d.color,
      getLineColor: [0, 0, 0],
      getElevation: (d) => d.weight,
      visible: layerStyle == "column",
      opacity: 0.2,
      updateTriggers: {
        visible: layerStyle,
        extruded: showThreeD,
        radius: radiusFactor,
      },
    }),
    new PathLayer({
      id: "stop-path",
      data: filteredData.paths,
      getPath: (d) => d.properties.coordinates,
      pickable: true,
      widthMinPixels: 3,
      getWidth: (d) => 5,
      getColor: (d) => d.properties.color,
      filled: true,
      opacity: 0.8,
      visible: layerStyle == "tracks",
      updateTriggers: {
        visible: layerStyle,
      },
    }),
    new PolygonLayer({
      id: "stop-poly",
      data: filteredData.paths,
      getPolygon: (d) => d.properties.poly,
      getElevation: (d) => d.properties.duration_seconds,
      extruded: showThreeD,
      elevationScale: 1,
      pickable: true,
      autoHighlight: true,
      getFillColor: (d) => d.properties.color,
      getLineColor: [255, 0, 0],
      filled: true,
      opacity: 0.8,
      wireframe: false,
      visible: layerStyle == "polygon",
      updateTriggers: {
        visible: layerStyle,
        extruded: showThreeD,
      },
    }),
    new ScatterplotLayer({
      id: "stop-points",
      data: filteredData.points,
      getPosition: (d) => d.coordinates,
      pickable: true,
      getRadius: 10 * radiusFactor,
      autoHighlight: true,
      getFillColor: (d) => d.color,
      getLineColor: (d) => d.color,
      filled: true,
      opacity: 0.8,
      visible: layerStyle == "points",
      updateTriggers: {
        visible: layerStyle,
        getRadius: radiusFactor,
      },
    }),
    new HeatmapLayer({
      id: "stop-heat",
      data: filteredData.points,
      getPosition: (d) => d.coordinates,
      getWeight: (d) => d.weight,
      visible: layerStyle == "heatmap",
      colorRange: colorRange,
      updateTriggers: {
        visible: layerStyle,
      },
    }),
    new GridLayer({
      id: "stop-grid",
      data: filteredData.points,
      getPosition: (d) => d.coordinates,
      getElevationWeight: (d) => d.weight,
      elevationScale: 2,
      cellSize: 250 * radiusFactor,
      extruded: showThreeD,
      visible: layerStyle == "3D grid",
      colorRange: colorRange,
      pickable: true,
      autoHighlight: true,
      updateTriggers: {
        visible: layerStyle,
        extruded: showThreeD,
        cellSize: radiusFactor,
      },
    }),
    new HexagonLayer({
      id: "stop-hex",
      data: filteredData.points,
      getPosition: (d) => d.coordinates,
      getElevationWeight: (d) => d.weight,
      colorRange,
      elevationScale: 1,
      radius: 150 * radiusFactor,
      coverage: 1,
      extruded: showThreeD,
      pickable: true,
      autoHighlight: true,
      visible: layerStyle == "hexagon",
      updateTriggers: {
        visible: layerStyle,
        extruded: showThreeD,
        radius: radiusFactor,
      },
    }),
    new ScenegraphLayer({
      id: "scenegraph-layer",
      data: filteredData.points,
      getPosition: (d) => d.coordinates,
      getColor: [255, 255, 0],
      sizeScale: 10 * radiusFactor,
      scenegraph:
        "https://raw.githubusercontent.com/KhronosGroup/glTF-Sample-Models/master/2.0/Duck/glTF-Binary/Duck.glb",
      getOrientation: [180, 0, -90],
      getScale: [1, 1, 1],
      _lighting: "pbr",
      visible: layerStyle == "ducks",
      updateTriggers: {
        visible: layerStyle,
        sizeScale: radiusFactor,
      },
    }),
    new ScenegraphLayer({
      id: "scenegraph-layer-truck",
      data: filteredData.points,
      getPosition: (d) => d.coordinates,
      getColor: (d) => d.color,
      sizeScale: 0.05 * radiusFactor,
      scenegraph: "https://infratrode.gitlab.io/public-data/models/truck.glb",
      getOrientation: [180, 0, -90],
      getScale: [1, 1, 1],
      _lighting: "pbr",
      visible: layerStyle == "trucks",
      updateTriggers: {
        visible: layerStyle,
        sizeScale: radiusFactor,
      },
    }),
    new IconLayer({
      id: "icon-layer-demo",
      data: filteredData.points,
      getPosition: (d) => d.coordinates,
      getColor: (d) => d.color,
      getSize: 40 * radiusFactor,
      sizeScale: 10,
      sizeMaxPixels: 38 * radiusFactor,
      iconAtlas: "images/icons/icon-atlas.png",
      iconMapping: ICON_MAPPING,
      getIcon: (d) => "report",
      opacity: 1.0,
      visible: layerStyle == "icons",
      updateTriggers: {
        visible: layerStyle,
        getSize: radiusFactor,
      },
    }),
    new ScreenGridLayer({
      id: "screengrid-layer-demo",
      data: filteredData.points,
      getPosition: (d) => d.coordinates,
      getWeight: (d) => d.weight,
      cellSizePixels: 15 * radiusFactor,
      visible: layerStyle == "screen grid",
      colorRange,
      pickable: true,
      autoHighlight: true,
      updateTriggers: {
        visible: layerStyle,
        cellSizePixels: radiusFactor,
      },
    }),
    new ArcLayer({
      id: "arc-layer",
      data: filteredData.points,
      getSourcePosition: (d) => d.coordinates,
      getTargetPosition: (d) => d.coordinatesTarget,
      getSourceColor: (d) => d.color.concat([0.4]),
      getTargetColor: (d) => d.color,
      getWidth: 6,
      visible: layerStyle == "arcs",
      pickable: true,
      autoHighlight: true,
      updateTriggers: {
        visible: layerStyle,
      },
    }),
  ];

  return (
    <div>
      <div>
        <FormControl className={classes.formControl}>
          <InputLabel shrink id="select-label">
            Filter By Vehicle
          </InputLabel>
          <Select
            labelId="select-label"
            id="select-veh"
            value={(filter && filter?.vehicle) || ""}
            displayEmpty
            onChange={handleVehicleChange}
            className={classes.selectEmpty}
          >
            <MenuItem aria-label="None" value="">
              <em>None</em>
            </MenuItem>
            {vehicles.map((v) => {
              return (
                <MenuItem key={v} value={v}>
                  {v}
                </MenuItem>
              );
            })}
          </Select>
        </FormControl>
      </div>
      <DemoGrid
        controls={[]}
        layers={layers}
        baseMap={baseMap}
        view={{ ...VIEWS.helsinki, zoom: 12, pitch: 45 }}
      />
    </div>
  );
};

//not for general de
const IconOptions = () => {
  const calcGridDimensions = (n) => {
    let dim = 0;
    let i = 0;
    while (n > dim) {
      dim = Math.pow(++i, 2);
    }
    return i;
  };
  const DISTANCE_METERS = 50;
  const iconKeys = Object.keys(ICON_MAPPING);
  const ruler = cheapRuler(VIEWS.helsinki.latitude, "meters");

  const gridDims = calcGridDimensions(iconKeys.length);
  const BUFFER_DISTANCE = (gridDims / 2) * DISTANCE_METERS;
  const bbox = ruler.bufferPoint(
    [VIEWS.helsinki.longitude, VIEWS.helsinki.latitude],
    BUFFER_DISTANCE
  );

  const grid = pointGrid(bbox, DISTANCE_METERS, { units: "meters" });
  const data = grid.features.map((f, i) => {
    return {
      key: iconKeys[i],
      coordinates: grid.features[i].geometry.coordinates,
    };
  });

  const layers = [
    new IconLayer({
      id: "icon-layer-demo",
      data,
      getPosition: (d) => d.coordinates,
      getColor: (d) => [255, 65, 0],
      getSize: 40,
      sizeScale: 10,
      sizeMaxPixels: 38,
      iconAtlas: "images/icons/icon-atlas.png",
      iconMapping: ICON_MAPPING,
      getIcon: (d) => d.key,
      opacity: 1.0,
    }),
    new TextLayer({
      id: "text-layer-demo",
      data,
      getPosition: (d) => d.coordinates,
      getText: (d) => d.key,
      getColor: (d) => [255, 65, 0],
      getSize: 10,
      sizeScale: 10,
      sizeMaxPixels: 18,
      getPixelOffset: [0, 30],
      opacity: 1.0,
    }),
  ];

  return (
    <div>
      <DemoGrid
        controls={[]}
        layers={layers}
        view={{ ...VIEWS.helsinki, zoom: 12 }}
      />
    </div>
    /*  */
  );
};
