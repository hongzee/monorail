import { PathLayer, ScatterplotLayer, TextLayer } from "@deck.gl/layers";
import Button from "@material-ui/core/Button";
import { createStyles, makeStyles } from "@material-ui/core/styles";
import { number, boolean } from "@storybook/addon-knobs";
import bearing from "@turf/bearing";
import { bearingToAzimuth } from "@turf/helpers";
import { zip } from "d3-array";
import { easeExpOut as theEase } from "d3-ease";
import { mean, median } from "d3-array";
import { json as jsonFetch } from "d3-fetch";
import { interpolateBasis, quantize, interpolateNumber } from "d3-interpolate";
import { interval } from "d3-timer";
import DeckGL, { FlyToInterpolator, MapController } from "deck.gl";
import React, { useCallback, useEffect, useState } from "react";
import StaticMap from "react-map-gl";
import { MAP_DEFAULT_VIEW } from "./cache";

// const useStyles = makeStyles((theme: Theme) =>
const useStyles = makeStyles((theme) =>
  createStyles({
    root: {
      "& > *": {
        margin: theme.spacing(1),
      },
    },
  })
);

const normalize = (val, max, min) => {
  return (val - min) / (max - min);
};

const calculateBearing = (c1, c2) => {
  let b = 0;
  if (c1 && c2) {
    b = bearingToAzimuth(bearing(c1, c2));
  } else {
    console.error("error [c1,c2]:", [c1, c2]);
  }
  return Math.round(b);
};

const transformToInterpolatedCurves = (d, n) => {
  const lon = interpolateBasis(d.coordinates.map((d) => d[0]));
  const lat = interpolateBasis(d.coordinates.map((d) => d[1]));
  const coordinates = zip(quantize(lon, n), quantize(lat, n));

  const hdg = coordinates.map((c, i, a) => {
    return calculateBearing(c, a[i + 1] ? a[i + 1] : a[i - 1]);
  });

  return {
    type: "LineString",
    coordinates,
    properties: {
      hdg,
    },
  };
};

const FollowPathMap = () => {
  const initialView = {
    ...MAP_DEFAULT_VIEW,
    zoom: 20,
    latitude: 60.21038,
    longitude: 25.07801,
    height: 900,
    width: 800,
  };
  const baseMap =
    "https://api.maptiler.com/maps/streets/style.json?key=R01BPnJMraEGqoIU2zM6";
  const [currentViewState, setViewState] = useState({ ...initialView });
  const [data, setData] = useState([]);
  const [dataPoints, setDataPoints] = useState([]);
  const [cameraPath, setCameraPath] = useState([]);
  const [cameraPoints, setCameraPoints] = useState([]);
  const [timer, setTimer] = useState();

  const MAX_SMOOTHING_SAMPLES = 5000;
  let samples = number(
    "smoothing samples",
    MAX_SMOOTHING_SAMPLES * 0.75,
    {
      range: true,
      min: 100,
      max: MAX_SMOOTHING_SAMPLES,
      step: MAX_SMOOTHING_SAMPLES / 100,
    },
    "viewgroup"
  );

  const MIN_DELAY_MS = 10;
  const MAX_DELAY_MS = 2500;
  const delayRangeMS = number(
    "speed",
    MAX_DELAY_MS * 0.8,
    {
      range: true,
      min: MIN_DELAY_MS,
      max: MAX_DELAY_MS - MIN_DELAY_MS,
      step: MIN_DELAY_MS,
    },
    "viewgroup"
  );
  const delay_ms =
    MAX_DELAY_MS * normalize(delayRangeMS, MIN_DELAY_MS, MAX_DELAY_MS);
  const showCameraLayers = boolean(
    "Show Camera Path (for demo only)",
    true,
    "viewgroup"
  );

  useEffect(() => {
    jsonFetch(
      "https://infratrode.gitlab.io/public-data/transit/trips-1.json"
    ).then((d) => {
      setData(d);
      setDataPoints(
        d[0].coordinates.map((c, i) => {
          if (c) return { coordinates: c, hdg: d[0].properties.hdg[i] };
        })
      );

      const cameraPath = transformToInterpolatedCurves(d[0], samples);
      setCameraPath([cameraPath]);

      setCameraPoints(
        cameraPath.coordinates.map((c, i) => {
          return { coordinates: c, hdg: cameraPath.properties.hdg[i] };
        })
      );
    });
  }, [samples]);

  const cities = [
    { label: "Helsinki", latitude: 60.17177, longitude: 24.94135 },
    { label: "New York", latitude: 40.6643, longitude: -73.9385 },
    { label: "New York 2", latitude: 40.07, longitude: -73.831 },
    { label: "Los Angeles", latitude: 34.0194, longitude: -118.4108 },
    { label: "Chicago", latitude: 41.8376, longitude: -87.6818 },
    { label: "Houston", latitude: 29.7805, longitude: -95.3863 },
    { label: "Phoenix", latitude: 33.5722, longitude: -112.088 },
    { label: "Philadelphia", latitude: 40.0094, longitude: -75.1333 },
    { label: "San Antonio", latitude: 29.4724, longitude: -98.5251 },
  ];

  const COLOUR = {
    camera: [254, 153, 44],
    original_data: [0, 20, 200],
  };

  const layers = [
    new PathLayer({
      id: "path-layer",
      data,
      pickable: false,
      widthMinPixels: 3,
      getPath: (d) => d.coordinates,
      getColor: (d) => COLOUR.original_data,
      getWidth: (d) => 1,
      opacity: 0.1,
    }),
    new TextLayer({
      id: "text-layer-camera",
      data: cameraPoints,
      getPosition: (d) => d.coordinates,
      getText: (d) => `${d.hdg}`,
      getColor: (d) => COLOUR.camera,
      getPixelOffset: [55, 55],
      sizeMaxPixels: 30,
      sizeMinPixels: 10,
      getSize: 7,
      sizeScale: 3,
      fontFamily: "monospace",
      opacity: 1,
      visible: showCameraLayers,
      updateTriggers: {
        visible: showCameraLayers,
      },
    }),
    new PathLayer({
      id: "camera-layer",
      data: cameraPath,
      pickable: false,
      widthMinPixels: 1,
      getPath: (d) => d.coordinates,
      getColor: (d) => COLOUR.camera,
      getWidth: (d) => 0.5,
      visible: showCameraLayers,
      updateTriggers: {
        visible: showCameraLayers,
      },
    }),
    new ScatterplotLayer({
      id: "camera-layer-scatterplot",
      data: cameraPoints,
      getPosition: (d) => d.coordinates,
      pickable: false,
      getRadius: (d) => 1,
      widthMinPixels: 2,
      getFillColor: (d) => COLOUR.camera,
      filled: true,
      stroked: false,
      opacity: 0.8,
      visible: showCameraLayers,
      updateTriggers: {
        visible: showCameraLayers,
      },
    }),
    new ScatterplotLayer({
      id: "data-layer-scatterplot",
      data: dataPoints,
      getPosition: (d) => d.coordinates,
      pickable: false,
      getRadius: (d) => 2,
      widthMinPixels: 2,
      getFillColor: (d) => COLOUR.original_data,
      filled: true,
      stroked: false,
      opacity: 0.4,
    }),
  ];

  // const onViewStateChange = ({viewState, oldViewState, interactionState}) => {
  const onViewStateChange = ({ viewState }) => {
    setViewState(viewState);
  };

  const gotoCity = useCallback((dataIndex) => {
    if (!cities[dataIndex]) {
      return;
    }
    const { latitude, longitude } = cities[dataIndex];
    const newView = {
      viewState: {
        ...currentViewState,
        latitude,
        longitude,
        transitionInterpolator: new FlyToInterpolator(),
        transitionDuration: "auto",
        transitionEasing: theEase,
      },
    };

    onViewStateChange(newView);
  });

  const gotoNextPoint = (i) => {
    const d = cameraPath[0];
    const coordinates = d.coordinates[i];
    const bearing = d.properties.hdg[i];
    const newView = {
      viewState: {
        ...currentViewState,
        latitude: coordinates[1],
        longitude: coordinates[0],
        bearing,
        pitch: 59,
        // transitionInterpolator: new FlyToInterpolator(),
        transitionDuration: delay_ms,
        transitionEasing: theEase,
      },
    };
    onViewStateChange(newView);
  };

  function animate(startIndex, endIndex, delay) {
    var coef = startIndex;
    const animationInterval = interval((t) => {
      coef += 1;
      if (coef >= endIndex) {
        animationInterval.stop();
      }
      gotoNextPoint(coef);
    }, delay);
    return animationInterval;
  }

  const start = (delay) => {
    const endIndex = cameraPath[0].coordinates.length;
    if (timer) {
      timer.stop();
      setTimer(null);
    } else {
      setTimer(animate(0, endIndex, delay));
    }
  };

  return (
    <div>
      <div style={{ height: "100%" }}>
        <DeckGL
          // initialViewState={MAP_DEFAULT_VIEW} //note: cannot provide both initial and viewState
          height={800}
          viewState={currentViewState}
          onViewStateChange={onViewStateChange}
          layers={layers}
          controller={MapController}
        >
          <StaticMap reuseMaps mapStyle={baseMap} preventStyleDiffing={true} />
        </DeckGL>
      </div>
      <div>
        {/* {cities.map((city, i) => {
          return (
            <Button
              key={`button_${i}`}
              variant='contained'
              onClick={() => {
                gotoCity(i);
              }}
            >
              {city.label}
            </Button>
          );
        })} */}
        <Button
          variant="contained"
          onClick={() => {
            start(delay_ms);
          }}
        >
          {timer ? `stop` : `start`}
        </Button>
      </div>
    </div>
  );
};
export default FollowPathMap;
