import React, { useState } from "react";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";

import Container from "@material-ui/core/Container";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    formControl: {
      margin: theme.spacing(1),
      minWidth: 120,
    },
  })
);

export default function StyleSelector({ mapStyle, onStyleChange, baseMaps }) {
  const [drawerOpen, setDrawerOpen] = useState(false);
  const [selectedStyle, setSelectedStyle] = useState(mapStyle);
  const classes = useStyles();

  const handleChange = (event: React.ChangeEvent<{ value: unknown }>) => {
    setSelectedStyle(event.target.value as string);
    onStyleChange(event.target.value as string);
  };

  return (
    <Container>
      <FormControl className={classes.formControl}>
        <InputLabel id="select-label">Map Style</InputLabel>
        <Select
          labelId="select-label"
          id="simple-select"
          value={selectedStyle}
          onChange={handleChange}
        >
          {Object.keys(baseMaps).map((baseMapKey) => {
            return (
              <MenuItem key={baseMapKey} value={baseMaps[baseMapKey]}>
                {baseMapKey}
              </MenuItem>
            );
          })}
        </Select>
      </FormControl>
    </Container>
  );
}
