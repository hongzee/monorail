import React, { useState } from "react";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";

import IconButton from "@material-ui/core/IconButton";
import ChevronLeft from "@material-ui/icons/ChevronLeft";
import ChevronRight from "@material-ui/icons/ChevronRight";
import Container from "@material-ui/core/Container";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    formControl: {
      margin: theme.spacing(1),
      minWidth: 120,
    },
  })
);

export default function SideDrawer(props) {
  const [drawerOpen, setDrawerOpen] = useState(false);
  const { components } = props;
  const classes = useStyles();

  return (
    <div
      style={{
        position: "absolute",
        top: 66,
        right: 0,
        height: drawerOpen ? "92%" : "50px",
        width: drawerOpen ? "450px" : "50px",
        background: "linear-gradient(95deg, black, transparent)",
        borderRadius: "25px",

        // : "transparent",
      }}
      onClick={(event) => {
        event.stopPropagation();
        event.preventDefault();
      }}
    >
      <Container
        style={{
          height: "60px",
          position: "absolute",
          top: "0px",
          right: "23px",
        }}
      >
        <IconButton
          aria-label="toggle"
          onClick={() => setDrawerOpen(!drawerOpen)}
        >
          {drawerOpen ? <ChevronRight /> : <ChevronLeft />}
        </IconButton>
      </Container>
      {drawerOpen && (
        <Container style={{ paddingTop: "50px" }}>{components}</Container>
      )}
    </div>
  );
}
