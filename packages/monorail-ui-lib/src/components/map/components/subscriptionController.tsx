import React, { useState } from "react";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Switch from "@material-ui/core/Switch";
import Button from "@material-ui/core/Button";
import Badge from "@material-ui/core/Badge";
import FormControl from "@material-ui/core/FormControl";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";
import Container from "@material-ui/core/Container";
import InputLabel from "@material-ui/core/InputLabel";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    formControl: {
      margin: theme.spacing(1),
      minWidth: 320,
    },
  })
);

export const SubscriptionController = ({
  subscriptionEnabled,
  onSubscriptionToggle,
  liveData,
  onLiveDataToggle,
  newDataCount,
  onLoadData,
  filters,
  onFilterChange,
  filter,
}) => {
  const [isLiveDataEnabled, setIsLiveDataEnabled] = useState(liveData);
  const [isSubscriptionEnabled, setIsSubscriptionEnabled] = useState(
    subscriptionEnabled
  );
  const [selectedFilter, setSelectedFilter] = useState(filter);
  const classes = useStyles();

  const handleSubscriptionChange = () => {
    if (isSubscriptionEnabled) {
      setIsLiveDataEnabled(false);
    }
    setIsSubscriptionEnabled(!isSubscriptionEnabled);
    onSubscriptionToggle();
  };

  const handleLiveDataChange = () => {
    setIsLiveDataEnabled(!isLiveDataEnabled);
    onLiveDataToggle();
  };

  const handleFilterChange = (event: React.ChangeEvent<{ value: unknown }>) => {
    setSelectedFilter(event.target.value as string);
    onFilterChange(event.target.value as string);
  };

  return (
    <Container style={{ paddingTop: "30px" }}>
      <FormControlLabel
        control={
          <Switch
            checked={isSubscriptionEnabled}
            onChange={handleSubscriptionChange}
            onClick={handleSubscriptionChange}
            name="subscriptions"
            color="primary"
          />
        }
        label="Subscription Enabled"
      />
      {isSubscriptionEnabled && (
        <React.Fragment>
          <FormControlLabel
            control={
              <Switch
                checked={isLiveDataEnabled}
                onChange={handleLiveDataChange}
                onClick={handleLiveDataChange}
                name="liveData"
                color="primary"
              />
            }
            label="Live Data"
          />
          {!isLiveDataEnabled && (
            <Badge badgeContent={newDataCount} color="primary">
              <Button
                variant="contained"
                disabled={liveData}
                onClick={onLoadData}
              >
                Load Data
              </Button>
            </Badge>
          )}
          <FormControl className={classes.formControl}>
            <InputLabel id="select-label">Subsciption Filter</InputLabel>
            <Select
              labelId="select-label"
              id="simple-select"
              value={selectedFilter}
              onChange={handleFilterChange}
            >
              {filters.map((filter) => {
                return (
                  <MenuItem key={filter} value={filter}>
                    {filter}
                  </MenuItem>
                );
              })}
            </Select>
          </FormControl>
        </React.Fragment>
      )}
    </Container>
  );
};
export default SubscriptionController;
