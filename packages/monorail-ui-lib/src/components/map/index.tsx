// app.js
import DeckGL from "@deck.gl/react";
import React, { useEffect } from "react";
import StaticMap from "react-map-gl";
import {
  ApolloClient,
  HttpLink,
  ApolloProvider,
  // split,
  gql,
  useQuery,
} from "@apollo/client";
// import { getMainDefinition } from "@apollo/client/utilities";
// import { WebSocketLink } from "@apollo/client/link/ws";
import {
  mapView,
  baseMap,
  cache,
  MAP_DEFAULT_BASEMAP,
  BASE_MAPS,
  MAP_DEFAULT_VIEW,
} from "./cache";

import SideDrawer from "./components/sideDrawer";
import StyleSelector from "./components/styleSelector";
import SubscriptionController from "./components/subscriptionController";

// Create an http link:
const httpLink = new HttpLink({
  uri: "https://gql.monorail.infratrode.com/graphql",
});

const APOLLO_CLIENT: ApolloClient<object> = new ApolloClient({
  //cache: new InMemoryCache(),
  cache,
  link: httpLink,
  connectToDevTools: true,
});

const GET_MAP_STATE = gql`
  query {
    baseMap @client
    mapView @client
  }
`;

const DeckGLComponent = (props) => {
  const {
    height,
    width,
    layers = [],
    initialView = { ...MAP_DEFAULT_VIEW, ...props.initialView },
    baseMap = MAP_DEFAULT_BASEMAP,
  } = props;

  return (
    <DeckGL
      height={height}
      width={width}
      initialViewState={initialView}
      controller={true}
      layers={layers}
    >
      <StaticMap reuseMaps mapStyle={baseMap} preventStyleDiffing={true} />
      {props.children}
    </DeckGL>
  );
};

//Loads the intial map state from the Apollo Cache to pass to map component
const ApolloMap = (props) => {
  const { data, error } = useQuery(GET_MAP_STATE);
  useEffect(() => {
    //When the basemap prop changes, set it in the apollo cache
    if (props.baseMap && props.baseMap !== baseMap()) {
      baseMap(props.baseMap);
    }
  }, [props.baseMap]);

  useEffect(() => {
    //When the basemap prop changes, set it in the apollo cache
    if (props.initialView && props.intialView !== mapView()) {
      mapView(props.initalView);
    }
  }, [props.initalView]);

  const handleStyleChange = (style) => {
    baseMap(style);
  };

  //Add the components passed in as props to the component array
  //These components are displayed in the side panel
  const componentProps = props.components ? props.components : [];
  const components = [
    <StyleSelector
      key={"styleselector"}
      mapStyle={data ? data.baseMap : MAP_DEFAULT_BASEMAP}
      onStyleChange={handleStyleChange}
      baseMaps={BASE_MAPS}
    />,
    ...componentProps,
  ];

  if (!data) {
    return <SimpleMap {...props} />;
  }

  return (
    <DeckGLComponent
      {...props}
      baseMap={data.baseMap}
      initialView={data.mapView}
    >
      <SideDrawer components={components} />
    </DeckGLComponent>
  );
};

//Map component with Apollo Provider- stores map state in the Apollo cache
export const DeckMap = (props: any) => {
  //Use components prop on the Map to pass down components to the side drawer
  return (
    <ApolloProvider client={APOLLO_CLIENT}>
      <ApolloMap {...props} />
    </ApolloProvider>
  );
};

//Simple Map component with no Apollo Provider- no storage in cache, works off props
export const SimpleMap = (props: any) => {
  return <DeckGLComponent {...props} />;
};

export default DeckMap;
