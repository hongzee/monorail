import React from 'react';
import { StateProvider } from './context/store.js';
import ImagesPanel from './ImagesPanel';

export const Images = props => {

  return (
    <StateProvider>
      <ImagesPanel />
    </StateProvider>
  );
};

export default Images;
