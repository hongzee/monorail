import React, { useEffect, useState, useContext } from 'react';
import axios from 'axios';

//material ui
import Skeleton from '@material-ui/lab/Skeleton';

//components
import FilterPanel from "./filter/FilterPanel";
import ImageGridReactPhotoGallery from './grids/reactPhotoGallery/ImageGridReactPhotoGallery';

//context
import { store } from './context/store.js';

import moment from 'moment';

const momentRandom = require('moment-random');
const startDate = moment('2019-10-10 00:00:00');

export const ImagesPanel = props => {

  const globalState = useContext(store);
  const { dispatch } = globalState;

  const [isLoading, setIsLoading] = useState(true);
    
    useEffect(() => {
      const fetchData = async () => {
        const response = await axios(        
          'https://pixabay.com/api/?key=15443994-8337ceee83849b83f69fae558&q=nature&per_page=200&image_type=photo'
        );
               
        let images = response.data.hits.map(i => ({
          ...i,
          width: i.previewWidth,
          height: i.previewHeight,
          src: i.previewURL,         
          url: i.previewURL,
          date: momentRandom(moment.now(), startDate).format('YYYY-MM-DD hh:MM:ss')        
        }));
          
        //console.log("Images response: ", images);
  
        dispatch({ type: 'setImages', payload: images });
  
        setIsLoading(false);
      }
  
      if (isLoading) {
        fetchData();
      }
    }, [isLoading])
 
    return (
      isLoading ? <Skeleton variant="rect"  /> :        
      <div style={{marginTop: '20px'}}>
        <FilterPanel/>
        <ImageGridReactPhotoGallery/>        
      </div>
    );
};

export default ImagesPanel;
