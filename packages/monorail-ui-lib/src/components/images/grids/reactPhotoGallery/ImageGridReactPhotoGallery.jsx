import React, { useState, useEffect, useCallback, useContext } from 'react';

//components
import Gallery from 'react-photo-gallery';
import Image from './Image';


//material ui
import Divider from '@material-ui/core/Divider';
import List from '@material-ui/core/List';
import Typography from '@material-ui/core/Typography';

//icons
import DateRangeIcon from '@material-ui/icons/DateRange';

//context
import { store } from '../../context/store';

//libraries
import _ from 'lodash';
import moment from 'moment';

const ImageGridReactPhotoGallery = () => {
  const globalState = useContext(store);
  const [filteredImages, setFilteredImages] = useState(globalState.state.images);
  const [isGrouped, setIsGrouped] = useState(false);
  
  useEffect(() => {

    if (globalState.state.images && globalState.state.displayCriteria)
      applyFilter();

  }, [globalState.state.images, globalState.state.displayCriteria])

  const applyFilter = () => {    
    let filteredImages;

    if(globalState.state.displayCriteria.date){
      //date filter
      filteredImages = _.filter(globalState.state.images, (image) => {
        return new moment(image.date).format('YYYYMMDD') > new moment(globalState.state.displayCriteria.date).format('YYYYMMDD')
      })            
    }else{
      //no date filter
      filteredImages = globalState.state.images;
    }

    if(globalState.state.displayCriteria.groupBy){                        
      let groupedImages = _(filteredImages)
      .groupBy(i => moment(i.date).format('YYYY-MM-DD'))      
      .map((value,key) => ({date: key, images: value}))      
      .value();
      
      //console.log("filteredImages grouped: ", groupedImages);

      let ordered = _.orderBy(groupedImages, [globalState.state.displayCriteria.groupBy.value], [globalState.state.displayCriteria.groupBy.order]);

      //console.log("ordered: ", ordered);
      filteredImages = ordered;      
      setIsGrouped(true);
    }else{      
      filteredImages = filteredImages;
      setIsGrouped(false);
    }
    console.log("filteredImages final: ", filteredImages);

    setFilteredImages(filteredImages);     
  }

  const imageRenderer = useCallback(
    ({ index, left, top, key, photo }) => (
      <Image key={photo.id} image={photo} />
    )
  );

  return (
    globalState.state.images ? 
    
    isGrouped ? 
    <div>
      {filteredImages.length > 0 ?
         <List>
         {filteredImages.map((group) => 
           <div key={group.key} style={{marginTop: '10px'}}>
             <div style={{display: 'flex',  alignItems: 'center', width: '100%'}}>
                <DateRangeIcon/>
                <Typography variant="h6" style={{marginLeft: '5px'}}>{group.date}</Typography>                
             </div>             
             <Divider style={{width: '100%', height: '1px', margin: "10px 0px 10px 0px"}}/>
             <Gallery photos={group.images} renderImage={imageRenderer} /> 
           </div>            
         )}
         </List>
        :
        <p>No Images</p>}
    </div> 
    :
    <div>
      {filteredImages.length > 0 ?
        <Gallery photos={filteredImages} renderImage={imageRenderer} />
        :
        <p>No Images</p>}
    </div> 
    
    : 
    <p>No Images</p>
  );
}

export default ImageGridReactPhotoGallery;
