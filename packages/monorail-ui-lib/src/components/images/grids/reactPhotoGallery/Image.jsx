import React, { useState, useEffect } from 'react';

import { InView } from 'react-intersection-observer'

import Skeleton from '@material-ui/lab/Skeleton';

function ImageRPG({ image, showImagePanel }) {

  const [state, setState] = useState({
    viewed: false,
    loaded: false
  })

  useEffect(() => {    
  }, [image, state.viewed, state.loaded])

  return (
    <InView as="div" onChange={(inView, entry) => {
      if (inView && !state.viewed) {    
        setState({
          viewed: true,
          loaded: false
        })
      }
    }}>
      {state.viewed ? <div style={{ display: 'flex', flexDirection: 'column' }}>
       
        <img style={{ cursor: 'pointer' }} src={image.webformatURL} width={image.webformatWidth / 2} height={image.webformatHeight / 2} onClick={() => {
          showImagePanel(image.id)
        }} onLoad={() => setState({
          ...state,
          loaded: true
        })} />       
        {image.date}
        

      </div> : <Skeleton variant="rect" width={300} height={250} />}

    </InView>

  );
}

export default ImageRPG;
