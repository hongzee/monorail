import React, { createContext, useReducer } from 'react';

const initialState = {
    displayCriteria: {
        date: undefined,
        groupBy: undefined
    },
    images: [] 
};

const store = createContext(initialState);
const { Provider } = store;

const StateProvider = ({ children }) => {
    const [state, dispatch] = useReducer((state, action) => {
        switch (action.type) {
            case 'setImages':
                return {
                    ...state,
                    images: action.payload                                    
                };
            case 'setDisplayCriteria':
                return {
                    ...state,
                    displayCriteria: action.payload
                };        
            default:
                throw new Error();
        };
    }, initialState);

    return <Provider value={{ state, dispatch }}>{children}</Provider>;
};

export { store, StateProvider }