import React, { useContext } from 'react';
import { makeStyles } from '@material-ui/core/styles';

// material-ui
import Button from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Tooltip from '@material-ui/core/Tooltip';
import Typography from '@material-ui/core/Typography';

//icons
import ClearAllIcon from '@material-ui/icons/ClearAll';
import TuneIcon from '@material-ui/icons/Tune';

import moment from 'moment';

import { store } from '../context/store';

const useStyles = makeStyles(theme => ({
  expansionPanelSummary: {
    margin: '0px',
    padding: '0px',
    maxHeight: '28px',
    '&$expanded': {
      margin: '12px 0',
    },
  },
  expansionPanelSummaryContent: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginLeft: '10px',
    width: '100%',
  },
  expansionPanelContent: {
    display: 'flex',
    flexDirection: 'row',
    margin: '0px',
    padding: '0px',
  },
  expansionPanelContentColumn: {
    display: 'flex',
    flexDirection: 'column',
    margin: '5px'
  },
  header: {
    fontSize: '14px',
    marginLeft: '5px',
    fontWeight: '500',
    letterSpacing: '0.007px',
    textTransform: 'uppercase'
  },
  buttonLabel: {
    margin: '0px',
    padding: '0px',
    justifyContent: 'start',
    fontSize: '12px',
    fontWeight: '400',
    textTransform: 'capitalize'
  }
}));


const FilterPanel = () => {

  const classes = useStyles();

  const globalState = useContext(store);
  const { dispatch } = globalState;

  const [expanded, setExpanded] = React.useState('filter');

  const handleChange = panel => (event, newExpanded) => {
    setExpanded(newExpanded ? panel : false);
  };

  const setDateCriteria = (value) => {
    let newDate = undefined;

    switch (value) {
      case 'today': {
        newDate = moment();
        break;
      }
      case '24h': {
        newDate = moment().subtract(1, 'days');
        break;
      }
      case '48h': {
        newDate = moment().subtract(2, 'days');
        break;
      }
      case '7d': {
        newDate = moment().subtract(7, 'days');
        break;
      }
      case '30d': {
        newDate = moment().subtract(30, 'days');
        break;
      }
      case 'year': {
        newDate = moment().subtract(moment().dayOfYear(), 'days');
        break;
      }
      case 'all': {
        newDate = undefined;
        break;
      }
      case 'clear': {
        newDate = undefined;
        break;
      }
      default: {
        newDate = undefined;
      }
    }
    let displayCriteria = {
      ...globalState.state.displayCriteria,
      date: newDate
    };
    dispatch({ type: 'setDisplayCriteria', payload: displayCriteria });
  }

  const setGroupByCriteria = (value) => {
    let newGroupBy = undefined;

    if (value)
      newGroupBy = value;

    let displayCriteria = {
      ...globalState.state.displayCriteria,
      groupBy: newGroupBy
    };
    dispatch({ type: 'setDisplayCriteria', payload: displayCriteria });
  }

  const clearAll = (event) => {
    setDateCriteria(undefined);
    setGroupByCriteria(undefined);
    event.stopPropagation();
  };

  return (
    <ExpansionPanel style={{ margin: "5px, 0px" }} square expanded={expanded === 'filter'} onChange={handleChange('filter')}>
      <ExpansionPanelSummary className={classes.expansionPanelSummary}>
        <div className={classes.expansionPanelSummaryContent}>
          <Divider />
          <TuneIcon />
          <Typography style={{ marginLeft: '5px' }}>FILTER</Typography>
          <Tooltip title="Clear Filter">
            <IconButton style={{ marginLeft: 'auto' }} onClick={clearAll}>
              <ClearAllIcon />
            </IconButton>
          </Tooltip>
        </div>
      </ExpansionPanelSummary>
      <ExpansionPanelDetails className={classes.expansionPanelContent}>
        <div className={classes.expansionPanelContentColumn}>
          <Typography className={classes.header} display="block" gutterBottom>
            IMAGE DATE
            </Typography>
          <Divider />
          <Button size="small" classes={{
            label: classes.buttonLabel
          }}
            onClick={() => setDateCriteria("today")}>
            Today
            </Button>
          <Button size="small" classes={{
            label: classes.buttonLabel
          }}
            onClick={() => setDateCriteria("24h")}>
            Last 24H
            </Button>
          <Button size="small" classes={{
            label: classes.buttonLabel
          }}
            onClick={() => setDateCriteria("48h")}>
            Last 48H
            </Button>
          <Button size="small" classes={{
            label: classes.buttonLabel
          }}
            onClick={() => setDateCriteria("7d")}>
            Last 7 Days
            </Button>
          <Button size="small" classes={{
            label: classes.buttonLabel
          }}
            onClick={() => setDateCriteria("30d")}>
            Last 30 Days
            </Button>
          <Button size="small" classes={{
            label: classes.buttonLabel
          }}
            onClick={() => setDateCriteria("year")}>
            This Year
            </Button>
          <Divider />
          <Button size="small" classes={{
            label: classes.buttonLabel
          }}
            onClick={() => setDateCriteria(undefined)}>
            Clear
            </Button>
        </div>
        <div className={classes.expansionPanelContentColumn}>
          <Typography className={classes.header} display="block" gutterBottom>
            SORT BY
            </Typography>
          <Divider />
          <Button classes={{
            label: classes.buttonLabel
          }} onClick={() => setGroupByCriteria({
            value: 'date',
            order: 'desc'
          })}>Date Descending</Button>
          <Button classes={{
            label: classes.buttonLabel
          }} onClick={() => setGroupByCriteria({
            value: 'date',
            order: 'asc'
          })}>Date Ascending</Button>
          <Divider />
          <Button classes={{
            label: classes.buttonLabel
          }} onClick={() => setGroupByCriteria(undefined)}>Clear</Button>
        </div>

      </ExpansionPanelDetails>
    </ExpansionPanel >
  );
}

export default FilterPanel;
