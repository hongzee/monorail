import React from "react";
import { Container } from "@material-ui/core";
import { Dashboard } from "@monorail/ui-lib";

import "../App.css";

function DashboardPage() {
  return (
    <Container style={{ margin: "1em 0" }}>
      <Dashboard />
    </Container>
  );
}
export default DashboardPage;
