import React, { useState } from "react";
import _get from "lodash/get";
import { gql, useSubscription } from "@apollo/client";
import { ScatterplotLayer, PathLayer, IconLayer } from "@deck.gl/layers";
import { PRESET_FILTERS, subscriptionFilter } from "./cache";
import {
  isValidCoord,
  scatterplot_options,
  iconlayer_options,
  mapToArrayTransformer,
} from "../../constants/constants";
// import { ICON_MAPPING } from "./icon-mapping";
import { DeckMap, SubscriptionController } from "@monorail/ui-lib";

const TRANSIT_SUBSCRIPTION_BY_KAFKA_WITH_FILTER = gql`
  subscription($filter: JourneyFilter!) {
    subscribeToKafkaJourney(filter: $filter) {
      oper
      veh
      tsi
      tst
      spd
      hdg
      lat
      long
      acc
    }
  }
`;

export interface MonorailMapProps {}

function MonorailMap(props: MonorailMapProps) {
  const [events, setEvents] = useState(new Map());
  const [newEvents, setNewEvents] = useState(new Map());
  const [newDataCount, setNewDataCount] = useState(0);
  const [liveData, setLiveData] = useState(false);
  const [subscriptionEnabled, setSubscriptionEnabled] = useState(false);

  const updateEvents = (event: any) => {
    if (!isValidCoord(event.lat, event.long)) {
      return;
    }
    // const key = `${event.oper}_${event.veh}_${event.tsi}`;
    const key = `${event.oper}_${event.veh}`;
    let data = liveData ? new Map(events) : new Map(newEvents);

    // let data = new Map(events);
    // if (!subscriptionEnabled) {
    //   if (newEvents.size > 10) {
    //     data = new Map();
    //   } else {
    //     data = new Map(newEvents);
    //   }
    //   if (newDataCount < 99) {
    //     setNewDataCount((prev) => prev + 1);
    //   }
    //   // console.log("newEvents.size:", newEvents.size);
    // }

    let obj = data.get(key) || { coords: [], tsi: [], hdg: [] };

    obj.coords.unshift([event.long, event.lat]);
    obj.hdg.unshift(event.hdg);
    obj.tsi.unshift(event.tsi);

    if (obj.coords.length > 6) {
      obj.coords.pop();
      obj.tsi.pop();
      obj.hdg.pop();
    }
    data.set(key, obj);

    //If live data is on add to the events map
    if (liveData) {
      // console.log("sub event", events.size);
      setEvents(data);
    } else {
      //Otherwise add to a separate new events map to be added later
      // console.log("not sub event", newEvents.size);
      setNewEvents(data);
      if (newDataCount < 99) {
        setNewDataCount((prev) => prev + 1);
      }
    }
  };
  // const { data, error } =
  useSubscription(TRANSIT_SUBSCRIPTION_BY_KAFKA_WITH_FILTER, {
    variables: { filter: PRESET_FILTERS[subscriptionFilter()] },
    skip: !subscriptionEnabled,
    shouldResubscribe: true,
    onSubscriptionData: ({ subscriptionData }) => {
      const event = _get(
        subscriptionData,
        "data.subscribeToKafkaJourney",
        false
      );

      if (event) {
        console.log("new event");
        updateEvents(event);
      } else {
        console.log(subscriptionData);
      }
    },
  });

  const handleSubscriptionToggle = () => {
    //Subscriptions state has changed
    //If the live data has been turned on, then copy and empty whatever is in newEvents to the events
    if (subscriptionEnabled) {
      setLiveData(false);
    }
    setSubscriptionEnabled(!subscriptionEnabled);
  };

  const handleLiveDataToggle = () => {
    //Subscriptions state has changed
    // console.log("subscriptionEnabled:", subscriptionEnabled);
    //If the live data has been turned on, then copy and empty whatever is in newEvents to the events
    if (!liveData) {
      handleLoadData();
    }
    setLiveData(!liveData);
  };

  const handleLoadData = () => {
    let mergedMap: Map<string, any> = new Map([
      ...Array.from(events.entries()),
      ...Array.from(newEvents.entries()),
    ]);
    setEvents(mergedMap);
    setNewEvents(new Map());
    setNewDataCount(0);
  };

  const handleFilterChange = (filter: any) => {
    console.log("changeFilter:", filter);
    subscriptionFilter(filter);
    setEvents(new Map());
    setNewEvents(new Map());
    setNewDataCount(0);
  };

  const pastPositionScatterplot = new ScatterplotLayer({
    ...scatterplot_options,
    id: "past-scatterplot",
    data: mapToArrayTransformer(events),
    getPosition: (d: any) => d.coords,
  });

  const currentPositionIcon = new IconLayer({
    ...iconlayer_options,
    data: events,
  });

  // const pastPositionIcon = new IconLayer({
  //   ...icon_options,
  //   id: "past-postion-icon",
  //   data: mapToArrayTransformer(events),
  //   getPosition: (d: any) => d.coords,
  //   getAngle: (d: any) => -d["hdg"],
  //   getIcon: (d: any) => "expand_less",
  // });

  const pathLayer = new PathLayer({
    id: "path-layer",
    data: events,
    getPath: (d: any) => d[1].coords,
    getColor: [255, 65, 0],
    getWidth: 3,
    opacity: 0.5,
    widthUnits: "meters",
    billboard: true,
  });

  console.log("subscriptionEnabled:", subscriptionEnabled);
  console.log("liveData:", liveData);

  return (
    <DeckMap
      layers={[pathLayer, currentPositionIcon, pastPositionScatterplot]}
      components={[
        <SubscriptionController
          key={"subscriptioncontroller"}
          onSubscriptionToggle={handleSubscriptionToggle}
          subscriptionEnabled={subscriptionEnabled}
          newDataCount={newDataCount}
          liveData={liveData}
          onLiveDataToggle={handleLiveDataToggle}
          onLoadData={handleLoadData}
          filters={Object.keys(PRESET_FILTERS)}
          filter={subscriptionFilter()}
          onFilterChange={handleFilterChange}
        />,
      ]}
      // initialView={VIEW}
    />
  );
}

export default MonorailMap;
