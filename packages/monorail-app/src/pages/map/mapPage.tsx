import React from "react";

import {
  ApolloClient,
  HttpLink,
  // InMemoryCache,
  ApolloProvider,
  split,
} from "@apollo/client";
import { getMainDefinition } from "@apollo/client/utilities";
import { WebSocketLink } from "@apollo/client/link/ws";

import MonorailMap from "./monorailMap";
import { cache } from "./cache";
import "../../App.css";

// Create an http link:
const httpLink = new HttpLink({
  uri: "https://gql.monorail.infratrode.com/graphql",
});

// Create a WebSocket link:
const wsLink = new WebSocketLink({
  uri: `wss://gql.monorail.infratrode.com/graphql`,
  options: {
    reconnect: true,
  },
});

const link = split(
  // split based on operation type
  ({ query }) => {
    const definition = getMainDefinition(query);
    return (
      definition.kind === "OperationDefinition" &&
      definition.operation === "subscription"
    );
  },
  wsLink,
  httpLink
);

const apolloClient = new ApolloClient({
  cache,
  link,
});

const MapPage = () => {
  return (
    <ApolloProvider client={apolloClient}>
      <MonorailMap />
    </ApolloProvider>
  );
};

export default MapPage;
