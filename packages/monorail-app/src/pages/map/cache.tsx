import { InMemoryCache, ReactiveVar, makeVar } from "@apollo/client";

export const PRESET_FILTERS: any = {
  route_1003: { routeId: ["1003"] },
  route_1043: { routeId: ["1043"] },
  all_metros: { operatorId: 50 },
  all_trains: { operatorId: 90 },
  all_trams: { operatorId: 40 },
  two_vehicles: { operatorId: 30, vehicleNumber: [53, 52] },
  five_vehicles: { operatorId: 40, vehicleNumber: [472, 464, 451, 454, 455] },
  company_22: { operatorId: 22 },
  company_18: { operatorId: 18 },
};
// Create the defaultMapView var and initialize it with the initial value
// export const subscriptionFilter: ReactiveVar<any> = makeVar<any>("route_1003");
export const subscriptionFilter: ReactiveVar<any> = makeVar<any>(
  "two_vehicles"
);

export const cache = new InMemoryCache({
  typePolicies: {
    // Subscription: {
    //   fields: {
    //     subscriptionFilter: {
    //       read() {
    //         console.log("subscriptionFilter():", subscriptionFilter());
    //         return PRESET_FILTERS[subscriptionFilter()];
    //       },
    //     },
    //   },
    // },
    Query: {
      fields: {
        subscriptionFilter: {
          read() {
            console.log("subscriptionFilter():", subscriptionFilter());
            return PRESET_FILTERS[subscriptionFilter()];
          },
        },
      },
    },
  },
});
