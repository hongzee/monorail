import React, { useState, useRef, useCallback } from "react";
import useComponentSize from "@rehooks/component-size";

import Button from "@material-ui/core/Button";
import CircularProgress from "@material-ui/core/CircularProgress";
import Divider from "@material-ui/core/Divider";
import Drawer from "@material-ui/core/Drawer";
import Grid from "@material-ui/core/Grid";
import IconButton from "@material-ui/core/IconButton";
import DoubleArrowOutlined from "@material-ui/icons/DoubleArrowOutlined";
import Typography from "@material-ui/core/Typography";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";

import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import Checkbox from "@material-ui/core/Checkbox";
import FormControl from "@material-ui/core/FormControl";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Select from "@material-ui/core/Select";

import dayjs from "dayjs";

import { makeStyles, Theme, createStyles } from "@material-ui/core/styles";

import { NodeObject } from "react-force-graph-2d";
import { Network2D, MyNodeObject } from "@monorail/ui-lib";

import { GET_LINK_BY, GET_RM_CHARS, CLIENT, SET_LINK_BY } from "./network-gql";

import { ApolloProvider, useQuery, useMutation } from "@apollo/client";

const drawerWidth = 400;

interface LinkByData {
  linkBy: string;
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    base: {
      flexGrow: 1,
    },
    formControl: {
      margin: theme.spacing(1),
      minWidth: 120,
    },
    drawer: {
      width: drawerWidth,
      flexShrink: 0,
    },
    drawerPaper: {
      width: drawerWidth,
    },
    drawerContainer: {
      margin: "1em 0",
    },
    drawerDesc: {
      margin: "1em",
    },
    img: {
      margin: "auto",
      display: "block",
      maxWidth: "100%",
      maxHeight: "100%",
    },
    toolbar: theme.mixins.toolbar,
  })
);

const NetworkApp = () => {
  // Refs
  const targetRef = useRef<HTMLDivElement>(null);

  // States
  const [drawer, setDrawer] = useState<boolean>(false);
  const [selNode, setSelNode] = useState<MyNodeObject>({});
  const [categorize, setCategorize] = useState<boolean>(false);

  const [highlightNodes, setHighlightNodes] = useState(new Set());
  // const [highlightLinks, setHighlightLinks] = useState(new Set());
  const [hoverNode, setHoverNode] = useState<NodeObject | null>(null);

  const [page, setPage] = useState<number>(1);
  const [colorBy, setColorBy] = useState<string>("");

  const graphSize = useComponentSize(targetRef);
  const { width } = graphSize;

  // Styles
  const classes = useStyles();

  // GraphQL
  const { loading, error, data } = useQuery(GET_RM_CHARS, {
    variables: { page, categorize },
  });
  const { data: linkByData } = useQuery<LinkByData>(GET_LINK_BY);
  const [setLinkBy] = useMutation(SET_LINK_BY);

  // Handlers
  const onNodeClick = (node: MyNodeObject) => {
    if (!node?.class) {
      setDrawer(true);
      setSelNode(node);
    }
  };

  const toggleDrawer = () => {
    setDrawer((drawerState: boolean) => !drawerState);
  };

  const onLinkSelectionChange = (
    event: React.ChangeEvent<{ value: unknown }>
  ) => {
    setLinkBy({
      variables: { linkBy: event.target.value as string, categorize },
    });
  };

  const onColorSelectionChange = (
    event: React.ChangeEvent<{ value: unknown }>
  ) => {
    setColorBy(event.target.value as string);
  };

  const updateHighlight = () => {
    setHighlightNodes(highlightNodes);
    // setHighlightLinks(highlightLinks);
  };

  const handleNodeHover = (node: MyNodeObject | null) => {
    highlightNodes.clear();
    // highlightLinks.clear();

    if (node) {
      highlightNodes.add(node);
      // node.neighbors.forEach(neighbor => highlightNodes.add(neighbor));
      // node.links.forEach(link => highlightLinks.add(link));
    }

    setHoverNode(node || null);
    updateHighlight();
  };

  const handleCategorize = (event: React.ChangeEvent<HTMLInputElement>) => {
    setCategorize(event.target.checked);
    setLinkBy({
      variables: {
        linkBy: linkByData?.linkBy,
        categorize: event.target.checked,
      },
    });
  };

  const nodeCanvasObject = useCallback(
    (node: MyNodeObject, ctx) => {
      const { x = 0, y = 0, image = "", color = "#ea5400" } = node;
      const size = 50;

      // Handle hovering over node
      if (hoverNode === node) {
        ctx.beginPath();
        ctx.arc(node.x, node.y, size / 2 + 4, 0, 2 * Math.PI, false);
        ctx.fillStyle = "red";
        ctx.fill();
      } else {
        // Draws outline around nodes
        ctx.beginPath();
        ctx.fillStyle = color;
        ctx.arc(x, y, size / 2 + 3, 0, 2 * Math.PI, false);
        ctx.fill();
        ctx.closePath();
      }

      // Clips the images to the size of the defined arc
      ctx.beginPath();
      ctx.save();
      ctx.arc(x, y, size / 2, 0, 2 * Math.PI, false);
      ctx.clip();
      const img = new Image();
      img.src = image;
      ctx.drawImage(img, x - size / 2, y - size / 2, size, size);
      ctx.restore();
      ctx.closePath();
    },
    [hoverNode]
  );

  const filterOutKeyValues = (k: string) =>
    k !== "__typename" && k !== "image" && k !== "created" && k !== "id";

  const {
    image = "",
    name = "",
    gender = "",
    species = "",
    status = "",
    type = "",
    created = "",
  } = selNode;

  return (
    <Grid container ref={targetRef}>
      {loading && (
        <Grid item>
          <CircularProgress />
        </Grid>
      )}
      {error && (
        <Grid item>
          <Typography variant="h3">
            An error has occurred with the server.
          </Typography>
        </Grid>
      )}
      {!loading && !error && (
        <Grid container className={classes.base}>
          <Grid item xs={12}>
            <Grid container spacing={3} alignItems="center">
              <Grid item>
                <Button
                  variant="outlined"
                  size="large"
                  onClick={() => setPage((p: number) => p + 1)}
                >
                  Add Data
                </Button>
              </Grid>
              <Grid item>
                <FormControl
                  size="small"
                  variant="outlined"
                  className={classes.formControl}
                >
                  <InputLabel id="linkByLabel">LINK BY:</InputLabel>
                  <Select
                    labelId="linkByLabel"
                    id="linkBy"
                    value={linkByData?.linkBy}
                    onChange={onLinkSelectionChange}
                    label="Link By"
                  >
                    <MenuItem value="">
                      <em>NONE</em>
                    </MenuItem>
                    {data?.graphData?.nodes &&
                      Object.keys(data.graphData.nodes[0])
                        .filter(filterOutKeyValues)
                        .map((k: string) => (
                          <MenuItem key={k} value={k}>
                            {k.toUpperCase()}
                          </MenuItem>
                        ))}
                  </Select>
                </FormControl>
              </Grid>
              <Grid item>
                <FormControl
                  size="small"
                  variant="outlined"
                  className={classes.formControl}
                >
                  <InputLabel id="colorByLabel">COLOR BY:</InputLabel>
                  <Select
                    labelId="colorByLabel"
                    id="colorBy"
                    value={colorBy}
                    onChange={onColorSelectionChange}
                    label="Color By"
                  >
                    <MenuItem value="">
                      <em>NONE</em>
                    </MenuItem>
                    {data?.graphData?.nodes &&
                      Object.keys(data.graphData.nodes[0])
                        .filter(filterOutKeyValues)
                        .map((k: string) => (
                          <MenuItem key={k} value={k}>
                            {k.toUpperCase()}
                          </MenuItem>
                        ))}
                  </Select>
                </FormControl>
              </Grid>
              <Grid item>
                <FormControlLabel
                  control={
                    <Checkbox
                      checked={categorize}
                      onChange={handleCategorize}
                      name="Categorize"
                    />
                  }
                  label="Categorize"
                />
              </Grid>
            </Grid>
            <Grid item>
              <Grid container alignItems="center">
                <Grid item>
                  {data?.graphData && (
                    <Network2D
                      width={width}
                      graphData={data?.graphData}
                      nodeVal={50}
                      linkWidth={3}
                      distance={linkByData?.linkBy === "" ? 20 : 150}
                      strength={linkByData?.linkBy === "" ? -20 : -100}
                      nodeAutoColorBy={colorBy || "name"}
                      linkColor={() => "mediumvioletred"}
                      // dagMode="radialin"
                      onNodeClick={onNodeClick}
                      onNodeHover={handleNodeHover}
                      nodeCanvasObject={nodeCanvasObject}
                      nodeCanvasObjectMode={(node: MyNodeObject) =>
                        highlightNodes.has(node) ? "after" : "replace"
                      }
                    />
                  )}
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      )}
      <Drawer
        className={classes.drawer}
        variant="persistent"
        anchor="right"
        open={drawer}
        onClose={toggleDrawer}
        classes={{ paper: classes.drawerPaper }}
      >
        <div className={classes.toolbar} />
        <Grid
          className={classes.drawerContainer}
          container
          justify="center"
          alignItems="center"
        >
          <Grid item>
            <Grid
              container
              direction="row"
              justify="space-between"
              alignItems="center"
            >
              <Grid item>
                <Typography variant="h5" component="h5">
                  Profile
                </Typography>
              </Grid>
              <Grid item>
                <IconButton
                  aria-label="Close Drawer"
                  onClick={() => setDrawer(false)}
                >
                  <DoubleArrowOutlined />
                </IconButton>
              </Grid>
            </Grid>
            <Divider />
            <br />
            <img className={classes.img} alt={name} src={image} />
            <div className={classes.drawerDesc}>
              <Typography gutterBottom variant="h5" component="h2">
                {name}
              </Typography>
              <Divider />
              <List>
                <ListItem disableGutters>
                  <ListItemText
                    primary="Created"
                    secondary={dayjs(created).format("MMMM D YYYY, h:mm:ss A")}
                  />
                </ListItem>
                <ListItem disableGutters>
                  <ListItemText
                    primary="Status"
                    secondary={status}
                    secondaryTypographyProps={{
                      color: status === "Alive" ? "textSecondary" : "error",
                    }}
                  />
                </ListItem>
                <ListItem disableGutters>
                  <ListItemText primary="Species" secondary={species} />
                </ListItem>
                <ListItem disableGutters>
                  <ListItemText primary="Gender" secondary={gender} />
                </ListItem>
                <ListItem disableGutters>
                  <ListItemText primary="Type" secondary={type || "n/a"} />
                </ListItem>
              </List>
            </div>
          </Grid>
        </Grid>
      </Drawer>
    </Grid>
  );
};

function Network() {
  return (
    <ApolloProvider client={CLIENT}>
      <NetworkApp />
    </ApolloProvider>
  );
}

export default Network;
