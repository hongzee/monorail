import React from "react";
import renderer from "react-test-renderer";
import ReactDOM from "react-dom";
import { GET_LINK_BY, GET_RM_CHARS, CLIENT, SET_LINK_BY } from "./network-gql";
import { ApolloProvider, useQuery, useMutation } from "@apollo/client";

const Network = ({ page, categorize }) => {
  // GraphQL
  const { loading, error, data } = useQuery(GET_RM_CHARS, {
    variables: { page, categorize },
  });
  if (loading) return <p>Loading...</p>;
  if (error) return <p>Error!</p>;

  return <div>test</div>;
};

it("Get RM Characters", () => {
  renderer.create(
    <ApolloProvider client={CLIENT}>
      <Network page={1} categorize={true} />
    </ApolloProvider>
  );
});
