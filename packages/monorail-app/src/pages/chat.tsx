import React, { useState } from "react";
import {
  ApolloClient,
  HttpLink,
  InMemoryCache,
  ApolloProvider,
  useQuery,
  useMutation,
  gql,
  split,
} from "@apollo/client";
import { getMainDefinition } from "@apollo/client/utilities";
import { WebSocketLink } from "@apollo/client/link/ws";

// import { SubscriptionClient, addGraphQLSubscriptions } from 'subscriptions-transport-ws';
import Switch from "@material-ui/core/Switch";

import { Container } from "@material-ui/core";
import CommentContainer from "./commentContainer";
import Grid from "@material-ui/core/Grid";

const GET_CHARACTERS = gql`
  query {
    characters {
      id
      name
      desc
      comments {
        id
        user {
          id
          username
        }
        comment
        createdAt
      }
    }
  }
`;

const CREATE_COMMENT = gql`
  mutation($userId: Int!, $characterId: Int!, $comment: String!) {
    createComment(
      userId: $userId
      characterId: $characterId
      comment: $comment
    ) {
      id
      user {
        id
        username
      }
      character {
        id
        name
        desc
      }
      comment
      createdAt
    }
  }
`;

export interface ChatProps {
  user?: any;
}

const Chat = (props: ChatProps) => {
  const [subscriptions, setSubscriptions] = useState(false);
  const { data } = useQuery(GET_CHARACTERS);
  const [createComment] = useMutation(CREATE_COMMENT);
  const characters = data ? data.characters : [];
  const { user } = props;
  //   console.log("Render Chat", user);

  const handleCreateComment = (characterId: number, comment: string) => {
    createComment({
      variables: {
        userId: user.id,
        characterId: characterId,
        comment: comment,
      },
      //   update(cache, { data: { createComment } }) {
      //     const cacheResponse: any = cache.readQuery({ query: GET_CHARACTERS });
      //     const { characters = [] } = cacheResponse;
      //     const newCharacters = characters.map((character: any) => {
      //       if (character.id === characterId) {
      //         //add comment
      //         return {
      //           ...character,
      //           comments: character.comments.concat(createComment),
      //         };
      //       }
      //       return character;
      //     });
      //     cache.writeQuery({
      //       query: GET_CHARACTERS,
      //       data: { characters: newCharacters },
      //     });
      //   },
    });
  };

  return (
    <Container style={{ margin: "3em 0" }}>
      {user.username}
      <Grid component="label" container alignItems="center" spacing={1}>
        <Grid item>Off</Grid>
        <Grid item>
          <Switch
            checked={subscriptions}
            onChange={() => setSubscriptions(!subscriptions)}
            name="checkedC"
          />
        </Grid>
        <Grid item>On</Grid>
      </Grid>
      <CommentContainer
        data={characters}
        onComment={handleCreateComment}
        user={user.username}
        subscriptions={subscriptions}
      />
    </Container>
  );
};

// Create an http link:
const httpLink = new HttpLink({
  uri: "https://monorail-gql.herokuapp.com/graphql",
});
// Create a WebSocket link:
const wsLink = new WebSocketLink({
  uri: `wss://monorail-gql.herokuapp.com/graphql`,
  options: {
    reconnect: true,
    // connectionParams: {
    //     authToken: localStorage.getItem(AUTH_TOKEN),
    //   }
  },
});
const link = split(
  // split based on operation type
  ({ query }) => {
    const definition = getMainDefinition(query);
    return (
      definition.kind === "OperationDefinition" &&
      definition.operation === "subscription"
    );
  },
  wsLink,
  httpLink
);

const client = new ApolloClient({
  cache: new InMemoryCache(),
  link,
});

// Create an http link:
const httpLink2 = new HttpLink({
  uri: "https://monorail-gql.herokuapp.com/graphql",
});
// Create a WebSocket link:
const wsLink2 = new WebSocketLink({
  uri: `wss://monorail-gql.herokuapp.com/graphql`,
  options: {
    reconnect: true,
    // connectionParams: {
    //     authToken: localStorage.getItem(AUTH_TOKEN),
    //   }
  },
});
const link2 = split(
  // split based on operation type
  ({ query }) => {
    const definition = getMainDefinition(query);
    return (
      definition.kind === "OperationDefinition" &&
      definition.operation === "subscription"
    );
  },
  wsLink2,
  httpLink2
);

const client2 = new ApolloClient({
  cache: new InMemoryCache(),
  link: link2,
});

export const ChatPage = () => {
  return (
    <Grid container spacing={3}>
      <Grid item>
        <ApolloProvider client={client}>
          <Chat user={{ id: 1, username: "gm" }} />
        </ApolloProvider>
      </Grid>
      <Grid item>
        <ApolloProvider client={client2}>
          <Chat user={{ id: 2, username: "hz" }} />
        </ApolloProvider>
      </Grid>
    </Grid>
  );
};

export default ChatPage;
