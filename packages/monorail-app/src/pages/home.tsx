import React from "react";
import logo from "../logo.jpeg";
import { Typography, Container, Box, Grid } from "@material-ui/core";
import "../App.css";

function Home() {
  return (
    <Container style={{ margin: "1em auto" }}>
      <Grid container justify="center" alignItems="center">
        <Grid item>
          <div style={{ textAlign: "center", paddingBottom: "50px" }}>
            <img src={logo} className="App-logo" alt="logo" />
          </div>
          <Typography variant="h6">Monorails are proven</Typography>
          <Box component="span" m={1}>
            <Typography>
              Each and every day hundreds of thousands of passengers are carried
              on monorails. Many of the world's transit monorails exist in
              Japan, eight of which are full-scale urban transit systems. Others
              exist in Malaysia, Europe, Russia, Korea, China, Brazil, UAE,
              Saudi Arabia, Singapore and a few in the United States. Several
              more are either under construction or in advanced planning.
              Surprisingly, Walt Disney World's Monorail System near Orlando,
              Florida, has one of the highest riderships of all monorails. Well
              over 100,000 passenger trips are recorded each day on the 14 miles
              of beamways (a far higher ridership than most USA light rail
              systems). Nothing "Mickey Mouse" about that! The system is there
              to move people between six stations, not just amuse them.
            </Typography>
          </Box>
          <Typography variant="h6">Monorails are safe</Typography>
          <Box component="span" m={1}>
            <Typography>
              Whether they are of the straddle-beam or suspended variety, modern
              monorail technology makes derailment virtually impossible. As
              monorail is elevated, accidents with surface traffic are
              impossible. Zero accidents with pedestrians or surface traffic
              translates to no system down time, less liability suits and most
              importantly, a safer public. Street rail systems with grade
              crossings (light rail, trams or trollies) can't approach this
              level of safety, as any study of accident history will show.
            </Typography>
          </Box>
          <Typography variant="h6">
            Monorails are environmentally friendly
          </Typography>
          <Box component="span" m={1}>
            <Typography>
              Since most monorails are electrically powered, they are
              non-polluting. In 2007, the Las Vegas Monorail aided in the annual
              removal of an estimated 3.2 million vehicle miles from Southern
              Nevada’s major roadways and reduced emissions by more than 58 tons
              of carbon monoxide (CO), volatile organic compounds (VOC) and
              nitrogen oxides (NOx) over the course of the year. Most monorails
              run on rubber tires and are very quiet. Monorails are the most
              aesthetically pleasing of all elevated rail systems. Their sleek
              design blends in with modern urban environments. Quick
              construction time results in less disruption to the surrounding
              environments, whether business or residential. See our environment
              friendly page for details.
            </Typography>
          </Box>
        </Grid>
      </Grid>
    </Container>
  );
}
export default Home;
