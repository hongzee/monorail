import React, { useState } from "react";
import { makeStyles, Theme, createStyles } from "@material-ui/core/styles";
import clsx from "clsx";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Collapse from "@material-ui/core/Collapse";
import Grid from "@material-ui/core/Grid";
import CardMedia from "@material-ui/core/CardMedia";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import CommentsList from "./commentsList";
import Container from "@material-ui/core/Container";
import TextField from "@material-ui/core/TextField";
import Divider from "@material-ui/core/Divider";
import Avatar from "@material-ui/core/Avatar";
// import AccountCircle from "@material-ui/icons/AccountCircle";
// import FavoriteIcon from "@material-ui/icons/Favorite";
// import Chat from "@material-ui/icons/Chat";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import Button from "@material-ui/core/Button";
// import _orderBy from "lodash/orderBy";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      maxWidth: 445,
    },
    media: {
      height: 340,
    },
    expand: {
      transform: "rotate(0deg)",
      marginLeft: "auto",
      transition: theme.transitions.create("transform", {
        duration: theme.transitions.duration.shortest,
      }),
    },
    expandOpen: {
      transform: "rotate(180deg)",
    },
    title: {
      color: "darkgoldenrod",
    },
  })
);

export interface CommentContanerProps {
  data?: Array<any>;
  onComment?: (characterId: number, comment: string) => void;
  user?: string;
  subscriptions?: boolean;
}

export default function CommentContainer(props: CommentContanerProps) {
  const classes = useStyles();
  const [expanded, setExpanded] = useState(true);
  const [commentText, setCommentText] = useState<string>("");

  const {
    data = [],
    onComment = () => console.log("create comment"),
    user = "",
    subscriptions,
  } = props;

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };

  const createComment = (characterId: number) => {
    onComment(characterId, commentText);
    setCommentText("");
  };
  //   console.log("subscriptions:", subscriptions, user);
  return (
    <Container>
      {data.map((character) => (
        <Card className={classes.root}>
          <CardActionArea>
            <CardMedia
              className={classes.media}
              image="pickleRick.jpg"
              title="Pickle Rick- who else?"
            />
            <CardContent>
              <Typography
                gutterBottom
                variant="h5"
                component="h2"
                className={classes.title}
              >
                {character.name}
              </Typography>
              <Typography variant="body2" color="textSecondary" component="p">
                {character.desc}
              </Typography>
            </CardContent>
          </CardActionArea>
          <CardActions disableSpacing>
            {/* <IconButton aria-label="add to favorites">
          <FavoriteIcon />
        </IconButton> */}
            <IconButton
              className={clsx(classes.expand, {
                [classes.expandOpen]: expanded,
              })}
              onClick={handleExpandClick}
              aria-expanded={expanded}
              aria-label="show more"
            >
              <ExpandMoreIcon />
            </IconButton>
          </CardActions>
          <Collapse in={expanded} timeout="auto" unmountOnExit>
            <Divider />
            <CardContent>
              <Typography
                paragraph
                variant="body2"
              >{`${character.comments.length} Comments`}</Typography>
              <Grid container spacing={3} direction="row" xs={12}>
                <Grid item xs={12}>
                  <Grid container spacing={1} alignItems="flex-end">
                    <Grid item>
                      {/* <AccountCircle /> */}
                      <Avatar
                        alt={user.toUpperCase()}
                        src="/static/images/avatar/1.jpg"
                      />
                    </Grid>
                    <Grid item>
                      <TextField
                        id="input-with-icon-grid"
                        label="Add a comment..."
                        value={commentText}
                        onChange={(event) => setCommentText(event.target.value)}
                      />
                    </Grid>
                    <Grid item>
                      <Button
                        variant="contained"
                        color="secondary"
                        size="small"
                      >
                        Cancel
                      </Button>
                      <Button
                        variant="contained"
                        color="primary"
                        size="small"
                        onClick={() => createComment(character.id)}
                      >
                        Comment
                      </Button>
                    </Grid>
                  </Grid>
                  <Grid
                    item
                    alignItems="flex-end"
                    alignContent="flex-end"
                    xs={12}
                  ></Grid>
                </Grid>
                <Grid>
                  <CommentsList
                    // data={_orderBy(character.comments, ["createdAt"], ["desc"])}
                    characterId={character.id}
                    user={user}
                    subscriptions={subscriptions}
                  />
                </Grid>
              </Grid>
            </CardContent>
          </Collapse>
        </Card>
      ))}
    </Container>
  );
}
