import {
  gql,
  ApolloClient,
  InMemoryCache,
  HttpLink,
  Resolvers,
} from "@apollo/client";

import _has from "lodash/has";
import _last from "lodash/last";
import _isEmpty from "lodash/isEmpty";

import { LinkObject, NodeObject } from "react-force-graph-2d";

type NewNodeObject = NodeObject & {
  class?: boolean;
};

const API = "https://rickandmortyapi.com/graphql";

const cache = new InMemoryCache();

export const resolvers: Resolvers = {
  Mutation: {
    setLinkBy: (_root, { linkBy, categorize }, { cache }) => {
      if (linkBy) {
        const {
          graphData: { nodes },
        } = cache.readQuery({
          query: GET_GRAPH_DATA,
          // variables: { categorize },
        });

        cache.writeQuery({
          query: GET_LINK_BY,
          data: {
            linkBy,
            categorize,
          },
        });
        // const links: LinkObject[] = createLinks(nodes, linkBy);
        const graphData = createNewGraph(nodes, linkBy, categorize);

        cache.writeQuery({
          query: GET_GRAPH_DATA,
          data: {
            graphData,
          },
        });
      }
    },
  },
  Query: {
    graphData({ characters }, _args, { cache }) {
      const { results } = characters;
      const { linkBy, categorize } = cache.readQuery({ query: GET_LINK_BY });

      const {
        graphData: { nodes: oldNodes },
      } = cache.readQuery({
        query: GET_GRAPH_DATA,
        // variables: { categorize },
      });

      const nodes = oldNodes.concat(results);

      const graphData = linkBy
        ? createNewGraph(nodes, linkBy, categorize)
        : {
            /**
             * It's important to have    __typname present, otherwise Apollo cannot uniquiely identify the object
             */
            __typename: "graphData",
            nodes: nodes,
            links: [],
          };

      return graphData;
    },
  },
};

export const createNewGraph = (
  origNodes: NodeObject[],
  linkBy: string,
  categorize?: boolean
) => {
  // remove old classifications
  const nodes = categorize
    ? origNodes.filter((n) => !_has(n, "class"))
    : origNodes;

  // create new array for each unique link value
  const linkByVal = Array.from(new Set(nodes.map((r: any) => r[linkBy])));

  // creates an object mapping of every possible link value
  const linkVal: any = linkByVal.reduce(
    (obj: any, val: any) => ({ ...obj, [val]: [] }),
    {}
  );

  // new nodes to be created from defined Link values
  const newNodes = categorize
    ? nodes.concat(
        linkByVal.map((val) => ({
          id: val || "n/a",
          name: val || "n/a",
          image: "",
          class: true,
        }))
      )
    : nodes.filter((n: NewNodeObject) => !n.class);

  // create link to new nodes
  const links = categorize
    ? nodes.map((r: any) => ({
        source: r.id,
        target: r[linkBy] || "n/a",
      }))
    : newNodes
        .map((r: any) => {
          const link: any = _last(linkVal[r[linkBy]]);
          linkVal[r[linkBy]].push(r);
          if (_isEmpty(link)) {
            return {
              source: "",
              target: "",
            };
          }
          return {
            source: link.id,
            target: r.id,
          };
        })
        .filter((l: LinkObject) => l.source !== "");

  return {
    __typename: "graphData",
    nodes: newNodes,
    links,
  };
};

/**
 * Client for Apollo
 * Assume immutable
 */
export const CLIENT = new ApolloClient({
  cache,
  assumeImmutableResults: true,
  connectToDevTools: true,
  link: new HttpLink({
    uri: API,
  }),
  resolvers,
});

// export const GET_GRAPH_DATA = gql`
//   query getGraphData {
//     graphData @client
//   }
// `;

export const GET_GRAPH_DATA = gql`
  query getGraphData {
    graphData @client
  }
`;

export const GET_LINK_BY = gql`
  query getLinkBy {
    linkBy @client
    categorize @client
  }
`;

cache.writeQuery({
  query: GET_LINK_BY,
  data: {
    linkBy: "",
    categorize: false,
  },
});

cache.writeQuery({
  query: GET_GRAPH_DATA,
  // variables: { categorize: false },
  data: {
    graphData: {
      __typename: "graphData",
      nodes: [],
      links: [],
    },
  },
});

// watch the source field query and write resolvedDevice back to the cache at top-level
// CLIENT.watchQuery({ query: GET_LINK_BY }).subscribe(({ data }) => {
//   console.log(data);
// });

export const SET_LINK_BY = gql`
  mutation SetLinkBy($linkBy: String!, $categorize: Boolean!) {
    setLinkBy(linkBy: $linkBy, categorize: $categorize) @client
  }
`;

export const GET_RM_CHARS = gql`
  query getRMChars($page: Int) {
    characters(page: $page) {
      info {
        count
        pages
        next
        prev
      }
      results {
        id
        name
        status
        species
        type
        gender
        image
        created
      }
    }
    graphData @client {
      __typename
      nodes
      links
    }
  }
`;

export default GET_RM_CHARS;
