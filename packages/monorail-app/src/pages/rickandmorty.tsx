import React, { useState, useEffect } from "react";
import {
  makeStyles,
  createStyles,
  Theme,
  fade,
} from "@material-ui/core/styles";
import Popover from "@material-ui/core/Popover";
import Container from "@material-ui/core/Container";
import { PopperCard, Histogram } from "@monorail/ui-lib";
import Grid from "@material-ui/core/Grid";
import Skeleton from "@material-ui/lab/Skeleton";
import InfiniteScroll from "react-infinite-scroller";
import SearchIcon from "@material-ui/icons/Search";
import InputBase from "@material-ui/core/InputBase";
import AppBar from "@material-ui/core/AppBar";
import IconButton from "@material-ui/core/IconButton";
import Toolbar from "@material-ui/core/Toolbar";
import Clear from "@material-ui/icons/Clear";
import * as d3 from "d3";
import rmData from "../constants/rickAndMorty.json";

import _isEmpty from "lodash/isEmpty";
import _orderBy from "lodash/orderBy";
import _isNil from "lodash/isNil";
import _keys from "lodash/keys";
import _has from "lodash/has";

import {
  ApolloClient,
  HttpLink,
  InMemoryCache,
  ApolloProvider,
  useQuery,
  gql,
} from "@apollo/client";
import { Typography } from "@material-ui/core";

const GET_CHARACTERS = gql`
  query($page: Int, $filter: FilterCharacter) {
    characters(page: $page, filter: $filter) {
      info {
        count
        pages
        next
        prev
      }
      results {
        id
        name
        status
        species
        type
        gender
        origin {
          id
          name
          type
          dimension
          created
        }
        location {
          id
          name
          type
          dimension
          created
        }
        image
        episode {
          id
          name
          episode
          created
        }
        created
      }
    }
  }
`;

// 281 Alive, 65 unknown, 147 Dead
// 371 Male, 74 Female, 42 unknown, 6 Genderless
// 297 Human, 132 Alien, 53 Humanoid, 5 unknown, 6 Poopybutthole, 7 Mytholog, Animal: 17,Vampire: 3,Robot: 11,Cronenberg: 8,Disease: 6 ,Parasite: 1

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    appbar: { background: "orange" },
    typography: {
      padding: theme.spacing(2),
    },
    root: {
      flexGrow: 1,
    },
    menuButton: {
      marginRight: theme.spacing(2),
    },
    title: {
      flexGrow: 1,
      display: "none",
      [theme.breakpoints.up("sm")]: {
        display: "block",
      },
    },
    paper: {
      padding: theme.spacing(2),
      textAlign: "center",
      color: theme.palette.text.secondary,
    },
    search: {
      position: "relative",
      borderRadius: theme.shape.borderRadius,
      backgroundColor: fade(theme.palette.common.white, 0.15),
      "&:hover": {
        backgroundColor: fade(theme.palette.common.white, 0.25),
      },
      marginLeft: 0,
      width: "100%",
      [theme.breakpoints.up("sm")]: {
        marginLeft: theme.spacing(1),
        width: "auto",
      },
    },
    searchIcon: {
      padding: theme.spacing(0, 2),
      height: "100%",
      position: "absolute",
      pointerEvents: "none",
      display: "flex",
      alignItems: "center",
      justifyContent: "center",
    },
    cancelIcon: {
      padding: theme.spacing(0, 2),
      height: "100%",
      position: "absolute",
      pointerEvents: "none",
      display: "flex",
      alignItems: "center",
      justifyContent: "center",
    },
    inputRoot: {
      color: "inherit",
    },
    inputInput: {
      padding: theme.spacing(1, 1, 1, 0),
      // vertical padding + font size from searchIcon
      paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
      transition: theme.transitions.create("width"),
      width: "100%",
      [theme.breakpoints.up("sm")]: {
        width: "12ch",
        "&:focus": {
          width: "20ch",
        },
      },
    },
    characterName: {
      color: "darkgray",
      fontSize: "12px",
      textAlign: "center",
    },
    characterImage: {
      textAlign: "center",
    },
  })
);

const CharacterGrid = () => {
  const classes = useStyles();
  const imageHeight = 125;
  const imageWidth = 125;
  const [popoverContent, setPopoverContent] = useState(null);
  const [anchorEl, setAnchorEl] = useState<HTMLImageElement | null>(null);
  const [pageData, setPageData] = useState([]);
  const [searchVars, setSearchVars] = useState<any>({
    page: 1,
    text: "",
    gender: "",
    status: "",
    species: "",
  });

  const { loading, error, data } = useQuery(GET_CHARACTERS, {
    variables: {
      page: searchVars.page,
      filter: {
        name: searchVars.text,
        gender: searchVars.gender,
        status: searchVars.status,
        species: searchVars.species,
      },
    },
  });

  useEffect(() => {
    //Have new data, add or append the new characters
    if (data) {
      //We have errors, clear the list of characters
      if (data.errors) {
        setPageData([]);
        return;
      }

      //This is needed to prevent adding second page twice
      const nextPage = _isNil(data.characters.info.next)
        ? searchVars.page + 1
        : data.characters.info.next;
      if (
        searchVars.page < nextPage ||
        (searchVars.page === 1 && data.characters.info.next === null)
      ) {
        setPageData(
          (prev) =>
            (_isNil(data.characters.info.next) &&
              _isNil(data.characters.info.prev)) ||
            data.characters.info.next === 2 ||
            searchVars.page === 1
              ? data.characters.results //On the first page, start with new characters
              : prev.concat(data.characters.results) //On a subsequent page, append the characters to existing results
        );
      }
    }
  }, [loading, data, searchVars.page]);

  if (error) {
    if (searchVars.page > 1) {
      //The GraphQL api returns an error if on page 3 and only 1 page of results, try page 1
      setSearchVars((prev: any) => ({ ...prev, page: 1 }));
    } else {
    }
  }

  const handleClick = (
    event: React.MouseEvent<HTMLImageElement>,
    content: any
  ) => {
    setAnchorEl(event.currentTarget);
    setPopoverContent(content);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const generateSkeletons = () => {
    const skeletons = [];
    let i = 0;
    while (i < 20) {
      skeletons.push(
        <Grid item xs key={i} className={classes.characterImage}>
          <Skeleton
            animation="wave"
            variant="rect"
            width={imageWidth}
            height={imageHeight}
          />
          {/* <Typography variant="subtitle2" > */}
          <Skeleton animation="wave" variant="text" width={90} />
          {/* </Typography> */}
        </Grid>
      );
      i++;
    }
    return skeletons;
  };
  const handleLoadMoreCharacters = () => {
    if (data && searchVars.page !== data.characters.info.next) {
      setSearchVars((prev: any) => ({
        ...prev,
        page: data.characters.info.next,
      }));
    }
  };

  const handleSearchText = (event: any) => {
    setSearchVars((prev: any) => ({
      ...prev,
      text: event.currentTarget.value,
      page: 1,
    }));
  };

  const loader = <div className="loader"></div>;

  // const groups = barData.map((data: any) => data.id);
  const groups = ["gender", "species", "status"];

  let nesting = _keys(searchVars)
    .filter((key) => key !== "page" && key !== "text")
    .map((key: string) => {
      if (searchVars[key] !== "") {
        return key;
      }
      return "";
    });

  const groupRollups = groups.map((group: string) => {
    let groupData = d3.nest();
    let path = [];
    nesting
      .filter((nest: string) => nest !== group && !_isEmpty(nest))
      .forEach((nest: string) => {
        // console.log("group, nest:", group, nest);
        groupData.key((d: any) => d[nest]);
        path.push(searchVars[nest]);
      });
    groupData.key((d: any) => d[group]);
    let groupRolled: any = groupData
      .rollup((v: any) => v.length)
      .entries(rmData);
    if (!_isEmpty(searchVars[group])) {
      path.push(searchVars[group]);
    }
    // console.log("groupRolled:", group, groupRolled);
    return {
      data: groupRolled,
      path: path,
    };
  });
  // console.log("groupRollups:", groupRollups);
  let genderCounts = groupRollups[0];
  let speciesCounts = groupRollups[1];
  let statusCounts = groupRollups[2];

  statusCounts.path.forEach((p) => {
    statusCounts.data = _has(
      statusCounts.data.filter((d: any) => d.key === p)[0],
      "values"
    )
      ? statusCounts.data.filter((d: any) => d.key === p)[0].values
      : [statusCounts.data.filter((d: any) => d.key === p)[0]];
  });
  // console.log("statusCounts.data:", statusCounts.data);
  statusCounts.data = _orderBy(
    statusCounts.data.map((d: any) => ({
      key: d.key,
      count: d.value,
      color: "orange",
    })),
    ["count", "asc"]
  );

  speciesCounts.path.forEach((p) => {
    speciesCounts.data = _has(
      speciesCounts.data.filter((d: any) => d.key === p)[0],
      "values"
    )
      ? speciesCounts.data.filter((d: any) => d.key === p)[0].values
      : [speciesCounts.data.filter((d: any) => d.key === p)[0]];
  });
  speciesCounts.data = _orderBy(
    speciesCounts.data.map((d: any) => ({
      key: d.key,
      count: d.value,
      color: "orange",
    })),
    ["count", "asc"]
  );

  genderCounts.path.forEach((p) => {
    genderCounts.data = _has(
      genderCounts.data.filter((d: any) => d.key === p)[0],
      "values"
    )
      ? genderCounts.data.filter((d: any) => d.key === p)[0].values
      : [genderCounts.data.filter((d: any) => d.key === p)[0]];
  });
  genderCounts.data = _orderBy(
    genderCounts.data.map((d: any) => ({
      key: d.key,
      count: d.value,
      color: "orange",
    })),
    ["count", "asc"]
  );
  // console.log("genderCounts:", genderCounts.data);
  // console.log("statusCounts:", statusCounts.data);
  // console.log("speciesCounts:", speciesCounts.data);

  const barData = [
    {
      id: "gender",
      label: "Gender",
      data: genderCounts.data,
      keys: ["count"],
      indexBy: "key",
      groupMode: "stacked" as "grouped" | "stacked",
      barHeight: 25,
      colors: (data: any) => data.data.color,
      onClick: (data: any) =>
        setSearchVars((prev: any) => ({
          ...prev,
          page: 1,
          gender: data.indexValue,
        })),
      onClear: (data: any) =>
        setSearchVars((prev: any) => ({ ...prev, page: 1, gender: "" })),
    },
    {
      id: "status",
      label: "Status",
      data: statusCounts.data,
      keys: ["count"],
      indexBy: "key",
      groupMode: "stacked" as "grouped" | "stacked",
      barHeight: 25,
      colors: (data: any) => data.data.color,
      onClick: (data: any) =>
        setSearchVars((prev: any) => ({
          ...prev,
          page: 1,
          status: data.indexValue,
        })),
      onClear: (data: any) =>
        setSearchVars((prev: any) => ({ ...prev, page: 1, status: "" })),
    },
    {
      id: "species",
      label: "Species",
      data: speciesCounts.data,
      keys: ["count"],
      indexBy: "key",
      groupMode: "stacked" as "grouped" | "stacked",
      barHeight: 25,
      colors: (data: any) => data.data.color,
      onClick: (data: any) =>
        setSearchVars((prev: any) => ({
          ...prev,
          page: 1,
          species: data.indexValue,
        })),
      onClear: (data: any) =>
        setSearchVars((prev: any) => ({ ...prev, page: 1, species: "" })),
    },
  ];

  const chartTheme = {
    tooltip: {
      container: {
        fontSize: 12,
        color: "white",
        background: "dimgray",
        borderRadius: "2px",
        boxShadow: "0 1px 2px rgba(0, 0, 0, 0.25)",
        padding: "5px 9px",
      },
    },
    axis: {
      legend: {
        text: {
          fontSize: 11,
          fill: "darkgoldenrod",
        },
      },
    },
  };

  // console.log("pageData:", JSON.stringify(pageData));
  return (
    <Container style={{ margin: "1em 0" }}>
      <AppBar position="static" color="secondary">
        <Toolbar>
          <Typography className={classes.title} variant="h6" noWrap>
            Rick and Morty Facebook
          </Typography>
          <div className={classes.search}>
            <div className={classes.searchIcon}>
              <SearchIcon />
            </div>
            <InputBase
              placeholder="Search…"
              classes={{
                root: classes.inputRoot,
                input: classes.inputInput,
              }}
              inputProps={{ "aria-label": "search" }}
              endAdornment={
                !_isEmpty(searchVars.text) ? (
                  <IconButton
                    onClick={() =>
                      setSearchVars((prev: any) => ({
                        ...prev,
                        page: 1,
                        text: "",
                      }))
                    }
                  >
                    <Clear />
                  </IconButton>
                ) : null
              }
              onChange={handleSearchText}
              value={searchVars.text}
            />
          </div>
        </Toolbar>
      </AppBar>
      <br />
      <Grid
        container
        direction="row"
        justify="flex-start"
        alignItems="flex-start"
        spacing={1}
      >
        <Grid item xs={8}>
          <Grid container spacing={3}>
            {data && !error && (
              <Grid item xs={12}>
                <InfiniteScroll
                  initialLoad={true}
                  pageStart={1}
                  loadMore={handleLoadMoreCharacters}
                  hasMore={data ? data.characters.info.next !== null : false}
                  loader={loader}
                >
                  <Grid container spacing={3}>
                    {pageData.map((character: any) => {
                      return (
                        <Grid
                          item
                          xs
                          key={character.id}
                          className={classes.characterImage}
                        >
                          <img
                            src={character.image}
                            alt={character.name}
                            width={imageWidth}
                            height={imageHeight}
                            onClick={(event) => handleClick(event, character)}
                          />
                          <Typography
                            variant="subtitle2"
                            className={classes.characterName}
                          >
                            {character.name}
                          </Typography>
                        </Grid>
                      );
                    })}
                  </Grid>
                </InfiniteScroll>
                {
                  //Append on some skeletons
                  loading && (
                    <Grid container spacing={3}>
                      {generateSkeletons()}
                    </Grid>
                  )
                }
              </Grid>
            )}
          </Grid>
          <Popover
            id={"popover"}
            open={Boolean(anchorEl)}
            anchorEl={anchorEl}
            onClose={handleClose}
            anchorOrigin={{
              vertical: "top",
              horizontal: "right",
            }}
            transformOrigin={{
              vertical: "bottom",
              horizontal: "left",
            }}
          >
            <PopperCard content={popoverContent} />
          </Popover>
          {/* </div> */}
        </Grid>
        <Grid item xs={4}>
          <Histogram
            data={barData}
            theme={chartTheme}
            width={300}
            onHome={() =>
              setSearchVars({
                page: 1,
                text: "",
                gender: "",
                status: "",
                species: "",
              })
            }
            filterData={false}
          />
        </Grid>
      </Grid>
    </Container>
  );
};

const client = new ApolloClient({
  cache: new InMemoryCache(),
  link: new HttpLink({
    uri: "https://rickandmortyapi.com/graphql",
  }),
});

export const RickAndMorty = () => {
  return (
    <ApolloProvider client={client}>
      <Container>
        <CharacterGrid />
      </Container>
    </ApolloProvider>
  );
};

export default RickAndMorty;
