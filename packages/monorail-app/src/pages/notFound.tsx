import React from "react";
import logo from "../logo.jpeg";
import { Typography, Container, Grid } from "@material-ui/core";
import "../App.css";

function NotFound() {
  return (
    <Container style={{ margin: "1em auto" }}>
      <Grid container justify="center" alignItems="center">
        <Grid item>
          <div style={{ textAlign: "center", paddingBottom: "50px" }}>
            <img src={logo} className="App-logo" alt="logo" />
          </div>
          <br />
          <Typography variant="h2" align="center">
            Page not found
          </Typography>
        </Grid>
      </Grid>
    </Container>
  );
}

export default NotFound;
