import React from "react";
import { Container } from "@material-ui/core";
import { Images } from "@monorail/ui-lib";

import "../App.css";

function ImagesPage() {
  return (
    <Container>
      <Images />
    </Container>
  );
}
export default ImagesPage;
