import React from "react";
import { createStyles, Theme, makeStyles } from "@material-ui/core/styles";
import { useQuery, gql } from "@apollo/client";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import Divider from "@material-ui/core/Divider";
import ListItemText from "@material-ui/core/ListItemText";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import Avatar from "@material-ui/core/Avatar";
import Typography from "@material-ui/core/Typography";
import ThumbUp from "@material-ui/icons/ThumbUp";
import ThumbDown from "@material-ui/icons/ThumbDown";
// import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import Box from "@material-ui/core/Box";
import IconButton from "@material-ui/core/IconButton";
import _orderBy from "lodash/orderBy";

const GET_COMMENTS = gql`
  query($characterId: Int!) {
    characterComments(characterId: $characterId) {
      id
      user {
        id
        username
      }
      character {
        id
        name
        desc
      }
      comment
      createdAt
    }
  }
`;

const COMMENTS_SUBSCRIPTION = gql`
  subscription($characterId: Int!) {
    commentAdded(characterId: $characterId) {
      id
      user {
        id
        username
      }
      character {
        id
        name
        desc
      }
      comment
      createdAt
    }
  }
`;

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: "100%",
      maxWidth: "56ch",
      backgroundColor: theme.palette.background.paper,
    },
    inline: {
      display: "inline",
      paddingRight: "5px",
      color: "darkgoldenrod",
    },
    thumbActions: {
      display: "inline-block",
      paddingRight: "10px",
      float: "right",
      width: "100%",
    },
  })
);
export interface CommentListProps {
  characterId: number;
  // data?: Array<any>;
  subscriptions?: boolean;
  user?: string;
}

export default function CommentList(props: CommentListProps) {
  const { characterId, subscriptions } = props;

  const { data, subscribeToMore } = useQuery(GET_COMMENTS, {
    variables: { characterId: characterId },
  });

  //Option 2: useSubscription hook, can update a state var, or use client to update cache
  // const { data: subscriptionData, client, loading: commentLoading } = useSubscription(
  //   COMMENTS_SUBSCRIPTION,
  //   {
  //     variables: { characterId: characterId },
  //     // onSubscriptionData: (data: any) => console.log("got sub data", data),
  //   }
  // );
  const classes = useStyles();
  if (subscribeToMore) {
    //Option 1: use the original query subscribeToMore function to tie in the sub.
    subscribeToMore({
      document: COMMENTS_SUBSCRIPTION,
      variables: { characterId: characterId },
      updateQuery: (prev, { subscriptionData }) => {
        if (!subscriptionData.data) return prev;
        if (!subscriptions) return prev; //ONLY USEFUL FOR TESTING
        const newComment = subscriptionData.data.commentAdded;
        const exists = prev.characterComments.filter(
          ({ id }: any) => id === newComment.id
        );
        if (exists.length > 0) return prev;
        return { characterComments: prev.characterComments.concat(newComment) };
      },
    });
  }

  const { characterComments = [] } = data ? data : {};

  return (
    <List className={classes.root}>
      {_orderBy(characterComments, ["createdAt"], ["desc"]).map(
        (comment: any) => (
          <React.Fragment>
            <ListItem alignItems="flex-start">
              <ListItemAvatar>
                <Avatar
                  alt={comment.user.username.toUpperCase()}
                  src="/static/images/avatar/1.jpg"
                />
              </ListItemAvatar>
              <ListItemText
                //   primary={comment.user.username}
                secondary={
                  <React.Fragment>
                    <Typography
                      component="span"
                      variant="body2"
                      className={classes.inline}
                      color="textPrimary"
                    >
                      {comment.user.username}
                    </Typography>
                    {comment.comment}
                    <Box className={classes.thumbActions}>
                      <IconButton edge="end" aria-label="comments" size="small">
                        <ThumbUp />
                      </IconButton>
                      <IconButton edge="end" aria-label="comments">
                        <ThumbDown />
                      </IconButton>
                    </Box>
                  </React.Fragment>
                }
              />
            </ListItem>
            <Divider variant="inset" component="li" />
          </React.Fragment>
        )
      )}
    </List>
  );
}
