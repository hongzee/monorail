export const isValidCoord = (lat, lon) => {
  const latF = parseFloat(lat);
  const lonF = parseFloat(lon);
  if (
    !isNaN(lonF) &&
    !isNaN(latF) &&
    Math.abs(latF) + Math.abs(lonF) > 0 &&
    latF <= 90 &&
    latF >= -90 &&
    lonF <= 180 &&
    lonF >= -180
  )
    return true;
  else return false;
};

export const mapToArrayTransformer = (data) => {
  let newData = [];
  data.forEach((v, k) => {
    v.coords.forEach((c, i) => {
      if (i > 0) {
        newData.push({ coords: c, tsi: v.tsi[i], hdg: v.hdg[i] });
      }
    });
  });
  return newData;
};

const COLOURS = {
  orange: [255, 65, 0],
};

export const scatterplot_options = {
  // data: events,
  getFillColor: COLOURS.orange,
  getLineColor: COLOURS.orange,
  getRadius: 3,
  radiusMinPixels: 3,
  opacity: 1.0,
  filled: true,
  stroked: false,
};
const SVG = encodeURIComponent(
  '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 24 24"><defs><filter id="dropshadow" width="200%" height="200%"><feDropShadow dx="1.5" dy="1.5" stdDeviation="0.8" /></filter></defs><path d="M12,2L4.5,20.29L5.21,21L12,18L18.79,21L19.5,20.29L12,2Z" fill="#ff4100" filter="url(#dropshadow)"/></svg>'
);

export const iconlayer_options = {
  id: "current-icon",
  // data: events,
  getPosition: (d) => d[1]["coords"][0],
  getSize: 30,
  sizeUnits: "meters",
  sizeMinPixels: 20,
  getAngle: (d) => -d[1]["hdg"][0],
  getIcon: (d) => ({
    id: (d) => `${d.veh}_${d.tsi}`,
    url: `data:image/svg+xml;charset=utf-8,${SVG}`,
    width: 64,
    height: 64,
  }),
  opacity: 0.8,
  billboard: false,
};

// const icon_options = {
//   getColor: COLOURS.orange,
//   getSize: 20,
//   sizeUnits: "meters",
//   sizeMinPixels: 20,
//   iconAtlas: "images/icons/icon-atlas.png",
//   iconMapping: ICON_MAPPING,
//   getIcon: (d: any) => "expand_less",
//   opacity: 0.8,
// };
