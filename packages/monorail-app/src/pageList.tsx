import React from "react";

import HomeIcon from "@material-ui/icons/Home";
import MapIcon from "@material-ui/icons/Map";
import NetworkIcon from "@material-ui/icons/PersonPinRounded";
import PieChartIcon from "@material-ui/icons/PieChart";
import TagFacesIcon from "@material-ui/icons/TagFaces";
import ImageIcon from "@material-ui/icons/Image";
// import Chat from "@material-ui/icons/Chat";

//UI pages
import { Page } from "@monorail/ui-lib";
import HomePage from "./pages/home";
import MapPage from "./pages/map/mapPage";
import RickAndMortyPage from "./pages/rickandmorty";
import ImagesPage from "./pages/images";
import DashboardPage from "./pages/dashboard";
import NetworkPage from "./pages/network";
// import ChatPage from "./pages/chat";

export const PAGES: Array<Page> = [
  { text: "Home", icon: <HomeIcon />, path: "/", component: <HomePage /> },
  { text: "Map", icon: <MapIcon />, path: "/map", component: <MapPage /> },
  {
    text: "Network",
    icon: <NetworkIcon />,
    path: "/network",
    component: <NetworkPage />,
  },
  {
    text: "Dashboard",
    icon: <PieChartIcon />,
    path: "/dashboard",
    component: <DashboardPage />,
  },
  {
    text: "Images",
    icon: <ImageIcon />,
    path: "/images",
    component: <ImagesPage />,
  },
  {
    text: "Rick and Morty",
    icon: <TagFacesIcon />,
    path: "/rickandmorty",
    component: <RickAndMortyPage />,
  },
  // {
  //   text: "Chat Simulator",
  //   icon: <Chat />,
  //   path: "/chat",
  //   component: <ChatPage />,
  // },
];
