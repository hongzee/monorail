import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import * as serviceWorker from "./serviceWorker";
import theme from "./constants/theme";
import { ThemeProvider } from "@material-ui/core/styles";
import { BrowserRouter } from "react-router-dom";

// import Keycloak, { KeycloakConfig } from "keycloak-js";
// import { KeycloakProvider } from "@react-keycloak/web";

// const config: KeycloakConfig = {
//   clientId: process.env.REACT_APP_KEYCLOAK_CLIENT_ID || "",
//   realm: process.env.REACT_APP_KEYCLOAK_REALM || "",
//   url: process.env.REACT_APP_KEYCLOAK_URL,
// };

// const keycloak = new (Keycloak as any)(config);

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </ThemeProvider>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
