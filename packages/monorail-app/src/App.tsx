import React, { useState } from "react";
import "./App.css";
import { MiniDrawer } from "@monorail/ui-lib";

import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";

import { Routes, Route, useNavigate, useLocation } from "react-router-dom";
import NotFound from "./pages/notFound";
import { PAGES } from "./pageList";

import { useGoogleLogin, useGoogleLogout } from "react-google-login";
const clientId: string =
  "78267046844-1ur9kt69f70dk8f98v9s8d204h8qo3pc.apps.googleusercontent.com";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    toolbar: {
      display: "flex",
      alignItems: "center",
      justifyContent: "flex-end",
      padding: theme.spacing(0, 1),
      ...theme.mixins.toolbar,
    },
    content: {
      flexGrow: 1,
      padding: `0 ${theme.spacing(3)}px ${theme.spacing(3)}px ${theme.spacing(
        3
      )}px`,
    },
  })
);

function App() {
  const [profile, setProfile] = useState({});
  const [auth, setAuth] = useState(false);

  const classes = useStyles();

  const navigate = useNavigate();
  const location = useLocation();
  const activePath =
    location.pathname === "/"
      ? -1
      : PAGES.findIndex((page) => page.path === location.pathname);
  const [activePage, setActivePage] = useState(activePath);

  const handlePageChange = (pageIndex: number) => {
    console.log(pageIndex);
    setActivePage(pageIndex);
    navigate(PAGES[pageIndex].path);
  };

  const onSuccessLogin = (res: any) => {
    console.log(res);
    setAuth(true);
    setProfile(res.profileObj);
  };

  const onFailureLogin = (error: any) => {
    console.log("error: ", error);
  };

  const onLogoutSuccess = () => {
    console.log("Successfully logged off...");
    setAuth(false);
    setProfile({});
  };

  const onFailureLogout = () => {
    console.log("error logging out");
  };

  const { signIn } = useGoogleLogin({
    clientId,
    onSuccess: onSuccessLogin,
    onFailure: onFailureLogin,
    cookiePolicy: "single_host_origin",
    isSignedIn: true,
  });

  const { signOut } = useGoogleLogout({
    clientId,
    onLogoutSuccess,
    onFailure: onFailureLogout,
    cookiePolicy: "single_host_origin",
  });

  return (
    <div className="App">
      <MiniDrawer
        text="MONORAIL"
        pages={PAGES}
        onPageChange={handlePageChange}
        activePage={activePage}
        profile={profile}
        auth={auth}
        signIn={signIn}
        signOut={signOut}
      />
      <main className={classes.content}>
        <div className={classes.toolbar} />
        {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
        <Routes>
          {/* Add pages as routes */}
          {PAGES.map((page) => (
            <Route key={page.text} path={page.path} element={page.component} />
          ))}
          <Route path="*" element={<NotFound />} />
        </Routes>
      </main>
    </div>
  );
}

export default App;
